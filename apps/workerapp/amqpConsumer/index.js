/*jslint node: true */
/*jslint esversion : 6*/
'use strict';
var amqp = require('amqplib');
var config = require('../../config');
var ConsumerNextProfiles = require('./consumers/consumerNextProfiles.js');
var EventEmitter = require('events').EventEmitter;
var logger = require('../logger');

/**
  What if one of the events will throw an error?!
  1. Test what happens when event emitters are emitting different events.
  2. Test what happens if the consumer worker goes down (will the server attempt to restart it)??
*/

function Consumer() {
  EventEmitter.call(this);
  this.amqpConnection = null;
  this.nextProfilesConsumer = null;
}

Consumer.prototype = Object.create(EventEmitter.prototype);

Consumer.prototype.start = startConsumer;

function startConsumer() {
  var url = config.amqp.url + ':' + config.amqp.port + '?heartbeat=60';
  var _this = this;
  logger.info('[AMQP CONSUMER] Connecting to : ' + url);

  return amqp.connect(url)
      .then(onAmqpConnected)
      .then(startConsumerChannel)
      .then(onConsumerCreated)
      .catch(onAmqpConnectionError);

  //////

  function startConsumerChannel() {
    logger.info('[AMQP CONSUMER] Starting a consumer channel.');
    if (_this && _this.nextProfilesConsumer) {
      return _this.nextProfilesConsumer.startConsumerChannel(_this.amqpConnection);
    } else {
      throw new Error('No consumer.');
    }
  }

  function onConsumerCreated(consumer) { 
    logger.info('[AMQP CONSUMER] Consumer has started');
    consumer.on('consumerError', function(error) {
      logger.error('[AMQP CONSUMER ERROR] Consumer has emitted an error');
      _this.emit('channelError', error);
    });
    consumer.on('messageRejected', function(error) {
      logger.info('[AMQP CONSUMER MESSAGE REJECTED] Consumer has emitted a message rejected');
      _this.emit('channelError', error);
    });

    consumer.on('messageReceived', function(error) {
      logger.info('[AMQP CONSUMER MESSAGE RECEIVED] Consumer has emitted a message received');
    });
    return _this;
  }

  function onAmqpConnectionError(error) {
    logger.error('[AMQP CONSUMER ERROR]', error.message);
    _this.emit('amqpConnectionError', error);
    throw error;
  }

  function onAmqpConnected(connection) {
    _this.amqpConnection = connection;
    _this.amqpConnection.on('error', onAmqpConnectionClose.bind(this));
    _this.amqpConnection.on('close', onAmqpConnectionError.bind(this));

    logger.info('[AMQP CONSUMER CONNECTED]');
    logger.info('[AMQP CONSUMER] Created a next profile delegate');

    _this.nextProfilesConsumer = new ConsumerNextProfiles(_this.amqpConnection);
    return connection;
  }

  function onAmqpConnectionClose() {
    _this.emit('amqpConnectionClose');
  }
}

 


module.exports = new Consumer();