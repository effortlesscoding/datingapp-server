'use strict';
var config = require('../../../config');
var mongoose = require('mongoose');
var User = require('../../../webserviceapp/api/v1/user/model/user.js');
var EventEmitter = require('events').EventEmitter;
var logger = require('../../logger');
var events = {
  consumerError : 'consumerError',
  messageRejected : 'messageRejected',
  messageReceived : 'messageReceived'
}
const QUEUE_NAME = config.amqp.queues.nextProfiles; 

function ConsumerNextProfiles(ac) {
  this.amqpConnection = ac;
  this.channel = null;
}


ConsumerNextProfiles.prototype = Object.create(EventEmitter.prototype);

ConsumerNextProfiles.prototype.startConsumerChannel = startConsumerChannel;

ConsumerNextProfiles.prototype.purgeQueue = purgeQueue;

function purgeQueue() {
  var _this = this;
  return new Promise(function(resolve, reject) {
    if (_this.channel) {
      _this.channel.nackAll(false);
      logger.info('[AMQP Consumer] Abolished all messages in the queue.');
      return resolve();
    } else {
      throw new Error('No channel to purge');
    }
  });
};

function startConsumerChannel(amqpConnection) {
  if (!amqpConnection) { throw new Error('[AMQP Consumer] No connection in startConsumer().'); }
  var _this = this;
  logger.info('[AMQP CONSUMER] Creating a channel for the consumer');
  
  return amqpConnection.createChannel()
    .then(onChannelCreated)
    .catch(onChannelError);

  //////
  
  function onChannelError(err) {
    logger.error('[AMQP] error', err);
    amqpConnection.close();
    throw err;
  }

  function onChannelCreated(ch) {
    logger.info('[AMQP CONSUMER] Channel created');
    _this.channel = ch;
    _this.channel.on('error', onChannelError);
    _this.channel.prefetch(10);
    _this.channel.assertQueue(QUEUE_NAME, {durable : true})
      .then(function(ok) {
        _this.channel.consume(QUEUE_NAME, processMsg, { noAck : false });
        logger.info('[AMQP CONSUMER] Channel queue asserted.');
      })
      .catch(function(err) {
        throw err;
      });
    logger.info('[AMQP CONSUMER] Channel returning.');
    return _this;
  }

  function processMsg(msg) {
    logger.info('[AMQP Consumer] Message received in ' + QUEUE_NAME);
    logger.info(msg.content.toString());
    var user = null;
    try {
      user = JSON.parse(msg.content);
      logger.info('[AMQP Consumer] I started looking for next profiles to match with the user.');
      fetchNextProfiles(user);
    } catch(e) {
      _this.channel.reject(msg);
      logger.error('Consumer could not parse.');
      logger.error(e);
    }

    function fetchNextProfiles(user) {
      if (!user) { return; }
      logger.info('[AMQP CONSUMER] Fetching next profiles.');
      User.fetchNextProfiles({facebookId : user.facebookId})
        .then(onFetchSuccess)
        .catch(onFetchError);

    }
    
    function onFetchSuccess() {
      logger.info('[AMQP Consumer] Managed to fetch next profiles for the user#' + user.facebookId);
      _this.channel.ack(msg);
      _this.emit(events.messageReceived, user);
    }

    function onFetchError(err) {
      logger.info('[AMQP Consumer] Failed to fetch next profiles for the user#' + user.facebookId);
      _this.channel.reject(msg);
      _this.emit(events.messageRejected, err);
    }

  }

};


module.exports = ConsumerNextProfiles;