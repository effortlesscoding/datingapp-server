//Lets require/import the HTTP module
var http = require('http');
var amqpConsumer = require('./amqpConsumer');
var mongoose = require('mongoose');
var config = require('../config');
//Lets define a port we want to listen to
const PORT=3001; 

console.log('Starting the worker app.');
//We need a function which handles requests and send response
function handleRequest(request, response){
    response.end('It Works!! Path Hit: ' + request.url);
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Worker App Server listening on: http://localhost:%s", PORT);
    mongoose.connect(config.mongoDb.url + config.mongoDb.collection);
    amqpConsumer.start()
      .then(function() {
        console.log('[AMQP Consumer Server] Started on the server!');
      })
      .then(null, function(error) {
        console.log('[AMQP Consumer Server] Failed to start!');
      });
});