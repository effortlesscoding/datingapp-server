'use strict';
var winston = require('winston');
var config = require('../../config');

// Depending on the environment, you will use different loggers. I thihk

var loggerConfig = {};

if (process.env.NODE_ENV.indexOf('production') !== -1 || process.env.NODE_ENV.indexOf('staging') !== -1) {
  loggerConfig = {
    transports: [
      new (winston.transports.File)({
          filename: config.logging.consumerFile,
          level: config.logging.level,
          timestamp: function() {
            return Date.now();
          },
          json: false,
          formatter: function(options) {
            return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (undefined !== options.message ? options.message : '') +
              (options.meta && Object.keys(options.meta).length ? ' ------ '+ JSON.stringify(options.meta) : '' );
          }
      })
    ],
    exitOnError: false
  };
} else if (process.env.NODE_ENV.indexOf('localdev') !== -1) {
  loggerConfig = {
    transports: [
      new (winston.transports.Console)()
    ],
    exitOnError: false
  };
}
var logger = new winston.Logger(loggerConfig);

module.exports = logger;