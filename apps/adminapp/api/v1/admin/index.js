'use strict';

var controller = require('./controller');

class Api {

  constructor(app) {

    app.get('/api/v1/review/flaggedUsers', controller.getFlaggedUsers);
    app.post('/api/v1/review/flaggedUsers/ban', controller.banUser);
    app.post('/api/v1/review/flaggedUsers/reinstate', controller.reinstateUser);

    app.get('/api/v1/review/profileVideos', controller.getProfileVideosToReview);
    app.post('/api/v1/review/profileVideos/approve', controller.approveProfileVideo);
    app.post('/api/v1/review/profileVideos/reject', controller.rejectProfileVideo);

    app.get('/api/v1/review/signedUrl', controller.getSignedUrl);
  }
  
};

module.exports = Api;