'use strict'

const _ = require('lodash')

const crypto = require('crypto')

const User = rootRequire(`apps/webserviceapp/models/User`)
const config = rootRequire(`config`)
const key = rootRequire(`config/key.json`)
const NotificationsUseCase = rootRequire('apps/adminapp/usecases/notifications/').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../';
  return require(`${ROOT_PATH}${path}`)
}

class ApiController {

  getSignedUrl(req, res, next) {
    if (!req.query || !req.query.fileName) { return res.error('No required parameters'); }
    var expiry = new Date().getTime() + 60 * 10; // 10 minutes 
    var fileName = req.query.fileName;
    var bucketName = config.google.storage.bucket;
    var stringPolicy = ['GET\n\n\n', expiry, '\n', '/', bucketName, '/', fileName].join('');
    var base64Policy = Buffer(stringPolicy, 'utf-8').toString('base64'); 
    var accessId = key.client_id;  
    var privateKey = key.private_key;
    var signature = encodeURIComponent(crypto.createSign('sha256').update(stringPolicy).sign(privateKey,'base64'));   
    var signedUrl = ['https://', bucketName, '.commondatastorage.googleapis.com/', fileName, 
                      '?GoogleAccessId=', accessId, '&Expires=', expiry, '&Signature=', signature].join('');
    res.success(signedUrl);
  }

  getFlaggedUsers(req, res, next) {
    let users = [];
    for (let i = 0; i < 10; i++) {
      const user = {
        _id: i,
        profile: {
          firstName: 'User #' + i,
          lastName: 'User #' + i,
          email: 'email#' + i + '@gmail.com'
        },
        complaints: [
          { 
            by: { 
              _id: 99,
              profile: {
                firstName: 'Lina',
                lastName: 'Inverse'
              }
            },
            reason: `Reason #${i}`,
            date: new Date()
          }
        ],
      };
      users.push(user);
    }
    res.success(users);
  }

  banUser(req, res, next) {
    res.success({'message': 'You have banned this user.'});
  }

  reinstateUser(req, res, next) {
    res.success({'message': 'You have reinstated this user.'});
  }


  // I think this is ok for 50000-100000 users, but not more.
  getProfileVideosToReview(req, res, next) {
    // Get ALL users Who have AT LEAST ONE unreviewed video
    const query = { $exists: true };
    User.find({ 
      'profile.videos' : { $exists: true },
      $or: [
        {
          'profile.videos.0': {$exists: true},
          'profile.videos.0.review': { $exists: false }
        },
        {
          'profile.videos.1': {$exists: true},
          'profile.videos.1.review': { $exists: false }
        },
        {
          'profile.videos.2': {$exists: true},
          'profile.videos.2.review': { $exists: false }
        },
        {
          'profile.videos.3': {$exists: true},
          'profile.videos.3.review': { $exists: false }
        },
        {
          'profile.videos.4': {$exists: true},
          'profile.videos.4.review': { $exists: false }
        },
        {
          'profile.videos.5': {$exists: true},
          'profile.videos.5.review': { $exists: false }
        },
        {
          'profile.videos.6': {$exists: true},
          'profile.videos.6.review': { $exists: false }
        },
      ]
    }).exec()
    .then((users) => {
      let responseUsers = [];
      users.forEach((u, i) => {
        u = u.toObject({ versionKey: false });
        responseUsers.push({
          _id: u._id,
          profile: u.profile
        });
      });
      res.success({users: responseUsers}, 200);
    })
    .catch((error) => {
      console.error('ERROR ##', error)
      res.error(error);
    });
  }

  approveProfileVideo(req, res, next) {
    const userId = _.get(req, ['body', 'userId']);
    const videoId = _.get(req, ['body', 'videoId']);
    const reason = _.get(req, ['body', 'reason']);
    if (_.isNil(userId) || _.isNil(videoId) || _.isNil(reason)) 
      return res.error('Insufficient parameters.');
    User.findOne({ _id: userId }).exec()
      .then((u) => {
        let updatedVideos = null;
        let videoFound = false
        if (!u) throw new Error('User just could not be found');
        const videos = _.get(u, 'profile.videos', undefined)
        if (videos) {
          updatedVideos = {};
          let currentVideos = videos.toObject({ versionKey: false });
          _.forOwn(currentVideos, (currentVideo, key) => {
            if (currentVideo._id === videoId) {
              videoFound = true
              const isApproved = _.get(currentVideo,'review.approved');
              if (isApproved === true || isApproved === false) {
                throw new Error('Video has already been reviewed');
              }
              currentVideo.review = {
                approved: true,
                reason: reason,
                adminId: null,
                date: Date.now(),
              }
            }
            _.set(updatedVideos, [key], currentVideo)
          })
        }
        if (!updatedVideos || !videoFound) throw new Error('Could not update the videos')
        _.set(u, 'profile.videos', updatedVideos);
        return u.save();
      })
      .then((u) => {
        NotificationsUseCase.videoApproved(u, videoId, reason)
        res.success('Video has been approved', 200)
      })
      .catch((e) => {
        console.error('Error#controller', e)
        res.error(e.message);
      });
  }


  rejectProfileVideo(req, res, next) {
    const userId = _.get(req, ['body', 'userId']);
    const videoId = _.get(req, ['body', 'videoId']);
    const reason = _.get(req, ['body', 'reason']);
    if (_.isNil(userId) || _.isNil(videoId) || _.isNil(reason)) 
      return res.error('Insufficient parameters.');

    User.findOne({ _id: userId }).exec()
      .then((u) => {
        let updatedVideos = null;
        let videoFound = false
        if (!u) throw new Error('User just could not be found');
        const videos = _.get(u, 'profile.videos', undefined)
        if (videos) {
          updatedVideos = {};
          let currentVideos = videos.toObject({ versionKey: false });
          _.forOwn(currentVideos, (currentVideo, key) => {
            if (currentVideo._id === videoId) {
              videoFound = true
              const isApproved = _.get(currentVideo,'review.approved');
              if (isApproved === true || isApproved === false) {
                throw new Error('Video has already been reviewed');
              }
              currentVideo.review = {
                approved: false,
                reason: reason,
                adminId: null,
                date: Date.now(),
              }
            }
            _.set(updatedVideos, [key], currentVideo)
          })
        }
        if (!updatedVideos || !videoFound) throw new Error('Could not update the videos')
        _.set(u, 'profile.videos', updatedVideos);
        return u.save();
      })
      .then((u) => {
        NotificationsUseCase.videoRejected(u, videoId, reason)
        res.success('Video has been rejected', 200)
      })
      .catch((e) => res.error(e.message));
  }

}

module.exports = new ApiController();