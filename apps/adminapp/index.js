'use strict';
const config = rootRequire('config');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const apiEndpoints = {
  admin: require('./api/v1/admin/')
};

function rootRequire(path) {
  return require(`../../${path}`)
}
express.response.success = function(response, code) {
  this.status(code ? code : 200);
  this.json({ data : response } );
};

express.response.error = function(message, code) {
  this.status(code ? code : 400);
  this.json({error : { message : message } });  
};

class AdminApp {

  constructor() {
    this.app = express();
  }

  start() {

    return this.initApp()
      .then(this.initDatabase.bind(this));
  }

  initApp() {
    return new Promise((resolve, reject) => {
      console.log('Initialising the app!', apiEndpoints.admin);
      this.app.use(cors({ origin: '*' }));
      this.app.use(bodyParser.json());
      new apiEndpoints.admin(this.app);
      this.app.use((err, req, res, next) => {
        const code = err.status || 500; 
        res.status(code);
        res.json({ code : code, message: err.message});
      });
      this.app.on('after', (req, res, route, err) => {
          if (err) { console.error('Error happened', err);}
          console.log('[Main app] Started the server.');
        })
        .listen(config.adminApp.port, () => {
          console.log('Server listening on the port', config.adminApp.port);
        });
      resolve();
    });
  }

  initDatabase() {
    var connectAttempts = 0;
    return new Promise((resolve, reject) => {

      const dbConfig = { 
        user: config.mongoDb.username,
        pass: config.mongoDb.password,
      }
      mongoose.connect(
        `${config.mongoDb.url}${config.mongoDb.collection}`,
        dbConfig
      );
      mongoose.connection.on('connected', () => {
        connectAttempts = 0;
        resolve(true);
      });
      mongoose.connection.on('error', (error) => {
        connectAttempts++;
        console.error('Mongoose connection has failed.', error);
        if (connectAttempts < 3) {
          setTimeout(function() {
            console.log('Attempting to reconnect to ', config.mongoDb.url);
            mongoose.connect(config.mongoDb.url + config.mongoDb.collection);
          }, 2500);
        } else {
          throw new Error('Could not reconnect to mongodb.');
        }
      })
    })
  }

}

module.exports = AdminApp;
