'use strict';
const _ = require('lodash')
const FCM = require('fcm-node')

const config = rootRequire('config')
const logger = rootRequire('apps/webserviceapp/helpers/logger')

function rootRequire(path) {
  const ROOT_PATH = '../../../../';
  return require(`${ROOT_PATH}${path}`)
}

class NotificationsUseCase {

  constructor() {
    this.fcm = new FCM(config.fcm.serverKey);
  }

  videoRejected(userToSendTo, videoId, reason) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));

      const deviceToken = _.get(userToSendTo, ['fcm', 'token']);
      if (deviceToken && (videoId || videoId === 0)) {      
        const payload = {
          to: deviceToken,
          data: {
            category: 'profile_video_rejected',
            video_id: videoId,
            reason: reason,
          }
        };
        logger.info(`PROFILE VIDEO APPROVED: Sending this payload ${JSON.stringify(payload)} to 
                      user ${userToSendTo._id} with device token ${deviceToken}`)
        this.fcm.send(payload, function(err, response) {
          if (err) return reject(err);
          return resolve(response);  
        });
      }
    });
  }

  videoApproved(userToSendTo, videoId, reason) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));

      const deviceToken = _.get(userToSendTo, ['fcm', 'token']);
      if (deviceToken && (videoId || videoId === 0)) {      
        const payload = {
          to: deviceToken,
          data: {
            category: 'profile_video_approved',
            video_id: videoId,
            reason: reason,
          }
        };
        logger.info(`PROFILE VIDEO APPROVED: Sending this payload ${JSON.stringify(payload)} to 
                      user ${userToSendTo._id} with device token ${deviceToken}`)
        this.fcm.send(payload, function(err, response) {
          if (err) return reject(err);
          return resolve(response);  
        });
      }
    });
  }

}

module.exports = {
  UseCase: new NotificationsUseCase(),
}
