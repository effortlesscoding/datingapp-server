
class Utils {
  static hashToArray(hash) {
    hash = hash && hash.toObject ? hash.toObject({ versionKey: false }) : hash
    let result = []
    for (let integerKey in hash) {
      const obj = hash[integerKey]
      if (obj) {
        if (obj.length && Array.isArray(obj)) {
          if (obj.length > 0) {
            result = result.concat(obj) 
          }
        }
      }
    }
    return result
  }
}

module.exports = Utils