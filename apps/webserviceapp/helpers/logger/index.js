'use strict';
var winston = require('winston');
var config = require('../../../../config');

// Depending on the environment, you will use different loggers. I thihk

var loggerConfig = {};
var environment = process.env.NODE_ENV;
if (environment) {
  if (environment === 'test') {
    loggerConfig = {
      transports: [
      ],
      exitOnError: false
    };
  } else if (environment.indexOf('production') !== -1 || environment.indexOf('staging') !== -1) {
    loggerConfig = {
      transports: [
        new (winston.transports.File)({
            filename: config.logging.consumerFile,
            level: config.logging.level,
            timestamp: function() {
              return Date.now();
            },
            json: false,
            formatter: function(options) {
              return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (undefined !== options.message ? options.message : '') +
                (options.meta && Object.keys(options.meta).length ? ' ------ '+ JSON.stringify(options.meta) : '' );
            }
        })
      ],
      exitOnError: false
    };
  } else {
    loggerConfig = {
      transports: [
        new (winston.transports.Console)()
      ],
      exitOnError: false
    };
  }
  var logger = new winston.Logger(loggerConfig);
  if (logger.transports.console) {
    logger.transports.console.level = 'debug';  
  }

}


function logDebug(message, object) {
  logger.debug(message);
  if (object) {
    if (object.toObject) {
      logger.debug(object.toObject({ versionKey : false }));
    } else {
      logger.debug(object);
    }
  }
}

function logInfo(message, object) {
  logger.info(message);
  if (object) {
    if (object.toObject) {
      logger.info(object.toObject({ versionKey : false }));
    } else {
      logger.info(object);
    }
  }
}

function logError(message, object) {
  logger.error(message);
  if (object) {
    if (object.toObject) {
      logger.error(object.toObject({ versionKey : false }));
    } else {
      logger.error(object);
    }
  }
}

module.exports = {
  debug : logDebug,
  info : logInfo,
  error : logError
};