'use strict'

const passport = require('passport')
const FacebookTokenStrategy = require('passport-facebook-token')
const config = rootRequire('config')
const logger = rootRequire('apps/webserviceapp/helpers/logger')

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}
// TODO: add another strategy, and that is JWT token... (Implement LATER)
// It probably takes access_token from the request
// Then, it tries to verify it vs the user's session (if session is there)
// If user's session doesn't have the session, checks against FB
// http://stackoverflow.com/questions/20929092/passport-js-passport-facebook-token-strategy-login-trough-js-sdk-and-then-auth
passport.use('facebook-token', new FacebookTokenStrategy({
    clientID: config.facebook.appId,
    clientSecret: config.facebook.secret,
    profileFields : ['id', 'displayName', 'gender', 'name', 'emails']
  }, function(accessToken, refreshToken, profile, done) {
    logger.info('Getting a Facebook User!');
    var user = {
        'email': profile.emails[0].value,
        'firstName' : profile.name.givenName,
        'lastName' : profile.name.familyName,
        'gender' : profile.gender,
        'id'   : profile.id,
        'accessToken': accessToken,
        'refreshToken' : refreshToken
    };
    logger.info('Facebook User is : ', user);
    return done(null, user);
  }));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

module.exports = function(app) {
  app.use(passport.initialize());
  app.use(passport.session());
};