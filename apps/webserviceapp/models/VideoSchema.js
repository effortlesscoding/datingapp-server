'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
  _id: { type: Number, required: true },
  order: Number,
  storagePath: { type: String, required: true },
  thumbnailName: { type: String, required: true },
  videoName: { type: String, required: true },
  review: {
    approved: Boolean,
    reason: String,
    adminId: { 
      type: Schema.Types.ObjectId, ref: 'AdminUser' 
    },
    date: { type: Date }
  },
}, {_id : false, timestamps: true });


module.exports = VideoSchema;