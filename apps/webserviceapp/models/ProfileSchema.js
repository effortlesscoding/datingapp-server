const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProfileSchema = new Schema({
  _id: Schema.ObjectId,
  hasVideos: Boolean, // Not very accurate / not being updated (at all / often).
  matchScore: Number,
  priority: Number,
  seqNumber: Number,
  viewed: Boolean,
}, {_id : false, timestamps: true })

module.exports = ProfileSchema