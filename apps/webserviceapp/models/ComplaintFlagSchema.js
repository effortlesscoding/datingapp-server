'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

// Definitely to be improved
const ComplaintFlagSchema = new Schema({
  reporter: {
    name: String,
    facebookId: Number,
    _id: Schema.ObjectId
  },
  reason: String
}, {_id: false, timestamps: true });

module.exports = ComplaintFlagSchema;