'use strict';
const ROOT_PATH = '../../../';
const mongoose = require('mongoose');
const Schema = mongoose.Schema; 
const Versions = new Schema({
  _id: Number,
  android: {
    minVersion: String,
    curVersion: String,
    updateMessage: String,
    currentPackage: String
  },
  ios: {
    minVersion: String,
    curVersion: String,
    updateMessage: String
  }
});
const _ = require('lodash');

const DB_ID = 1;


Versions.statics.setAndroidVersion = function(minVersion, curVersion, updateMessage, currentPackage) {
  return new Promise((resolve, reject) => {
    const model = mongoose.model('Versions', Versions);
    if (_.isEmpty(minVersion) && _.isEmpty(curVersion) &&
        _.isEmpty(updateMessage) && _.isEmpty(currentPackage))
      return reject('Not enough arguments');

    let updateQuery = {};
    if (minVersion) updateQuery['android.minVersion'] = minVersion;
    if (curVersion) updateQuery['android.curVersion'] = curVersion;
    if (updateMessage) updateQuery['android.updateMessage'] = updateMessage;
    if (currentPackage) updateQuery['android.currentPackage'] = currentPackage;

    model.update({ _id: DB_ID }, { $set: updateQuery }, {upsert: true}, 
      (err) => {
        if (err) return reject(err);
        else return resolve();
      });

  });
}

Versions.statics.setIOSVersion = function(minVersion, curVersion, updateMessage) {
  return new Promise((resolve, reject) => {
    const model = mongoose.model('Versions', Versions);

    if (_.isEmpty(minVersion) && _.isEmpty(curVersion) &&
        _.isEmpty(updateMessage) && _.isEmpty(currentPackage))
      return reject('Not enough arguments');

    let updateQuery = {};
    if (minVersion) updateQuery['ios.minVersion'] = minVersion;
    if (curVersion) updateQuery['ios.curVersion'] = curVersion;
    if (updateMessage) updateQuery['ios.updateMessage'] = updateMessage;
    model.update({ _id: DB_ID }, { $set: updateQuery}, 
    {upsert: true}, 
    (err) => {
      if (err) return reject(err);
      else return resolve();
    });
  });
}

Versions.statics.getVersions = function() {
  return mongoose.model('Versions', Versions).findOne({ _id: 1 }).exec();
}

module.exports = mongoose.model('Versions', Versions);