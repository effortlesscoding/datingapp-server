function ProfileLikeError(message) {
  Error.call(this);
  Error.captureStackTrace(this, ProfileLikeError);
  this.name = 'ProfileLikeError';
  if (!message) {
    this.message = 'Failed to like the user';
  } else {
    this.message = message;
  }
}

ProfileLikeError.prototype = Object.create(Error.prototype);

function UserSaveError(message) {
  Error.call(this);
  Error.captureStackTrace(this, UserSaveError);
  this.name = 'UserSaveError';
  if (!message) {
    this.message = 'User could not be saved.';
  } else {
    this.message = message;
  }
}

UserSaveError.prototype = Object.create(Error.prototype);

function UserLookupError(message) {
  Error.call(this);
  Error.captureStackTrace(this, UserLookupError);
  this.name = 'UserLookupError';
  if (!message) {
    this.message = message;
  } else {
    this.message = 'Could not find the user';
  }
}

UserLookupError.prototype = Object.create(Error.prototype);

function MutualLikeError(message) {
  Error.call(this);
  Error.captureStackTrace(this, MutualLikeError);
  this.name = 'MutualLikeError';
  if (!message) {
    this.message = 'Failed to verify that the like was mutual';
  } else {
    this.message = message;
  }
}

MutualLikeError.prototype = Object.create(Error.prototype);


function ConversationCreationError(message) {
  Error.call(this);
  Error.captureStackTrace(this, ConversationCreationError);
  this.name = 'ConversationCreationError';
  if (!message) {
    this.message = 'Failed to create a conversation between two users';
  } else {
    this.message = message;
  }
}

ConversationCreationError.prototype = Object.create(Error.prototype);


function FetchProfilesError(message) {
  Error.call(this);
  Error.captureStackTrace(this, FetchProfilesError);
  this.name = 'FetchProfilesError';
  if (!message) {
    this.message = 'Failed to create a conversation between two users';
  } else {
    this.message = message;
  }
}

FetchProfilesError.prototype = Object.create(Error.prototype);


function EnoughProfilesToReviewError(message) {
  Error.call(this);
  Error.captureStackTrace(this, EnoughProfilesToReviewError);
  this.name = 'EnoughProfilesToReviewError';
  if (!message) {
    this.message = 'Enough parameters to review.';
  } else {
    this.message = message;
  }
}

EnoughProfilesToReviewError.prototype = Object.create(Error.prototype);

function InvalidRequestParametersError(message) {
  Error.call(this);
  Error.captureStackTrace(this, InvalidRequestParametersError);
  this.name = 'InvalidRequestParametersError';
  if (!message) {
    this.message = 'Enough parameters to review.';
  } else {
    this.message = message;
  }
}

InvalidRequestParametersError.prototype = Object.create(Error.prototype);



function UnimplementedError(message) {
  Error.call(this);
  Error.captureStackTrace(this, UnimplementedError);
  this.name = 'UnimplementedError';
  if (!message) {
    this.message = 'Enough parameters to review.';
  } else {
    this.message = message;
  }
}

UnimplementedError.prototype = Object.create(Error.prototype);


function ArgumentError(message) {
  Error.call(this);
  Error.captureStackTrace(this, ArgumentError);
  this.name = 'ArgumentError';
  if (!message) {
    this.message = 'Arguments or parameters were invalid.';
  } else {
    this.message = message;
  }
}

ArgumentError.prototype = Object.create(Error.prototype);


function InvalidInternalData(message) {
  Error.call(this);
  Error.captureStackTrace(this, InvalidInternalData);
  this.name = 'InvalidInternalData';
  if (!message) {
    this.message = 'Internal data in the database is corrupt.';
  } else {
    this.message = message;
  }
}

InvalidInternalData.prototype = Object.create(Error.prototype);

function UserMethodError(message) {
  Error.call(this);
  Error.captureStackTrace(this, UserMethodError);
  this.name = 'UserMethodError';
  if (!message) {
    this.message = 'A user model method is incorrect/received improper input.';
  } else {
    this.message = message;
  }
}

UserMethodError.prototype = Object.create(Error.prototype);

function InvalidNetworkResponse(message) {
  Error.call(this);
  Error.captureStackTrace(this, InvalidNetworkResponse);
  this.name = 'InvalidNetworkResponse';
  if (!message) {
    this.message = 'A call to external network resource has failed.';
  } else {
    this.message = message;
  }
}

InvalidNetworkResponse.prototype = Object.create(Error.prototype);

module.exports = {
  ProfileLikeError: ProfileLikeError,
  UserSaveError: UserSaveError,
  UserLookupError: UserLookupError,
  MutualLikeError : MutualLikeError,
  ConversationCreationError : ConversationCreationError,
  FetchProfilesError : FetchProfilesError,
  EnoughProfilesToReviewError : EnoughProfilesToReviewError,
  InvalidRequestParametersError : InvalidRequestParametersError,
  ArgumentError : ArgumentError,
  UnimplementedError : UnimplementedError,
  InvalidInternalData : InvalidInternalData,
  UserMethodError : UserMethodError
};
