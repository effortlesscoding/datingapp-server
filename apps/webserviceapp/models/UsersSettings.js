const mongoose = require('mongoose');
const Schema = mongoose.Schema; 
const genders = require('./GendersEnum.js');
const logger = rootRequire('apps/webserviceapp/helpers/logger');

function rootRequire(path) {
  const ROOT_PATH = '../../../';
  return require(`${ROOT_PATH}${path}`);
}

const UsersSettings = new Schema({
  _id : Number,
  nextSeqNumber: { type: Number, default: 1 }
});

UsersSettings.statics.nextSeqNumber = function() {
  return this.findOneAndUpdate({ $inc: { nextSeqNumber: 1 } }).exec();
};

UsersSettings.statics.fixSettingsNumber = function(UserModel) {
  // Find the maximum id in Users. Set nextSeqNumber to that id + 1
  return new Promise(function(resolve, reject) {
    UserModel.findOne()
      .sort({ _id : 'desc' })
      .exec((err, doc) => {
        if (err) {
          return reject(err);
        }
        logger.debug('Current max user:');
        logger.debug(doc.toObject ? doc.toObject() : doc);
        return mongoose.model('UsersSettings', UsersSettings)
                      .findOneAndUpdate({ nextSeqNumber : doc._id + 1 }).exec();
      })
      .then((result) => {
        debugger;
        resolve(result);
      })
      .catch(reject);
  });
};

/** 
    Misleading - instead of Model, I should have used "Schema" and 
    instantiated the model in other modules, where needed 
**/
module.exports = mongoose.model('UsersSettings', UsersSettings);