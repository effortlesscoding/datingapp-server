'use strict';

const UserPreferences = {
  age: {
    min: Number,
    max: Number,
  },
  distance: Number,
  gender: {
    male: Boolean,
    female: Boolean
  }
};

module.exports = UserPreferences;