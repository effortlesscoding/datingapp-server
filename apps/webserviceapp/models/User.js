const mongoose = require('mongoose')
const _ = require('lodash')
const path = require('path')

const Schema = mongoose.Schema
const ObjectId = Schema.ObjectId

const logger = rootRequire('apps/webserviceapp/helpers/logger')
const config = rootRequire('config')
const fcmManager = rootRequire('apps/webserviceapp/helpers/fcm')

const UsersSettings = require('./UsersSettings')
const GendersEnum = require('./GendersEnum.js').enum
const UserPreferences = require('./UserPreferences')
const VideoSchema = require('./VideoSchema')
const ComplaintFlag = require('./ComplaintFlagSchema')
const ProfileSchema = require('./ProfileSchema')

function rootRequire(path) {
  const ROOT_PATH = '../../../';
  return require(`${ROOT_PATH}${path}`);
}

const ReviewQueue = new Schema({
  priority: Number,
  profiles: [ProfileSchema],
}, {_id : false})

const User = new Schema({
  seqNumber: { type: Number, unique: true, required: true },
  facebookId: { type: Number, unique: true, required: true },
  gender: { type: Number, enum: GendersEnum, required: true },
  reviewedUpTo: { type: Number, required: true, default: -1 },
  complaints: [ComplaintFlag],
  // TODO: add a "delete" date. Possibly, in the future. Either that or just do deletion weekly <-- Easier
  isDeleted: Boolean,
  profile: {
    firstName: String,
    lastName: String,
    email: String,
    videos: {
      0: VideoSchema,
      1: VideoSchema,
      2: VideoSchema,
      3: VideoSchema,
      4: VideoSchema,
      5: VideoSchema,
    },
    age: Number,
    interests: [String],
    occupation: String,
    study: String,
    aboutMe: String,
    location: {
      lat: Number,
      lng: Number
    },
  },
  fcm: {
    token: String,
  },
  preferences: UserPreferences,
  qbchat: {
    id: String,
    login: String,
    password: String
  },
  fcm: {
    token: String
  },
  profilesMatched: [ProfileSchema],
  profilesLiked: [ProfileSchema],
  profilesToReview: {
    0: [ProfileSchema],
    1: [ProfileSchema],
    2: [ProfileSchema],
    3: [ProfileSchema],
    4: [ProfileSchema],
    5: [ProfileSchema],
    6: [ProfileSchema],
  }, // hash-key with ProfileSchema values
}, { timestamps: true });

// // This may be buggy... is it being called even when you are saving a user???
User.pre('validate', function(next) {
  var doc = this;
  if (_.isNil(doc.seqNumber)) {
    UsersSettings
      .findByIdAndUpdate(config.usersSettings.id, { $inc : { nextSeqNumber : 1 } }, 
        function(err, settings) {
          if (err) { 
            logger.error('Error : ', err); 
            return next(err); 
          }
          doc.seqNumber = settings.nextSeqNumber;
          doc.lastMatchNumber = 0;
          doc.profilesMatched = [];
          doc.profilesLiked = [];
          doc.profilesToReview = [];
          next();
        });
  } else {
    next();
  }
})
 
module.exports = mongoose.model('User', User);