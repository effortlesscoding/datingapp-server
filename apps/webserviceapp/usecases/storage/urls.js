const _ = require('lodash')
const config = rootRequire('config')
const key = rootRequire('config/key.json')
const crypto = require('crypto')

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UrlsUseCase {

  static getPublicUrl(filePath) {
    const bucketName = _.get(config, 'google.storage.bucket', undefined)
    return `https://${bucketName}.commondatastorage.googleapis.com/${filePath}`
  }

  static signResource(filePath) {
    return new Promise((resolve, reject) => {
      var expiry = new Date().getTime() + 60 * 30 // 30 minutes 
      var bucketName = config.google.storage.bucket
      var stringPolicy = ['GET\n\n\n', expiry, '\n', '/', bucketName, '/', filePath].join('')
      var base64Policy = Buffer(stringPolicy, 'utf-8').toString('base64')
      var accessId = key.client_id
      var privateKey = key.private_key
      var signature = encodeURIComponent(crypto.createSign('sha256').update(stringPolicy).sign(privateKey,'base64'))
      var signedUrl = ['https://', bucketName, '.commondatastorage.googleapis.com/', filePath, 
                        '?GoogleAccessId=', accessId, '&Expires=', expiry, '&Signature=', signature].join('')
      return resolve(signedUrl)
    })
  }

}
module.exports = {
  UseCase: UrlsUseCase,
}