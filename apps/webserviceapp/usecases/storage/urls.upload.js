const _ = require('lodash')
const crypto = require('crypto')
const path = require('path')
const https = require('https')

const config = rootRequire('config')
const key = rootRequire('config/key.json')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const genders = rootRequire('apps/webserviceapp/models/GendersEnum')

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UrlsUploadUseCase {

  static getChatBaseFileName(user) {
    const timestamp = Date.now()
    return `${timestamp}`
  }

  static getChatBaseDirectory(user, otherUser) {
    let chatDirectory;
    chatDirectory = `${user._id}/${otherUser._id}`;
    return path.join('chats', chatDirectory);
  }

  static getProfileVideosBaseDirectory(user) {
    let baseDirectory;
    if (user.gender === genders.MALE) {
      baseDirectory = 'profile/males/';
      return `profile/males/${user._id}/`;
    } else if (user.gender === genders.FEMALE) {
      baseDirectory = 'profile/females/';
      return `profile/females/${user._id}/`;
    }
  }

  static getProfileVideoBaseFileName(user, videoId) {
    const videos = _.get(user, 'profile.videos', undefined)
    if (_.isNil(videoId) || videoId < 0 || videoId > config.matchRules.MAX_PROFILE_VIDEOS) {
      return null;
    } else {
      return `${videoId}`;
    }
  }

  static generateUploadUrls(parameters) {
    return new Promise((resolve, reject) => {
      if (_.isNil(parameters.fileName) || _.isNil(parameters.directory) ||
          _.isNil(parameters.mimeType) || _.isNil(parameters.contentLength) ||
          _.isNil(parameters.accessToken)) {
        return reject(new Error('The generator has not been configured properly'))
      }
      if (parameters.contentLength <= 0) {
        return reject(new Error('The content length has to be more than 0'))
      }

      const urlPath = `/upload/storage/v1/b/${config.google.storage.bucket}/o?uploadType=resumable`
      const postData = JSON.stringify({ name : path.join(parameters.directory, parameters.fileName)})
      // 'upload/storage/v1/b/' + config.gcloud.projectId + '/o?uploadType=resumable&name=' + fileName;
      const postOptions = {
        host: 'www.googleapis.com',
        path: urlPath,
        method: 'POST',
        headers: {
          'X-Upload-Content-Type': parameters.mimeType,
          'Content-Type': 'application/json; charset=UTF-8',
          'Content-Length': postData.length,
          'Authorization': `Bearer ${parameters.accessToken}`
        },
        'X-Upload-Content-Length': parameters.contentLength,
      }
      const postRequest = https.request(postOptions, function(postResponse) {
        postResponse.setEncoding('utf8');
        logger.info('Configuring post response');
        let responseData = '';
        postResponse.on('data', function (chunk) {
            responseData += chunk;
        });
        postResponse.on('end', function() {
          if (postResponse.statusCode === 200) {
            resolve(postResponse.headers.location);
          } else {
            reject(new Error('Status : ' + postResponse.statusCode));
          }
        });
        postResponse.on('error', function(err) {
          logger.error(err);
          reject(new Error('Status : ' + postResponse.statusCode));
        });
      });
      postRequest.on('error', function(err) {
        logger.error(err);
        reject(new Error('Error : ' + err.message));
      });
      logger.debug('Sending post data!');
      postRequest.write(postData);
      postRequest.end();
    });
  }

}
module.exports = {
  UseCase: UrlsUploadUseCase,
}