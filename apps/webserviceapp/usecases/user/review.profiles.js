const _ = require('lodash')
const config = rootRequire('config')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const genders = rootRequire('apps/webserviceapp/models/GendersEnum')
const User = rootRequire('apps/webserviceapp/models/User')

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)  
}

const AGE_SMALL_MARGIN = 3
const DISTANCE_SMALL_MARGIN = 5
const AGE_BIG_MARGIN = 10
const DISTANCE_BIG_MARGIN = 15

const DEFAULT_DISTANCE_PREFERENCE_KM = 3
const DEFAULT_MIN_AGE = 18
const DEFAULT_MAX_AGE = 24
let amountToFill;

class UserProfilesReviewUseCase {

  static populateWithNextProfilesToReview(user) {
    amountToFill = _.get(config, 'matchRules.MAX_PROFILES_TO_REVIEW', 100)
    if (!user) { throw new Error('No user to populate.');}
    logger.debug(`Populating this user ${user._id} with next profiles to review.`)
    return fillProfilesToReview(user)
  }

}

function fillProfilesToReview(user) {
  return getNextProfilesIteration(user)
    .then((nextUsers) => {
      if (nextUsers && nextUsers.length > 0) {
        const maxId = _.reduce(nextUsers, (currentMax, nextUser) => {
          if (nextUser.seqNumber > currentMax) return nextUser.seqNumber
          else return currentMax
        }, 0)
        user.reviewedUpTo = maxId
        return addProfilesByMatchScore(user, nextUsers)
      } else {
        if (user.reviewedUpTo !== -1) {
          user.reviewedUpTo = -1
          return fillProfilesToReview(user)
        } else {
          return user
        }
      }
    })
    .catch((e) => {
      logger.error('Error', e)
      return null
    })
}

function getNextProfilesIteration(user) {
  logger.info('getNextProfilesToReview()')
  const gendersPreferences = _.get(user, 'preferences.genders', { 
    male: user.gender === genders.FEMALE,
    female: user.gender === genders.MALE
  })
  const matchedIds = _.map(user.profilesMatched, (profile) => profile._id)
  const likedIds = _.map(user.profilesLiked, (profile) => profile._id)
  const profilesPassed = _.map(user.profilesPassed, (profile) => profile._id)
  const condition = {
    isDeleted: { $ne: true },
    _id: { $nin: _.concat(matchedIds, likedIds, profilesPassed) }
  };
  if (gendersPreferences.male && !gendersPreferences.female) {
    condition.gender = genders.MALE
  } else if (!gendersPreferences.male && gendersPreferences.female) {
    condition.gender = genders.FEMALE
  }
  const conditionForward =  _.merge({},  
    { seqNumber : { $gt: user.reviewedUpTo, $ne: user.seqNumber }}, 
    condition)
  logger.debug('conditionForward', conditionForward)
  const searchForwardQuery = User.find(conditionForward).limit(amountToFill)

  // We are not going to search from the beginning ?? 
  // I actually think we need to.
  // const searchFromTheBeginningQuery = user.constructor.find(condition).limit(amountToFill)
  return searchForwardQuery.exec()
    .then((nextUsers) => {
      logger.debug('nextUsers count:', nextUsers.length)
      if (!nextUsers || !nextUsers.length) {
        return [];
      } else {
        return nextUsers;
      }
    })
    .catch((e) => {
      logger.error('Could not get users', e)
    })
}

function addProfilesByMatchScore(currentUser, nextUsers) {
  if (!nextUsers || !nextUsers.length) {
    logger.debug('No next users.');
  } else {
    logger.debug('populateWithNextProfilesToReview():', nextUsers.length)
    let maxSeqNumber = 0
    // Change this so we can totally replace... the profiles to review
    let queues = {0: [], 1:[], 2:[], 3:[], 4:[], 5:[], 6:[]}
    nextUsers.forEach((nu, i) => {
      const matchResult = calculatePriority(currentUser, nu)
      const priorityQueue = _.get(queues, [matchResult.priority], [])
      priorityQueue.push({ 
        _id: nu._id, 
        hasVideos: _.get(nu, 'profile.videos.length', 0) > 0,
        matchScore: matchResult.matchScore, 
        priority: matchResult.priority,
        viewed: false, 
        seqNumber: nu.seqNumber,
      })
      _.set(queues, [matchResult.priority], priorityQueue)
      if (nu.seqNumber > maxSeqNumber) {
        maxSeqNumber = nu.seqNumber
      }
    })
    currentUser.reviewedUpTo = maxSeqNumber;
    _.set(currentUser, 'profilesToReview', queues)
  }
  return currentUser;
}

function getKmDistance(coord1, coord2) {
  if (_.isNil(coord1) || _.isNil(coord2)) return 200
  const lat1 = coord1.lat
  const lon1 = coord1.lng
  const lat2 = coord2.lat
  const lon2 = coord2.lng
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2-lat1);  // deg2rad below
  const dLon = deg2rad(lon2-lon1); 
  const a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  const d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function genderFromFb(fbUser) {
  if (_.toLower(gender) === 'male') {
    return genders.MALE
  } else if (_.toLower(gender) === 'female') {
    return genders.FEMALE
  }
}

function getMatchScore(user, userToReview) {
  const agePreference = {
    min: _.get(user, 'preferences.age.min', DEFAULT_MIN_AGE),
    max: _.get(user, 'preferences.age.max', DEFAULT_MAX_AGE),
  }
  const distanceDifferencePreference = _.get(user, 'preferences.distance', DEFAULT_DISTANCE_PREFERENCE_KM)
  const distanceDifference = getKmDistance(
    _.get(user, 'profile.location', undefined),
    _.get(userToReview, 'profile.location', undefined)
  )
  const userToReviewAge = _.get(userToReview, 'profile.age', 0)
  // if age fits
  if (agePreference.min <= userToReviewAge && userToReviewAge <= agePreference.max) {
    //   if distance fits
    if (distanceDifferencePreference >= distanceDifference) {
      return 100
    } else if (distanceDifferencePreference + DISTANCE_SMALL_MARGIN >= distanceDifference) {
      return 90
    } else if (distanceDifferencePreference + DISTANCE_BIG_MARGIN >= distanceDifference) {
      return 70
    } else {
      return 40
    }
  } else if (
    agePreference.min - AGE_SMALL_MARGIN <= userToReviewAge && 
    userToReviewAge <= agePreference.max + AGE_SMALL_MARGIN
  ) {
    if (distanceDifferencePreference >= distanceDifference) {
      return 90
    } else if (distanceDifferencePreference + DISTANCE_SMALL_MARGIN >= distanceDifference) {
      return 80
    } else if (distanceDifferencePreference + DISTANCE_BIG_MARGIN >= distanceDifference) {
      return 70
    } else {
      return 40
    }
  } else if (
    agePreference.min - AGE_BIG_MARGIN <= userToReviewAge && 
    userToReviewAge <= agePreference.max + AGE_BIG_MARGIN
  ) {
    if (distanceDifferencePreference >= distanceDifference) {
      return 60
    } else if (distanceDifferencePreference + DISTANCE_SMALL_MARGIN >= distanceDifference) {
      return 50
    } else if (distanceDifferencePreference + DISTANCE_BIG_MARGIN >= distanceDifference) {
      return 40
    } else {
      return 30
    }
  } else {
    return 30
  }
}

function calculatePriority(user, userToReview) {
  const score = getMatchScore(user, userToReview)
  let priority = 6
  if (90 < score && score <= 100) {
    priority = 0
  } else if (80 < score && score <= 90) {
    priority = 1
  } else if (70 < score && score <= 80) {
    priority = 2
  } else if (60 < score && score <= 70) {
    priority = 3
  } else if (50 < score && score <= 60) {
    priority = 4
  } else if (40 < score && score <= 50) {
    priority = 5
  } else {
    priority = 6
  }
  return {
    priority: priority,
    matchScore: score
  }
}

module.exports = {
  calculatePriority: calculatePriority,
  getKmDistance: getKmDistance,
  getMatchScore: getMatchScore,
  UseCase: UserProfilesReviewUseCase,
  Constants: {
    DEFAULT_DISTANCE_PREFERENCE_KM: DEFAULT_DISTANCE_PREFERENCE_KM,
    DEFAULT_MIN_AGE: DEFAULT_MIN_AGE,
    DEFAULT_MAX_AGE: DEFAULT_MAX_AGE,
    DISTANCE_SMALL_MARGIN: DISTANCE_SMALL_MARGIN,
    DISTANCE_BIG_MARGIN: DISTANCE_BIG_MARGIN,
  }
}