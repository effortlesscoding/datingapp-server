'use strict';
const _ = require('lodash')
const mongoose = require('mongoose')

const logger = rootRequire('apps/webserviceapp/helpers/logger')
const User = rootRequire('apps/webserviceapp/models/User')

const NotificationUseCase = rootRequire('apps/webserviceapp/usecases/notifications/').UseCase
const LoginUseCase = require('./login').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserMatchingProcess {

  static match(user1, user2) {
    // Remember profiles from profilesLiked
    const user2ProfileReference = user1.profilesLiked.find((pl) => pl._id.toString() === user2._id.toString())
    const user1ProfileReference = user2.profilesLiked.find((pl) => pl._id.toString() === user1._id.toString())
    if (!user1ProfileReference)
      return Promise.reject(new Error(`Cannot match ${user1._id} with ${user2._id}`))
    if (!user2ProfileReference) {
      logger.debug('user 2 did not like user 1')
      return Promise.resolve({
        user: user1,
        liked: true,
        matched: false,
      })
    }
    const updateUser1Query = User.findOneAndUpdate(
      { _id: user1._id},
      {
        $pull: { profilesLiked: { _id: user2._id }},
        $push: { profilesMatched: user2ProfileReference }
      },
      { new: true }
    )
    const updateUser2Query = User.findOneAndUpdate(
      { _id: user2._id},
      {
        $pull: { profilesLiked: { _id: user1._id }},
        $push: { profilesMatched: user1ProfileReference }
      },
      { new: true }
    )
    return Promise.all([updateUser1Query.exec(), updateUser2Query.exec()])
      .then((users) => {
        NotificationUseCase.sendMatchedNotification(users[1], users[0])
        return {
          user: users[0],
          userMatchedWith: users[1],
          liked: true,
          matched: true,
        }
      })
      .catch((e) => {
        logger.error(e)
        return e
      })
  }

  static like(userFbId, userIdToLike) {
    const pullCondition = [0, 1, 2, 3, 4, 5, 6].reduce((condition, num, i) => {
      condition[`profilesToReview.${num}`] = { _id: userIdToLike }
      return condition
    }, {})
    let updatedUser
    let userId
    return User.findOne({ facebookId: userFbId }).exec()
      .then((user) => {
        let profileLiked
        if (!user) {
          throw new Error('Cannot pass the requested profile on this user')
        }
        userId = user._id.toString()
        const rawUser = user.toObject({versionKey: false})
        for (let priority in rawUser.profilesToReview) {
          const priorityQueye = _.get(rawUser, ['profilesToReview', priority], [])
          profileLiked = _.find(priorityQueye, (p) => p._id.toString() === userIdToLike)
          if (profileLiked) break
        }
        if (!profileLiked) throw new Error(`This profile ${userIdToLike} cannot be liked by ${userFbId}`)
        return User.findOneAndUpdate(
          { _id: userId },
          { 
            $pull: pullCondition,
            $push: {
              profilesLiked: profileLiked
            }
          },
          { multi: true, new: true }
        ).exec()
      })
      .then((newUser) => {
        updatedUser = newUser
        return User.findOne({ _id: userIdToLike }).exec()
      })
      .then((userLiked) => {
        if (!userLiked) {
          return {
            user: updatedUser,
            liked: true,
            matched: false,
          }
        }
        const rawUserLiked = userLiked.toObject({ versionKey: false})
        const isLikeReciprocal = rawUserLiked.profilesLiked.some((p) => p._id.toString() === userId)
        if (isLikeReciprocal) {
          return UserMatchingProcess.match(updatedUser, userLiked)
        } else {
          return {
            user: updatedUser,
            liked: true,
            matched: false,
          }
        }
      })
      .then((matchResults) => {
        return LoginUseCase.hidePrivateFields(matchResults.user)
          .then((u) => {
            return { 
              user: u,
              liked: matchResults.liked,
              matched: matchResults.matched,
            }
          })
          .catch((e) => {
            logger.error(e)
            return e
          })
      })
      .catch((e) => {
        logger.error(e)
        return e
      })
  }

  static pass(userFbId, userIdToPass) {
    if (!mongoose.Types.ObjectId.isValid(userIdToPass)) {
      return Promise.reject(new Error('Users ids are invalid'))
    }
    const pullCondition = [0, 1, 2, 3, 4, 5, 6].reduce((condition, num, i) => {
      condition[`profilesToReview.${num}`] = { _id: userIdToPass }
      return condition
    }, {})
    logger.debug('Pull condition', JSON.stringify(pullCondition));
    const query = User.findOneAndUpdate(
      { facebookId: userFbId },
      { $pull: pullCondition },
      { multi: true, new: true }
    )
    return query.exec()
      .then((user) => {
        if (!user) {
          logger.error('pass error()', new Error(`User ${userFbId} was not found`))
          throw new Error('Could not find the user to update')
        }
        return LoginUseCase.hidePrivateFields(user)
      })
      .catch((err) => {
        logger.error('pass error()', err)
        return err
      })
  }
}

module.exports = {
  UseCase: UserMatchingProcess,
}