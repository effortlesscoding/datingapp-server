'use strict'
const _ = require('lodash')
const QB = require('quickblox')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const config = rootRequire('config')

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class QBAuthUseCase {

  static reauthenticate(user) {
    return new Promise((resolve, reject) => {
      const login = _.get(user, 'qbchat.login', undefined)
      const password = _.get(user, 'qbchat.password', undefined)
      if (!login || !password) {
        createNewCredentials(user)
          .then(resolve)
          .catch(reject)
      } else {
        let params = {
          login: user.qbchat.login,
          password: user.qbchat.password,
        }
        QB.auth.login(params, (err, qbUser) => {
          if (!err) {
            return resolve({
              error: null,
              user: user,
            })
          } else {
            createNewCredentials(user)
              .then(resolve)
              .catch(reject)
          }
        })
      }
    })
  }
  
  static setDefaultCredentials(user) {
    user = addQbChatCredentials(user)
    return new Promise((resolve, reject) => {
      let login, password
      let params = {
        login: user.qbchat.login,
        password: user.qbchat.password,
      }
      logger.debug('QBAuthUseCase: Logging a user in')
      QB.auth.login(params, (err, qbUser) => {
        if (err) {
          logger.error('QBAuthUseCase: ')
          logger.error(JSON.stringify(err))
          // NOTE: Technically, we want to "re-register" the user, but
          // Quickblox does not allow you to delete a user if it is not you
          // so it is not possible to re-create him.
          if (err.code === 401) {
            logger.error('QBAuthUseCase: registration attempt')
            // Attempt to create him in hope that he just does not exist
            return signUserUp(user, params).then(resolve, reject)
          }
          return reject(err)
        }
        logger.debug('QBAuthUseCase: Logged a user in successfully!')
        user.qbchat.login = params.login
        user.qbchat.password = params.password
        user.qbchat.id = qbUser.id
        return resolve(user)
      });
    });
  }

  static defaultLogin(user) {
    return `qb_${config.quickblox.loginPrefix}${user._id}`;
  }

  static defaultPassword(user) {
    return `qb_${user.facebookId}_${user.seqNumber}`;
  }
}

function getAttempt(user) {
  const login = _.get(user, 'qbchat.login', undefined)
  let attempt = 0
  if (login) {
    const splitParts = login.split('_')
    if (splitParts.length === 3) {
      attempt = splitParts[splitParts.length - 1]
    }
  }
  return attempt
}

function createNewCredentials(user) {
  const newLogin = `${QBAuthUseCase.defaultLogin(user)}_${getAttempt(user)}`
  let params = {
    login: newLogin,
    password: QBAuthUseCase.defaultPassword(user),
  }
  return signUserUp(user, params)
    .then((newUser) => {
      return newUser.save()
    })
    .then((newUser) => {
      return {
        error: null,
        user: newUser
      }
    })
    .catch((error) => {
      logger.error(error)
      return {
        error: error,
        user: null
      }
    })
}

function addQbChatCredentials(user) {
  if ((user.qbchat && (!user.qbchat.login || !user.qbchat.password)) ||
      !user.qbchat) {
    user.qbchat = {
      login: QBAuthUseCase.defaultLogin(user),
      password: QBAuthUseCase.defaultPassword(user),
    }
  }
  return user
}

function signUserUp(user, params) {
  return new Promise((resolve, reject) => {
    logger.debug('QBAuthUseCase: signUserUp attempt');
    logger.debug(params);
    QB.users.create(params, (err, qbUser) => {
      if (err) {
        logger.debug('QBAuthUseCase error:');
        logger.debug(err);
        return reject(err);
      }
      logger.debug('QBAuthUseCase: registration successful')
      logger.debug(qbUser)
      user.qbchat.login = params.login;
      user.qbchat.password = params.password;
      user.qbchat.id = qbUser.id;
      return resolve(user);
    });
  });
}

module.exports = {
  UseCase: QBAuthUseCase,
}