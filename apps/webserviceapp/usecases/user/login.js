'use strict';
const _ = require('lodash')
const mongoose = require('mongoose')

const logger = rootRequire('apps/webserviceapp/helpers/logger')
const Utils = rootRequire('apps/webserviceapp/helpers/utils')
const User = rootRequire('apps/webserviceapp/models/User')
const UsersSettings = rootRequire('apps/webserviceapp/models/UsersSettings')
const ProfilesReviewUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').UseCase
const UrlsUseCase = rootRequire('apps/webserviceapp/usecases/storage/urls.js').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserLoginUseCase {

  static login(userFbId, fcmToken) {

    return User.findOne({facebookId : userFbId}).exec()
      .then((user) => {
        if (!user) throw new Error('No user found');
        const profilesToReview = Utils.hashToArray(user.profilesToReview)
        if (profilesToReview.length > 0) 
          return user
        else
          return ProfilesReviewUseCase.populateWithNextProfilesToReview(user)
      })
      .then((user) => {
        if (user.isDeleted) {
          user.isDeleted = false
          return user.save()
        }
        return user.save()
      })
      .then((user) => {
        return UserLoginUseCase.hidePrivateFields(user)
      })
      .catch((error) => {
        if (error.name === 'MongoError' && error.code === 11000) {
          return UsersSettings.fixSettingsNumber(User)
        } else {
          return error
        }
      });
  }

  static hidePrivateFields(user) {
    const publicUser = user.toObject ? user.toObject({ versionKey: false}) : user
    _.unset(publicUser, 'facebookId')
    _.unset(publicUser, 'fcm')
    _.unset(publicUser, 'seqNumber')
    _.unset(publicUser, 'reviewedUpTo')
    _.unset(publicUser, 'profile.email')
    const profilesToReview = [
      [],
      [],
      [],
      [],
      [],
      [],
    ]
    const videosResponse = []
    _.forOwn(publicUser.profilesToReview, (priorityQueue, key) => {
      if (_.isArray(priorityQueue)) {
        _.set(profilesToReview, key, priorityQueue)
      } else {
        _.set(profilesToReview, key, [])
      }
    })
    const videos = _.get(publicUser, 'profile.videos', {})
    _.forOwn(videos, (video, key) => {
      const storagePath = video.storagePath.endsWith('/') ? video.storagePath : `${video.storagePath}/`
      video.videoUrl = UrlsUseCase.getPublicUrl(`${storagePath}${video.videoName}`)
      video.thumbnailUrl = UrlsUseCase.getPublicUrl(`${storagePath}${video.thumbnailName}`)
      videosResponse.push(video)
    })
    _.set(publicUser, 'profile.videos', videosResponse)
    publicUser.profilesToReview = profilesToReview
    return Promise.resolve(publicUser)
  }
}


module.exports = {
  UseCase: UserLoginUseCase,
}