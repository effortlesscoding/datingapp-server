const _ = require('lodash')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const genders = rootRequire('apps/webserviceapp/models/GendersEnum')
const User = rootRequire('apps/webserviceapp/models/User')
const UsersSettings = rootRequire('apps/webserviceapp/models/UsersSettings')
const ProfilesReviewUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').UseCase
const QBAuthUseCase = rootRequire('apps/webserviceapp/usecases/user/auth.quickblox').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)  
}

const DEFAULT_DISTANCE_PREFERENCE_KM = 3
const DEFAULT_MIN_AGE = 18
const DEFAULT_MAX_AGE = 24

class UserRegistrationUseCase {

  static prepareNewUser(fbUser, location, fcmToken) {
    return isUserUnique(fbUser)
      .then(() => UsersSettings.nextSeqNumber())
      .then((seqInfo) => createNewUser(seqInfo, fbUser, location, fcmToken))
      .then((user) => QBAuthUseCase.setDefaultCredentials(user))
      .then((user) => ProfilesReviewUseCase.populateWithNextProfilesToReview(user))
      .then((newUser) => {
        logger.debug('Saving a new user', newUser.toObject({ versionKey: false }))
        return newUser.save()
      })
      .catch((e) => {
        logger.error(e)
      })
  }
}

function isUserUnique(fbUser) {
  return new Promise((resolve, reject) => {
    User.findOne({ facebookId: fbUser.id })
    .then((u) => {
      if (u) return reject(new Error('User already exists'));
      return resolve()
    })
    .catch((e) => {
      return reject(e)
    })
  })
}

function createNewUser(seqInfo, fbUser, location, fcmToken) {
  logger.debug('Generating a new user.');
  if (_.isNil(seqInfo) || _.isNil(seqInfo.nextSeqNumber)) { 
    throw new TypeError('No seq number'); 
  }
  if (_.isNil(fbUser) || _.isNil(fbUser.id) || _.isNil(fbUser.gender)) {
    throw new TypeError(`Fb user is nil`)
  }
  const gender = convertFacebookGender(fbUser)
  const userModel = {
    facebookId: fbUser.id,
    gender: gender,
    seqNumber: seqInfo.nextSeqNumber,
    profile: {
      firstName: fbUser.firstName || '',
      lastName: fbUser.lastName || '',
      email: fbUser.email || '',
      videos: [],
      age: fbUser.age || 0,
      interests: [],
      occupation: '',
      study: '',
      aboutMe: ''
    },
    preferences: {
      age: {
        min: DEFAULT_MIN_AGE,
        max: DEFAULT_MAX_AGE,
      },
      distance: DEFAULT_DISTANCE_PREFERENCE_KM,
      gender: {
        male: gender === genders.FEMALE,
        female: gender === genders.MALE,
      }
    },
    reviewedUpTo: 0
  };
  if (_.get(location, 'lat', undefined) !== undefined &&
      _.get(location, 'lng', undefined) !== undefined) {
    userModel.location = {
      lat: location.lat,
      lng: location.lng,
    }
  } else {
    userModel.location = null
  }
  if (fcmToken) {
    userModel.fcm = {
      token: fcmToken
    }
  } else {
    userModel.fcm = null
  }
  logger.info('Registering a new user:');
  logger.info(userModel);
  return new User(userModel);
}

function convertFacebookGender(fbUser) {
  if (_.toLower(fbUser.gender) === 'male') {
    return genders.MALE
  } else if (_.toLower(fbUser.gender) === 'female') {
    return genders.FEMALE
  } else {
    logger.error(`Unknown gender ${fbUser.gender}`)
    return genders.FEMALE
  }
}

module.exports = {
  UseCase: UserRegistrationUseCase,
}