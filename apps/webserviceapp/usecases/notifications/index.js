'use strict';
const _ = require('lodash');
const FCM = require('fcm-node');

const config = rootRequire('config');
const logger = rootRequire('apps/webserviceapp/helpers/logger');

function rootRequire(path) {
  const ROOT_PATH = '../../../../';
  return require(`${ROOT_PATH}${path}`)
}

class NotificationsUseCase {

  constructor() {
    this.fcm = new FCM(config.fcm.serverKey);
  }

  sendMessage(to, title, message) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));
      const payload = {
        to: to,
        notification: {
          title: title,
          message: message
        }
      };

      this.fcm.send(payload, function(err, response) {
        if (err) return reject(err);
        return resolve(response);  
      });
    });
  }
  
  sendUnmatchedNotification(userToSendTo, userUnmatched) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));

      const deviceToken = _.get(userToSendTo, ['fcm', 'token']);
      const unmatchedId = _.get(userUnmatched, ['_id']);
      if (deviceToken && unmatchedId) {      
        const payload = {
          to: deviceToken,
          data: {
            category: 'unmatched',
            unmatch_id: unmatchedId
          }
        };
        logger.info(`UNMATCH: Sending this payload ${JSON.stringify(payload)} to 
                      user ${userToSendTo._id} with device token ${deviceToken}`)
        this.fcm.send(payload, (err, response) => {
          if (err) return reject(err);
          return resolve(response);  
        });
      } else {
        logger.info('Send unmatch notification failed.');
      }
    });
  }

  sendMatchedNotification(userToSendTo, userMatched) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));
      const deviceToken = _.get(userToSendTo, ['fcm', 'token']);
      const matchedWithId = _.get(userMatched, ['_id']);
      if (deviceToken && matchedWithId) {      
        const payload = {
          to: deviceToken,
          notification: {
            title: 'A new match!', 
            body: 'Nice! You have a new match! Check who it is' 
          },
          data: {
            category: 'new_match',
            match_id: matchedWithId
          }
        };
        logger.info(`MATCH: Sending this payload ${JSON.stringify(payload)} to 
                      user ${userToSendTo._id} with device token ${deviceToken}`)
        this.fcm.send(payload, function(err, response) {
          logger.info('Send result:');
          logger.info(err);
          logger.info(response);
          if (err) return reject(err);
          return resolve(response);  
        });
      }
    });
  }

  /**
   * @brief Sends a notification about a new chat message
   * @details Sends a push notification about a new chat message
   * 
   * @param  to: device token
   */
  sendChatNotification(user) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised'));
      const deviceToken = _.get(user, ['fcm', 'token']);
      if (_.isEmpty(deviceToken)) return reject(new Error('No token provided.'));
      const payload = {
        to: deviceToken,
        notification: {
            title: 'You have a new message!', 
            body: 'One of your matches sent you a message!' 
        },
        data: {
          category: 'new_chat_message'
        }
      };

      logger.info(`CHAT NOTIFICATION: Sending this payload ${JSON.stringify(payload)} to 
                    user ${user._id} with device token ${deviceToken}`)
      this.fcm.send(payload, function(err, response) {
        logger.info('Send result:');
        logger.info(err);
        logger.info(response);
        if (err) return reject(err);
        return resolve(response);  
      });

    });
  }

  sendData(to, data) {
    return new Promise((resolve, reject) => {
      if (!this.fcm) return reject(new Error('FCM not initialised.'));

      const payload = {
        to: to,
        data: data
      };

      this.fcm.send(payload, function(err, response) {
        if (err) return reject(err);
        return resolve(response);  
      });
    });
  }

}

module.exports = {
  UseCase: new NotificationsUseCase(),
}
