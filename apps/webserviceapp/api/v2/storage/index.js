/*jslint node : true */
/*jslint esversion : 6 */
'use strict';
var controller = require('./controller');
var passport = require('passport');
var googleJwtAuth = require('./google.jwt.auth.js');
var express = require('express');

module.exports = function(app) {
  var router = express.Router();

  /**
   * @apiDefine Storage Storage API endpoints
   *
   * "Storage" APIs APIs to operate with the storage
   * 
   */

  /**
   * @api {GET} /api/v2/storage/signedUrl?filePath=:filePath Generate a signed url to access a media resource
   * @apiName Generate a signed url to access a media resource
   * @apiGroup Storage
   * 
   * @apiExample {curl} Example usage:
   *     curl -i https://apiendpoint.com/api/v2/storage/signedUrl?filePath=somewhere/somefile.mp4
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * 
   * {
   *   "data": "https://lalalala.commondatastorage.googleapis.com/somethingafterthis"
   * }
   */
  router.get('/signedUrl', passport.authenticate('facebook-token'), controller.getSignedUrl)
  
  /**
   * @api {GET} /api/v2/storage/chats/videos/:withId/uploadUrls Gets upload urls for a chat videos
   * @apiName Gets upload urls for a chat videos
   * @apiGroup Storage
   */
  router.get(
    '/chats/videos/:opponentId/uploadUrls',
    passport.authenticate('facebook-token'), 
    googleJwtAuth.authorise.bind(googleJwtAuth),
    controller.getChatVideoUploadUrls
  )
  
  /**
   * @api {GET} /api/v2/storage/videos/:profileVideoId/uploadUrls Gets upload urls for a video
   * @apiName Gets upload urls for a video
   * @apiGroup Storage
   */
  router.get(
    '/profiles/videos/:profileVideoId/uploadUrls',
     passport.authenticate('facebook-token'), 
    googleJwtAuth.authorise.bind(googleJwtAuth),
    controller.getProfileVideoUploadUrls
  )

  app.use('/api/v2/storage', router);
};