'use strict';

const google = require('googleapis')

const config = rootRequire('config')
const key = rootRequire('config/key.json')
const logger = rootRequire('apps/webserviceapp/helpers/logger');

const OAuth2 = google.auth.OAuth2
const scopes = [
  'https://www.googleapis.com/auth/devstorage.full_control'
];
const jwtClient = new google.auth.JWT(key.client_email, null, key.private_key, scopes, null);

let cachedTokens = null;

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

// NICE, now I HAVE a TOKEN !!!
// Now, I need to use this token to... generate a url!
class GoogleJWTAuthoriser {

  authorise(req, res, next) {
    logger.info('Authorising!');
    const timestamp = Date.now();
    if (!cachedTokens || !cachedTokens.access_token || !cachedTokens.expiry_date) {
      logger.info('Token never existed!');
      this.obtainTokens(req, res, next);
    } else {
      const expiryThreshold = 1000 * 60 * 30; // 30 minutes 
      if (timestamp + expiryThreshold > cachedTokens.expiry_date) {
        logger.info('Token has expired!');
        this.obtainTokens(req, res, next);
      } else {
        req.auth = cachedTokens;
        next();
      }
    }
  }

  obtainTokens(req, res, next) {
    logger.info('Obtaining tokens!');
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        logger.info(err);
        res.error(err.message);
        return;
      }
      logger.info('Tokens obtained!');
      cachedTokens = tokens;
      logger.info(cachedTokens);
      req.auth = tokens;
      next();
    }); 
  }
}

module.exports = new GoogleJWTAuthoriser();