'use strict';
const _ = require('lodash')
const gcloud = require('gcloud')
const crypto = require('crypto')

const config = rootRequire('config')
const key = rootRequire('config/key.json')
const User = rootRequire('apps/webserviceapp/models/User')
const genders = rootRequire('apps/webserviceapp/models/GendersEnum')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const UrlsUseCase = rootRequire('apps/webserviceapp/usecases/storage/urls').UseCase
const UrlGenerator = rootRequire('apps/webserviceapp/usecases/storage/urls.upload').UseCase

const VIDEO_SIZE_LIMIT = 62914560 // 60 MB
const THUMBNAIL_SIZE_LIMIT = 2097152
const bucket = gcloud
              .storage({
                projectId : config.google.storage.projectId,
                keyFilename : 'config/key.json'
              })
              .bucket(config.google.storage.bucket);

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class GCSController {

  getChatVideoUploadUrls(req, res, next) {
    const timestamp = Date.now();
    const userFacebookId = _.get(req, ['user', 'id'])
    const opponentId = _.get(req, ['params', 'opponentId'])
    const accessToken = _.get(req, ['auth', 'access_token'])
    const videoSize = _.get(req, ['query', 'videoSize'])
    const thumbnailSize = _.get(req, ['query', 'thumbnailSize'])
    if (parseInt(videoSize) >= VIDEO_SIZE_LIMIT) {
      return res.error('Video must not be above 60MB')
    }
    if (parseInt(thumbnailSize) >= THUMBNAIL_SIZE_LIMIT) {
      return res.error('Thumbnail must not be above 2MB')
    }

    let baseFileName, baseFileDirectory, thumbnailFileName, videoFileName, myUser
    if (!opponentId || !accessToken || _.isNil(userFacebookId)) { 
      return res.error('Not enough parameters.', 400)
    }
    if (!thumbnailSize || !videoSize) {
      return res.error('Resources sizes are needed')
    }
    User.findOne({ facebookId: userFacebookId, }).exec()
      .then((u) => {
        if (!u) throw new Error('This user does not exist.');
        myUser = u;
        return User.findOne({ _id: opponentId}).exec();
      })
      .then((otherUser) => {
        if (!otherUser) throw new Error('The other user does not exist.')
        baseFileName = UrlGenerator.getChatBaseFileName(myUser)
        baseFileDirectory = UrlGenerator.getChatBaseDirectory(myUser, otherUser)
        if (!baseFileName || !baseFileDirectory) {
          throw new Error('Empty file names')
        }
        videoFileName = `${baseFileName}.mp4`
        thumbnailFileName = `${baseFileName}.jpg`
        const videoUrlParameters = {
          fileName: videoFileName,
          directory: baseFileDirectory,
          mimeType: 'video/mp4',
          contentLength: videoSize,
          accessToken: accessToken
        }
        const thumbnailUrlParameters = {
          fileName: thumbnailFileName,
          directory: baseFileDirectory,
          mimeType: 'image/jpeg',
          contentLength: thumbnailSize,
          accessToken: accessToken
        }
        return Promise.all([
          UrlGenerator.generateUploadUrls(videoUrlParameters),
          UrlGenerator.generateUploadUrls(thumbnailUrlParameters)
        ]);
      })
      .then((urls) => {
        const thumbnailPath = `${baseFileDirectory}/${thumbnailFileName}`
        const videoPath = `${baseFileDirectory}/${videoFileName}`
        res.success({
          video: {
            accessUrl: UrlsUseCase.getPublicUrl(videoPath),
            uploadUrl: urls[0],
            mimeType: 'video/mp4',
            fileName: videoFileName,
            storagePath: baseFileDirectory,
            size: videoSize
          },
          thumbnail: {
            accessUrl: UrlsUseCase.getPublicUrl(thumbnailPath),
            uploadUrl: urls[1],
            mimeType: 'image/jpeg',
            fileName: thumbnailFileName,
            storagePath: baseFileDirectory,
            size: thumbnailSize,
          }
        })
      })
      .catch((e) => {
        logger.error(e)
        res.error(e)
      });
  }

  getProfileVideoUploadUrls(req, res, next) {
    logger.debug('getProfileVideoUploadUrls():')
    const accessToken = _.get(req, ['auth', 'access_token'])
    if (_.isNil(accessToken)) {
      return res.error('Access token has to be provided!')
    }
    const profileVideoId = _.get(req, ['params', 'profileVideoId'])
    const videoSize = _.get(req, ['query', 'videoSize'])
    const thumbnailSize = _.get(req, ['query', 'thumbnailSize'])
    if (_.isNil(profileVideoId) || _.isNil(videoSize) || _.isNil(thumbnailSize) ||
      parseInt(videoSize) === 0 || parseInt(thumbnailSize) === 0) { 
      return res.error('Profile video parameters are invalid.')
    }
    if (profileVideoId * 1 < 0 || profileVideoId * 1 > config.matchRules.MAX_PROFILE_VIDEOS) {
      return res.error('Invalid out of bounds profile video id.')
    }
    
    if (parseInt(videoSize) >= VIDEO_SIZE_LIMIT) {
      return res.error('Video must not be above 20MB')
    }
    if (parseInt(thumbnailSize) >= THUMBNAIL_SIZE_LIMIT) {
      return res.error('Thumbnail must not be above 2MB')
    }

    let videoUrl, thumbnailUrl, baseFileName, baseFileDirectory
    User.findOne({ facebookId : req.user.id}).exec()
      .then((u) => {
        if (!u) return res.error('No user found')
        baseFileName = UrlGenerator.getProfileVideoBaseFileName(u, profileVideoId)
        baseFileDirectory = UrlGenerator.getProfileVideosBaseDirectory(u)
        if (!baseFileDirectory || !baseFileName) {
          throw new Error('File names are empty')
        }
        const videoUrlParameters = {
          fileName: `${baseFileName}.mp4`,
          directory: baseFileDirectory,
          mimeType: 'video/mp4',
          contentLength: videoSize,
          accessToken: accessToken
        };
        return UrlGenerator.generateUploadUrls(videoUrlParameters)
      })
      .then((location) => {
        videoUrl = location;
        const thumbnailUrlParameters = {
          fileName: `${baseFileName}.jpg`,
          directory: baseFileDirectory,
          mimeType: 'image/jpeg',
          contentLength: thumbnailSize,
          accessToken: accessToken
        }
        return UrlGenerator.generateUploadUrls(thumbnailUrlParameters)
      })
      .then((location) => {
        thumbnailUrl = location
        const thumbnailPath = `${baseFileDirectory}/${baseFileName}.jpg`
        const videoPath = `${baseFileDirectory}/${baseFileName}.mp4`
        res.success({
          video: {
            accessUrl: UrlsUseCase.getPublicUrl(videoPath),
            uploadUrl: videoUrl,
            storagePath: baseFileDirectory,
            fileName: `${baseFileName}.mp4`,
            size: videoSize,
            mimeType: 'video/mp4'
          },
          thumbnail: {
            accessUrl: UrlsUseCase.getPublicUrl(thumbnailPath),
            uploadUrl: thumbnailUrl,
            storagePath: baseFileDirectory,
            fileName: `${baseFileName}.jpg`,
            size: thumbnailSize,
            mimeType: 'image/jpeg'
          }
        })
      })
      .catch((e) => {
        logger.error('Error happened', e)
        res.error(e)
      })
  }

  getSignedUrl(req, res, next) {
    const filePath = _.get(req, 'query.filePath', undefined)
    if (!filePath) { return res.error('No required parameters'); }
    UrlsUseCase.signResource(filePath)
      .then((url) => {
        res.success(url)
      })
      .catch((e) => {
        logger.error(`Could not sign the resource ${filePath}`, e)
        res.error(`Could not sign the resource ${filePath}`)
      })
  };

  isVideoUploaded(req, res, next) {
    res.error('Not implemented');
  }
  
  deleteVideo(req, res, next) {
    res.error('Not implemented');
  };
};

module.exports = new GCSController();