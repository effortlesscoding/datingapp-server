'use strict';
const _ = require('lodash')
const fs = require('fs')
const jsrsasign = require('jsrsasign')
const QB = require('quickblox')

const config = rootRequire('config')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const User = rootRequire('apps/webserviceapp/models/User')
const QBAuthUseCase = rootRequire('apps/webserviceapp/usecases/user/auth.quickblox').UseCase;
const Fcm = rootRequire('apps/webserviceapp/usecases/notifications').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}


class ChatController {
  
  // @DEPRECATED - this was for Layer
  register(req, res, next) {
    if (!req.user || !req.user.id) { return res.error('Unauthenticated', 401); }
    /** go through the User registration again! **/
    User.findOne({facebookId: req.user.id}).exec()
      .then(registerQbChat)
      .then(saveUser)
      .then(respondWithSuccess, respondWithError);

    function saveUser(user) {
      return new Promise((resolve, reject) => {
        user.save()
        .then((u) => {
          debugger;
          resolve(u);
        })
        .then(null, reject);
      });
    }

    function respondWithSuccess(user) {
      let response = user.toObject({versionKey: false});
      debugger;
      res.success(response, 201);
    }

    function respondWithError(error) {
      logger.error(error);
      res.error('Could not sign up to chats.');
    }

    function registerQbChat(user) {
      logger.info('User:');
      logger.info(user.toObject({}));
      return new QBAuthUseCase(user).setDefaultCredentials();
    }
  };

  // @DEPRECATED - this was for Layer
  authenticate(req, res, next) {
    var userId = req.body.userId;
    var nonce = req.body.nonce;

    logger.debug('Nonce received:');
    logger.debug(nonce);

    if (!userId) return res.error('Missing `user_id` body parameter.');
    if (!nonce) return res.error('Missing `nonce` body parameter.');

    if (!config.layerApi.layerProviderID) return res.error('Couldn\'t find LAYER_PROVIDER_ID', 500);
    if (!config.layerApi.layerKeyID) return res.error('Couldn\'t find LAYER_KEY_ID', 500);
    if (!config.layerApi.privateKey) return res.error('Couldn\'t find Private Key', 500);
    
    var header = JSON.stringify({
      typ: 'JWT',           // Expresses a MIMEType of application/JWT        
      alg: 'RS256',         // Expresses the type of algorithm used to sign the token, must be RS256
      cty: 'layer-eit;v=1', // Express a Content Type of application/layer-eit;v=1
      kid: config.layerApi.layerKeyID
    });

    var currentTimeInSeconds = Math.round(new Date() / 1000);
    var expirationTime = currentTimeInSeconds + 10000;

    var claim = JSON.stringify({
      iss: config.layerApi.layerProviderID,       // The Layer Provider ID
      prn: User.getLayerUserId(userId),                // User Identifier
      iat: currentTimeInSeconds,  // Integer Time of Token Issuance 
      exp: expirationTime,        // Integer Arbitrary time of Token Expiration
      nce: nonce                  // Nonce obtained from the Layer Client SDK
    });


    var jws = null;
    try {
      jws = jsrsasign.jws.JWS.sign('RS256', header, claim, config.layerApi.privateKey);
    } catch(e) {
      return res.error('Could not create signature. Invalid Private Key: ' + e, 500);
    }

    let identity_token = {
      identity_token: jws
    };
    logger.debug(`Identity token is ${identity_token}`);
    res.success(identity_token);
  };

  sendTestMessage(req, res, next) {
    Fcm.sendMessage(req.body.token, 'Hey', 'Buddy')
      .then(() => res.success(''))
      .catch((e) => {
        console.error('Error:', e)
        res.error(e.message)
      })
  }

  sendChatNotification(req, res, next) {
    const toUserId = _.get(req, ['body', 'to']);
    User.findOne({ _id: toUserId }).exec()
      .then((u) => {
        return Fcm.sendChatNotification(u);
      })
      .then(() => {
        return res.success('Sent!');
      })
      .catch((e) => {
        logger.error(e);
        return res.error('Could not send a notification to the user');
      });
  }

}

module.exports = new ChatController();