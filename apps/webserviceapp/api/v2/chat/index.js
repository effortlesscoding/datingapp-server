'use strict';

const controller = require('./controller');
const passport = require('passport');

module.exports = function(app) {

  app.post('/api/v2/chat/authenticate', passport.authenticate('facebook-token'), controller.authenticate);
  app.post('/api/v2/chat/register', passport.authenticate('facebook-token'), controller.register);
  app.post('/api/v2/chat/notification', controller.sendChatNotification);
  app.post('/api/v2/chat/test', controller.sendTestMessage);
  
  require('./fcm')(app);
};