'use strict';

var controller = require('./controller');
var passport = require('passport');

module.exports = function(app) {

  app.post('/api/v1/chat/fcm/token', passport.authenticate('facebook-token'), controller.registerToken);
  app.post('/api/v1/chat/fcm/test', controller.testNotification);
  app.post('/api/v1/chat/fcm/testMatched', controller.testMatchedNotification);
  
};