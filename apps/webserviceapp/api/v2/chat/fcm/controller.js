'use strict'
const _ = require('lodash')

const User = rootRequire('apps/webserviceapp/models/User')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const Fcm = rootRequire('apps/webserviceapp/helpers/fcm')


function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class FcmController {

  testMatchedNotification(req, res, next) {
    const token = _.get(req, ['body', 'token']);
    if (!token) return res.error('No token provided.');
    const userTo = {
      _id: 1,
      fcm: {
        token: token
      }
    };
    const userMatchedWith = {
      _id: 2
    };
    Fcm.sendMatchedNotification(userTo, userMatchedWith)
      .then(() => {
        res.success('Sent');
      })
      .catch((e) => {
        res.error(e);
      });
  }

  testNotification(req, res, next) {  
    if (req.body && req.body.fcmToken) {
      Fcm.sendData(req.body.fcmToken, {
        message: 'You have got a new match!',
        icon: 'frog_pile.png'
      });
      res.success('200');
    } else {
      res.error('No valid arguments provided');
    }
    //Fcm.sendMessage(REGISTRATION_TOKEN, 'Hola');
  }

  registerToken(req, res, next) {
    // Validate that the request has the required parameters
    if (!req.body) return res.error('Not a POST request.');
    if (!req.body.fcmToken) return res.error('Not enough arguments supplied.');
    if (!req.user || !req.user.id) { return res.error('Unauthenticated', 401); }

    // Find the user by that id
    User.findOne({facebookId: req.user.id}).exec()
      .then(saveToken)
      .then(sendBackSuccess)
      .catch(sendBackError);

    function saveToken(user) {
      if (!user) throw new Error('No user found in the database.');

      if (!user.fcm) 
        user.fcm = { token: req.body.fcmToken };
      else
        user.fcm.token = req.body.fcmToken;

      return user.save();
    }

    function sendBackSuccess(user) {
      res.success('Token registered.');
    }

    function sendBackError(error) {
      logger.error(error);
      res.error(error);
    }

  }
}

module.exports = new FcmController();
