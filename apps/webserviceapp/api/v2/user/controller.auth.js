'use strict'
const _ = require('lodash')

const logger = rootRequire('apps/webserviceapp/helpers/logger');

const UserModel = rootRequire('apps/webserviceapp/models/User')
const UserQuickbloxUseCase = rootRequire('apps/webserviceapp/usecases/user/auth.quickblox').UseCase
const UserRegistrationUseCase = rootRequire('apps/webserviceapp/usecases/user/registration').UseCase
const UserLoginUseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase
const ProfilesReviewUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserAuthController {

  registerUser(req, res, next) {
    const fbUserId = _.get(req, 'user.id', undefined)
    const fbUserGender = _.get(req, 'user.gender', undefined)
    if (_.isNil(fbUserId) || _.isNil(fbUserGender))
      return res.error('Could not register a user')
    logger.debug('Registering a user', req.user)
    return UserRegistrationUseCase.prepareNewUser(req.user)
      .then((savedUser) => UserLoginUseCase.hidePrivateFields(savedUser))
      .then((user) => {
        user = user.toObject ? user.toObject({ versionKey: false }) : user
        return res.success(user, 201)
      })
      .catch((err) => {
        console.error('Reason', err)
        logger.error(err);
        return res.error('Failed to create a new user')
      })
  }
  
  login(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined);
    // TODO: Need to set up some tests with this fcmToken... In headers??
    if (_.isNil(userFbId)) {
      return res.error('No user provided', 401);
    }
    return UserLoginUseCase.login(userFbId)
      .then((user) => {
        res.success(user)
      })
      .catch((e) => {
        logger.error(e)
        if (e.message === 'No user found') {
          res.error(e.message, 401)
        } else {
          res.error('Could not login')
        }
      })
  }

  fixQuickblox(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined)
    if (_.isNil(userFbId)) {
      return res.error('No user provided', 401)
    }
    let originalQbLogin, originalQbPassword
    UserModel.findOne({ facebookId: userFbId }).exec()
      .then((u) => {
        if (!u) {
          return res.error('No user found', 401)
        }
        originalQbLogin = _.get(u, 'qbchat.login', undefined)
        originalQbPassword = _.get(u, 'qbchat.password', undefined)
        return UserQuickbloxUseCase.reauthenticate(u)
      })
      .then((result) => {
        if (result.error || !result.user) {
          logger.error('fixQuickblox#then', result.error)
          return res.error('Could not verify user chat credentials')
        }
        const qbchat = _.get(result, 'user.qbchat', undefined)
        if (qbchat) {
          if (qbchat.login === originalQbLogin &&
              qbchat.password === originalQbPassword) {
            // 304 status doesn't return the body, apparently. Express gimmick
            return res.success({}, 304)
          } else {
            return res.success(qbchat) 
          }
        } else {
          return res.error('Could not verify user chat credentials')
        }
      })
      .catch((error) => {
        logger.error('fixQuickblox#catch', error)
        res.error('Could not verify chat credentials')
      })
  }
}

module.exports = new UserAuthController()