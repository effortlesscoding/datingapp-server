'use strict';
const _ = require('lodash')
const mongoose = require('mongoose')

const config = rootRequire('config');
const logger = rootRequire('apps/webserviceapp/helpers/logger');
const Utils = rootRequire('apps/webserviceapp/helpers/utils');

const User = rootRequire('apps/webserviceapp/models/User');

const NotificationsUseCase = rootRequire('apps/webserviceapp/usecases/notifications/').UseCase
const MatchingProcessUseCase = rootRequire('apps/webserviceapp/usecases/user/matching.process').UseCase
const ProfilesReviewUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserMatchingController {

  getNextProfiles(req, res, next) {
    const userFbId = _.get(req, 'user.id');
    if (_.isNil(userFbId)) return res.error('no user', 401);
    User.findOne({ facebookId: userFbId }).exec()
      .then((user) => {
        if (!user) throw new Error('Invalid user requested')
        return ProfilesReviewUseCase.populateWithNextProfilesToReview(user)
      })
      .then((user) => {
        return user.save()
      })
      .then((user) => {
        return res.success(user.profilesToReview.toObject({ versionKey: false }));
      })
      .catch((err) => {
        logger.error('Error', err)
        if (err.message === 'Invalid user requested') {
          return res.error(err.message, 401);
        } else {
          return res.error('Could not get the user', 400); 
        }
      })
  }

  likeProfile(req, res, next) {
    const fbId = _.get(req, ['user', 'id'], undefined)
    const userIdToLike = _.get(req, 'body.idToLike', undefined)
    if (!fbId || !userIdToLike) {
      return res.error('Invalid parameters')
    }
    MatchingProcessUseCase.like(fbId, userIdToLike)
      .then((result) => {
        res.success({
          user: result.user,
          liked: result.liked,
          matched: result.matched,
        })
      })
      .catch((e) => {
        logger.error(e)
        res.error('Could not pass the user')
      })
  }

  passProfile(req, res, next) {
    const fbId = _.get(req, ['user', 'id'], undefined)
    const userIdToPass = _.get(req, 'params.userIdToPass', undefined);
    if (!fbId || !userIdToPass) {
      return res.error('Invalid parameters')
    }
    MatchingProcessUseCase.pass(fbId, userIdToPass)
      .then((u) => {
        res.success({
          passed: true,
          user: u,
        })
      })
      .catch((e) => {
        logger.error(e)
        if (e.message === 'Could not find the user to update') {
          res.error('Could not update the user', 401)
        } else {
          res.error('Could not pass the user') 
        }
      })
  }
  
  unmatchProfile(req, res, next) {
    let errors
    let myUser, otherUser
    req.checkParams('with', 'You have not specified who you are unmatching with').notEmpty()
    req.getValidationResult()
      .then((errs) => {
        if (!errs.isEmpty()) {
          errors = errs.mapped()
          throw new Error('Validation failed')
        }
      })
      .then(() => {
        return Promise.all([
          User.findOne({ facebookId: req.user.id, }).exec(),
          User.findOne({ _id: req.params.with, }).exec()
        ])
      })
      .then((users) => {
        const firstUser = users[0]
        const secondUser = users[1]
        if (_.isNil(firstUser) || _.isNil(secondUser)) {
          throw new Error('Could not find users')
        }
        return User.findOneAndUpdate(
          {
            facebookId: req.user.id
          }, 
          {
            $pull: {
              profilesMatched: {
                _id: req.params.with
              }
            }
          },
          { upsert: false, new: true, }
        )
      })
      .then((updatedUser) => {
        myUser = updatedUser
        return User.findOneAndUpdate(
          {
            _id: req.params.with
          },
          {
            $pull: {
              profilesMatched: {
                _id: updatedUser._id
              }
            }
          },
          { upsert: false, }
        )
      })
      .then((result) => {
        otherUser = result
        console.log('Unmatched1!!')
        NotificationsUseCase.sendUnmatchedNotification(otherUser, myUser)
        res.success('Unmatched')
      })
      .catch((e) => {
        logger.error(e)
        res.error(errors || e.message)
      })
  }
}

module.exports = new UserMatchingController();