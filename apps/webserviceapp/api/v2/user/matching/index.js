'use strict';
var controller = require('./controller')
var passport = require('passport')
var express = require('express')

module.exports = function(app) {

  var router = express.Router();
  router.post('/like', passport.authenticate('facebook-token'), controller.like);
  router.post('/pass', passport.authenticate('facebook-token'), controller.pass);
  router.delete('/match', passport.authenticate('facebook-token'), controller.unmatch);

  app.use('/api/v1/user/matching', router);
};