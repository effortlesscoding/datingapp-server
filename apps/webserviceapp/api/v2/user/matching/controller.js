'use strict';
const User = rootRequire(`apps/webserviceapp/models/User`);
const logger = rootRequire(`apps/webserviceapp/logger`);
const fcmManager = rootRequire(`apps/webserviceapp/helpers/fcm`);
const _ = require('lodash');

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../';
  return require(`${ROOT_PATH}${path}`)
}
 
class UserMatchingController {

  like(req, res, next) {
    const userId = _.get(req, ['user', 'id']);
    const likedId = _.get(req, ['body', 'idToLike']); 

    logger.info('[Matching like] Liking user with facebook id : ', userId);

    if (_.isNil(userId) || _.isNil(likedId)) 
      return res.error('Cannot like - not enough parameters');

    User.findOne({ facebookId : userId}).exec()
    .then((user) => {
      if (user) {
        return user.likeProfile(likedId);
      } else {
        throw new Error('Could not find the user');
      }
    })
    .then((result) => {
      logger.debug('Like response:');
      if (result.toObject) {
        const resultParsed = result.toObject({versionKey : false})
        logger.debug(resultParsed);
      } else {
        logger.debug(result);
      }
      if (result.matched) {
        // Send a push notification to:
        // the guy you liked
      }
      res.success(result, 200);
    })
    .then(null, (error) => {
      logger.error(error);
      res.error('Could not like the profile.', 400);
    });
  }

  pass(req, res, next) {
    const userFbId = _.get(req, ['user', 'id']);
    const passedId = _.get(req, ['body', 'idToPass']); 
    if (_.isNil(userFbId) || _.isNil(passedId)) 
      return res.error('Cannot pass - not enough parameters');

    User.findOne({ facebookId : userFbId}).exec()
    .then(function(user) {
      if (user) {
        return user.passProfile(passedId);
      } else {
        throw new Error('Could not find the user');
      }
    })
    .then((result) => {
      if (result) {
        res.success({isPassed: true});
      } else {
        res.success({isPassed: false});
      }
    })
    .then(null, function(error) {
      logger.error(error);
      logger.info(error);
      res.error('Could not like the profile.');
    });
  };

  unmatch(req, res, next) {
    const unmatchId = _.get(req, ['body', '_id']);
    const myUserFbId = _.get(req, ['user', 'id']);
    const myUserPromise = User.findOne({ facebookId: myUserFbId }).exec();
    const unmatchedUserPromise = User.findOne({ _id: unmatchId }).exec();

    let myUser, unmatchedUser;
    Promise.all([myUserPromise, unmatchedUserPromise])
      .then((results) => {
        myUser = results[0];
        unmatchedUser = results[1];

        const updateOtherUser = User.update({ _id: unmatchId}, { $pull: { 'profilesMatched': { _id: myUser._id }}}, { safe: true }).exec();
        const updateMyUser = User.update({ _id: myUser._id}, { $pull: { 'profilesMatched': { _id: unmatchedUser._id }}}, { safe: true }).exec();
        
        // TODO: Send a Firebase notification
        return Promise.all([updateMyUser, updateOtherUser]);
      })
      .then((results) => {
        logger.debug('results of unmatching');
        logger.debug(results);
        fcmManager.sendUnmatchedNotification(unmatchedUser, myUser);
        const updated = _.filter(results, (result) => result.nModified > 0);
        logger.debug('updated');
        logger.debug(updated);
        if (updated && _.get(updated, 'length') > 0) {
          res.success({ result: 'Unmatched!' });
        } else {
          res.error('Could not unmatch anybody.');
        }
      })
      .catch((error) => {
        res.error('Could not unmatch');
      });
  }

};

module.exports = new UserMatchingController();