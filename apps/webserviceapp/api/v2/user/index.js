'use strict'
const authController = require('./controller.auth')
const matchingController = require('./controller.matching')
const profileController = require('./controller.profile')
const notificationsController = require('./controller.notifications')
const passport = require('passport')
const express = require('express')

module.exports = function(app) {
  const router = express.Router()
  // Possibly, rename this GET request to a token request. By using HASHIds.

  router.put('/notifications', passport.authenticate('facebook-token'), notificationsController.updateNotificationsToken)
  router.post('/notifications/test', passport.authenticate('facebook-token'), notificationsController.testSendNotification)
  router.post('/notifications/testMatched', passport.authenticate('facebook-token'), notificationsController.testSendMatchedNotification)

  /**
   * @api {DELETE} /api/v2/users/profilesToReview/:userIdToPass Pass a single
   * @apiName Unmatch with a single
   * @apiGroup Users
   *
   * @apiParam {Number} userIdToPass Users unique ID.
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 201 OK
   * 
   * {"data":{"user":{"_id":"5898334d59774b8ed9e30de6","updatedAt":"2017-02-11T01:56:55.486Z","createdAt":"2017-02-06T08:26:56.080Z","gender":0,"profilesToReview":[[],[],[],[],[],[],[{"updatedAt":"2017-02-11T01:47:57.618Z","createdAt":"2017-02-11T01:47:57.618Z","_id":"589833478ddc099ed95a94eb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},]],"profilesLiked":[{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9501","createdAt":"2017-02-11T01:47:57.620Z","updatedAt":"2017-02-11T01:47:57.620Z"},{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9513","createdAt":"2017-02-11T01:47:57.621Z","updatedAt":"2017-02-11T01:47:57.621Z"}],"profilesMatched":[],"qbchat":{"id":"23685265","login":"qb_5898334d59774b8ed9e30de6","password":"qb_10208930797610536_52"},"preferences":{"distance":3,"gender":{"male":false,"female":true},"age":{"min":18,"max":24}},"profile":{"firstName":"Anh","lastName":"Dao","age":0,"occupation":"","study":"","aboutMe":"","interests":[],"videos":[]},"complaints":[]},"liked":true,"matched":false}}
   */
  router.delete('/profilesMatched/:with', passport.authenticate('facebook-token'), matchingController.unmatchProfile);

  /**
   * @apiDefine Users Users APIs
   *
   * Authentication. Login and registration.
   * 
   */

  /**
   * @api {POST} /api/v2/users/ Register a new user
   * @apiName Registers a new user.
   * @apiGroup Users
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 201 OK
   * 
   * {"data":{"_id":"5898334d59774b8ed9e30de6","updatedAt":"2017-02-06T08:26:56.080Z","createdAt":"2017-02-06T08:26:56.080Z","gender":0,"profilesToReview":[[],[],[],[],[],[],[{"_id":"589833478ddc099ed95a94e3","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e5","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e7","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e9","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94eb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ed","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ef","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f1","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f3","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f5","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f7","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f9","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94fb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94fd","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ff","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9501","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9503","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9505","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9507","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9509","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950b","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950d","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950f","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9511","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9513","hasVideos":false,"matchScore":40,"priority":6,"viewed":false}]],"profilesLiked":[],"profilesMatched":[],"qbchat":{"id":"23685265","login":"qb_5898334d59774b8ed9e30de6","password":"qb_10208930797610536_52"},"preferences":{"distance":3,"gender":{"male":false,"female":true},"age":{"min":18,"max":24}},"profile":{"firstName":"Anh","lastName":"Dao","age":0,"occupation":"","study":"","aboutMe":"","interests":[],"videos":[]},"complaints":[]}}
   */
  router.post('/', passport.authenticate('facebook-token'), authController.registerUser);

  /**
   * @api {GET} /api/v2/users/ Logging in
   * @apiName Login. Gets a user's object with all public personal information.
   * @apiGroup Users
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * 
   * {"data":{"_id":"5898334d59774b8ed9e30de6","updatedAt":"2017-02-06T08:26:56.080Z","createdAt":"2017-02-06T08:26:56.080Z","gender":0,"profilesToReview":[[],[],[],[],[],[],[{"_id":"589833478ddc099ed95a94e3","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e5","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e7","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94e9","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94eb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ed","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ef","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f1","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f3","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f5","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f7","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94f9","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94fb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94fd","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a94ff","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9501","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9503","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9505","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9507","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9509","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950b","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950d","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a950f","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9511","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},{"_id":"589833478ddc099ed95a9513","hasVideos":false,"matchScore":40,"priority":6,"viewed":false}]],"profilesLiked":[],"profilesMatched":[],"qbchat":{"id":"23685265","login":"qb_5898334d59774b8ed9e30de6","password":"qb_10208930797610536_52"},"preferences":{"distance":3,"gender":{"male":false,"female":true},"age":{"min":18,"max":24}},"profile":{"firstName":"Anh","lastName":"Dao","age":0,"occupation":"","study":"","aboutMe":"","interests":[],"videos":[]},"complaints":[]}}
   */
  router.get('/', passport.authenticate('facebook-token'), authController.login);

  /**
   * @api {DELETE} /api/v2/users/profilesToReview/:userIdToPass Pass a single
   * @apiName Pass a single
   * @apiGroup Users
   *
   * @apiParam {Number} userIdToPass Users unique ID.
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 201 OK
   * 
   * {"data":{"user":{"_id":"5898334d59774b8ed9e30de6","updatedAt":"2017-02-11T01:56:55.486Z","createdAt":"2017-02-06T08:26:56.080Z","gender":0,"profilesToReview":[[],[],[],[],[],[],[{"updatedAt":"2017-02-11T01:47:57.618Z","createdAt":"2017-02-11T01:47:57.618Z","_id":"589833478ddc099ed95a94eb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},]],"profilesLiked":[{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9501","createdAt":"2017-02-11T01:47:57.620Z","updatedAt":"2017-02-11T01:47:57.620Z"},{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9513","createdAt":"2017-02-11T01:47:57.621Z","updatedAt":"2017-02-11T01:47:57.621Z"}],"profilesMatched":[],"qbchat":{"id":"23685265","login":"qb_5898334d59774b8ed9e30de6","password":"qb_10208930797610536_52"},"preferences":{"distance":3,"gender":{"male":false,"female":true},"age":{"min":18,"max":24}},"profile":{"firstName":"Anh","lastName":"Dao","age":0,"occupation":"","study":"","aboutMe":"","interests":[],"videos":[]},"complaints":[]},"liked":true,"matched":false}}
   */
  router.delete('/profilesToReview/:userIdToPass', passport.authenticate('facebook-token'), matchingController.passProfile);

  /**
   * @api {POST} /api/v2/users/profilesLiked Like a single
   * @apiName Likes a user
   * @apiGroup Users
   * 
   * @apiParamExample {json} Request-Example:
   *             { "idToLike": "589833478ddc099ed95a9501" }
   *
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 201 OK
   * 
   * {"data":{"user":{"_id":"5898334d59774b8ed9e30de6","updatedAt":"2017-02-11T01:56:55.486Z","createdAt":"2017-02-06T08:26:56.080Z","gender":0,"profilesToReview":[[],[],[],[],[],[],[{"updatedAt":"2017-02-11T01:47:57.618Z","createdAt":"2017-02-11T01:47:57.618Z","_id":"589833478ddc099ed95a94eb","hasVideos":false,"matchScore":40,"priority":6,"viewed":false},]],"profilesLiked":[{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9501","createdAt":"2017-02-11T01:47:57.620Z","updatedAt":"2017-02-11T01:47:57.620Z"},{"viewed":false,"priority":6,"matchScore":40,"hasVideos":false,"_id":"589833478ddc099ed95a9513","createdAt":"2017-02-11T01:47:57.621Z","updatedAt":"2017-02-11T01:47:57.621Z"}],"profilesMatched":[],"qbchat":{"id":"23685265","login":"qb_5898334d59774b8ed9e30de6","password":"qb_10208930797610536_52"},"preferences":{"distance":3,"gender":{"male":false,"female":true},"age":{"min":18,"max":24}},"profile":{"firstName":"Anh","lastName":"Dao","age":0,"occupation":"","study":"","aboutMe":"","interests":[],"videos":[]},"complaints":[]},"liked":true,"matched":false}}
   */
  router.post('/profilesLiked', passport.authenticate('facebook-token'), matchingController.likeProfile);

  /**
   * @api {GET} /api/v2/users/profilesToReview Get next profiles to review
   * @apiName Gets the next profiles for the logged in user to review.
   * @apiGroup Users
   *
   */
  router.get('/profilesToReview', passport.authenticate('facebook-token'), matchingController.getNextProfiles);

  /**
   * @api {POST} /api/v2/users/profiles Get details of other singles
   * @apiName Getting other users' details
   * @apiGroup Users
   * 
   * @apiParamExample {json} Request-Example:
   * 
   * { 
   *  "ids": 
   *  [
   *   { "_id": "589833478ddc099ed95a94e3" },
   *   { "_id": "589833478ddc099ed95a94e5" }
   *  ]
   * }
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * 
   * {"data":[{"_id":"589833478ddc099ed95a94e3","gender":1,"profilesLiked":[],"qbchat":{"login":"qb_1","password":"qb_password","id":"16306353"},"preferences":{"distance":3,"gender":{"male":false,"female":true}},"profile":{"firstName":"Girl#${i}","lastName":"Girly","aboutMe":"","occupation":"","study":"","age":27,"interests":[],"videos":[{"_id":0,"storagePath":"profiles/female/","uploaded":true,"videoName":"2.mp4","thumbnailName":"placeholder.jpg","review":{"approved":true,"reason":"Greatvideo!"}}]},"profilesToReview":[[],[],[],[],[],[]]},{"_id":"589833478ddc099ed95a94e5","gender":1,"profilesLiked":[],"qbchat":{"login":"qb_1","password":"qb_password","id":"16306353"},"preferences":{"distance":3,"gender":{"male":false,"female":true}},"profile":{"firstName":"Girl#${i}","lastName":"Girly","aboutMe":"","occupation":"","study":"","age":27,"interests":[],"videos":[{"_id":0,"storagePath":"profiles/female/","uploaded":true,"videoName":"4.mp4","thumbnailName":"placeholder.jpg","review":{"approved":true,"reason":"Greatvideo!"}}]},"profilesToReview":[[],[],[],[],[],[]]}]}
   */
  router.post('/profiles', passport.authenticate('facebook-token'), profileController.getUsersDetails);

  /**
   * @api {DELETE} /api/v2/users/ Deactivate/delete an account
   * @apiName Delete / deactive a user's account
   * @apiGroup Users
   *
   */
  router.delete('/', profileController.deleteUser);
  
  /**
   * @api {PATCH} /api/v2/users/profiles Update the user's profile details
   * @apiName Update a user's personal profile details.
   * @apiGroup Users
   * 
   * @apiParamExample {json} Request-Example:
   * 
   * {
   *   "age": 27,
   *   "interests": ["One", "Two", "Three"],
   *   "occupation": "Keysmith",
   *   "study": "RMIT University",
   *   "aboutMe": "A jolly person!",
   *   "location": {
   *     "lat": -38.35353535,
   *     "lng": 153.8559595
   *   }
   * }
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * 
   * {
   *   "data": {
   *     "aboutMe": "A jolly person!",
   *    "study": "RMIT University",
   *     "occupation": "Keysmith",
   *     "age": 27,
   *     "location": {
   *       "lat": -38.35353535,
   *       "lng": 153.8559595
   *     },
   *     "interests": [
   *       "One",
   *       "Two",
   *       "Three"
   *     ],
   *     "videos": []
   *   }
   * }
   * 
   */
  router.patch('/', passport.authenticate('facebook-token'), profileController.updateUserProfile)

  /**
   * @api {POST} /api/v2/users/:userId/complaints Report another user
   * @apiName Report another user
   * @apiGroup Users
   * 
   * @apiParam {Number} userId User's id
   */
  router.post('/:userId/complaints', passport.authenticate('facebook-token'), profileController.addComplaint);
  
  /**
   * @api {DELETE} /api/v2/users/profiles/videos/:videoId Remove a video
   * @apiName Remove a user's profile video
   * @apiGroup Users
   * 
   * @apiParam {Number} userIdToPass Video's id
   */
  router.delete('/profiles/videos/:videoId', passport.authenticate('facebook-token'), profileController.deleteProfileVideo);

  /**
   * @api {PUT} /api/v2/users/profiles/videos/ Update / add a user profile video
   * @apiName Update / add a user profile video
   * @apiGroup Users
   * 
   * @apiParamExample {json} Request-Example:
   * {
   *   "request": [
   *     {
   *       "_id": 3,
   *       "storagePath": "somewhere",
   *       "videoName": "video.mp4",
   *       "thumbnailName": "video2.png",
   *       "order": 2
   *     }
   *   ]
   * }
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * {
   *   "data": [
   *     {
   *       "order": 2,
   *       "thumbnailName": "video2.png",
   *       "videoName": "video.mp4",
   *       "storagePath": "somewhere",
   *       "_id": 0,
   *       "videoUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video.mp4",
   *       "thumbnailUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video2.png"
   *     },
   *     {
   *       "order": 1,
   *       "thumbnailName": "video.png",
   *       "videoName": "video.mp4",
   *       "storagePath": "somewhere",
   *       "_id": 1,
   *       "videoUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video.mp4",
   *       "thumbnailUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video.png"
   *     },
   *     {
   *       "order": 2,
   *       "thumbnailName": "video2.png",
   *       "videoName": "video.mp4",
   *       "storagePath": "somewhere",
   *       "_id": 3,
   *       "videoUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video.mp4",
   *       "thumbnailUrl": "https://snapflirt-1203.commondatastorage.googleapis.com/somewhere/video2.png"
   *     }
   *   ]
   * }
   */
  router.put('/profiles/videos', passport.authenticate('facebook-token'), profileController.updateProfileVideos);


  /**
   * @api {PATCH} /api/v2/users/profiles/videos/ Update a user's dating preferences
   * @apiName Update a user's dating preferences
   * @apiGroup Users
   * 
   * @apiParamExample {json} Request-Example:
   * 
   * {
   *   "gender": {
   *    "male": false,
   *    "female": true
   *   },
   *   "age": {
   *     "min": 28,
   *     "max": 38
   *   },
   *   "distance": 10 
   * }
   * 
   * @apiSuccessExample {json} Success-Response:
   * 
   * HTTP/1.1 200 OK
   * {
   *   "data": {
   *     "age": {
   *       "min": 28,
   *       "max": 38
   *     },
   *     "gender": {
   *       "male": false,
   *       "female": true
   *     },
   *     "distance": 10
   *   }
   * }
   * 
   * 
   */
  router.patch('/preferences', passport.authenticate('facebook-token'), profileController.updateUserPreferences);
  
  /**
   * @api {PATCH} /api/v2/users/profiles/videos/ Fix Quickblox credentials
   * @apiName Attempts to fix quickblox login / password if they are invalid
   * @apiGroup Users
   */
  router.patch('/profile/qbchat', passport.authenticate('facebook-token'), authController.fixQuickblox);
  
  app.use('/api/v2/users', router)
};