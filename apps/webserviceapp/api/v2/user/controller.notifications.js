'use strict'
const _ = require('lodash')
const mongoose = require('mongoose')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const User = rootRequire('apps/webserviceapp/models/User')
const VideoSchema = rootRequire('apps/webserviceapp/models/VideoSchema')
const VideoModel = mongoose.model('Video', VideoSchema)
const NotificationsUseCase = rootRequire('apps/webserviceapp/usecases/notifications').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class NotificationsController {

  testSendNotification(req, res, next) {
    NotificationsUseCase.sendMessage(req.body.to, 'Test', 'Message')
    res.success('')
  }

  testSendMatchedNotification(req, res, next) {
    const toUserId = req.body.to
    const fromUserId = req.body.from
    return Promise.all(
      [
        User.findOne({ _id: fromUserId, }).exec(),
        User.findOne({ _id: toUserId, }).exec(),
      ] 
    )
    .then((users) => {
      NotificationsUseCase.sendMatchedNotification(users[1], users[0])
      res.success('')
    })
    .catch((e) => {
      res.error(e)
    })
  }

  updateNotificationsToken(req, res, next) {
    let errors
    req.checkBody('token', 'A notifications token has to be provided').notEmpty()
    req.getValidationResult()
       .then((errs) => {
        if (!errs.isEmpty()) {
          errors = errs.mapped()
          throw new Error('Validation failed')
        }
       })
       .then(() => User.update(
          { facebookId: req.user.id },
          {
            $set: {
              'fcm': { token: req.body.token, }
            }
          },
          { upsert: false, }
        ).exec()
       )
       .then((result) => {
        if (result.nModified > 0) {
          res.success('Updated')
        } else {
          res.error('User could not be updated')
        }
       })
       .catch((err) => {
        res.error(errors)
       })
  }
}

module.exports = new NotificationsController()