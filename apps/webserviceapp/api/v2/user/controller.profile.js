'use strict'
const _ = require('lodash')
const mongoose = require('mongoose')
const logger = rootRequire('apps/webserviceapp/helpers/logger')
const User = rootRequire('apps/webserviceapp/models/User')
const VideoSchema = rootRequire('apps/webserviceapp/models/VideoSchema')
const VideoModel = mongoose.model('Video', VideoSchema)
const UseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserProfileController {

  updateUserPreferences(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined)
    const updateRequest = _.get(req, 'body', {})
    if (Object.keys(updateRequest).length === 0) {
      return res.error('Update request is invalid')
    }

    const genderRequest = _.get(updateRequest, 'gender', {})
    const distanceRequest = _.get(updateRequest, 'distance', undefined)
    const age = _.get(updateRequest, 'age', {})
    const modifiableFields = [
      'gender',
      'distance',
      'age',
    ]
    let dbRequest = {}
    if (_.isNumber(age.min)) _.set(dbRequest, 'age.min', age.min)
    if (_.isNumber(age.max)) _.set(dbRequest, 'age.max', age.max)
    if (_.isNumber(distanceRequest)) _.set(dbRequest, 'distance', distanceRequest)
    if (_.isBoolean(genderRequest.male)) _.set(dbRequest, 'gender.male', genderRequest.male)
    if (_.isBoolean(genderRequest.female)) _.set(dbRequest, 'gender.female', genderRequest.female)

    if (Object.keys(dbRequest).length === 0) return res.error('Wrong update query')

    User.findOneAndUpdate(
      { facebookId: userFbId },
      { preferences: dbRequest },
      { upsert: false, new: true }
    ).exec()
      .then((user) => {
        res.success(user.preferences)
      })
      .catch((error) => {
        logger.error('Error', error)
        res.error('Could not update the user')
      })
  }

  updateUserProfile(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined)
    const profileRequest = _.get(req, ['body'], {});
    if (Object.keys(profileRequest).length === 0) {
      return res.error('Update request is invalid')
    }

    const modifiableFields = [
      'interests',
      'occupation',
      'study',
      'aboutMe',
      'location',
      'age',
      'gender',
    ]

    let updateQuery = {}
    for (const key in profileRequest) {
      if (key === 'location') {
        if (!_.isNil(profileRequest[key].lng)) {
          updateQuery['profile.location.lng'] = profileRequest[key].lng
        }
        if (!_.isNil(profileRequest[key].lat)) {
          updateQuery['profile.location.lat'] = profileRequest[key].lat
        }
      } else {
        if (modifiableFields.indexOf(key) !== -1) {
          if (key !== 'gender') {
            updateQuery[`profile.${key}`] = profileRequest[key]
          } else {
            updateQuery[key] = profileRequest[key]
          }
        } 
      }
    }
    if (Object.keys(updateQuery).length === 0) {
      return res.error('Update request is invalid')
    }
    
    User.findOneAndUpdate(
      { facebookId: userFbId},
      { $set: updateQuery },
      { upsert: false, new: true }
    ).exec()
      .then((user) => UseCase.hidePrivateFields(user))
      .then((user) => res.success({
        profile: user.profile,
        gender: user.gender,
      }))
      .catch((error) => {
        logger.error('Error', error)
        res.error('Could not update the user')
      })
  }

  getUsersDetails(req, res, next) {
    const _idsRaw = _.get(req, 'body.ids', undefined)
    if (!_idsRaw) return res.error('No user profiles.')
    const _ids = _.map(_idsRaw, (obj) => obj._id)

    logger.info('Getting users:')
    logger.info(_ids)
    User.find().where('_id').in(_ids)
      .select({ 
        _id: 1, 
        facebookId: 1,
        gender: 1,
        profile: 1,
        preferences: 1,
        profilesLiked: 1,
        qbchat: 1,
      })
      .exec()
      .then((profiles) => {
        for (let i = 0; i < profiles.length; i++) {
          profiles[i] = profiles[i].toObject({versionKey: false})
          UseCase.hidePrivateFields(profiles[i])
        }
        res.success(profiles)
      })
      .then(null, (err) => {
        res.error(err.message)
      });
  }

  deleteProfileVideo(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined);
    const videoId = _.get(req, 'params.videoId', undefined)
    if (_.isNil(userFbId) || _.isNil(videoId)) {
      return res.error('Could not delete the video');
    }
    User.findOneAndUpdate(
      { facebookId: userFbId },
      { $unset: { [`profile.videos.${videoId}`] : '' }},
      { new: true}
    ).exec()
      .then((u) => {
        u = UseCase.hidePrivateFields(u)
        return res.success(u)
      })
      .catch((e) => {
        logger.error('deleteProfileVideo()')
        logger.error(e)
        return res.error('Could not delete the video')
      })
  }

  // Used for swapping
  updateProfileVideos(req, res, next) {
    const userFbId = _.get(req, 'user.id', undefined)
    const updateQuery = _.get(req, 'body.request', {})

    if (!userFbId) return res.error('No user', 401)
    if (Object.keys(updateQuery).length === 0) return res.error('Wrong update query')

    let dbUpdateQuery = {}
    for (const key in updateQuery) {
      if (_.isNil(updateQuery[key])) continue;
      const videoSchema = new VideoModel(updateQuery[key])
      const validationProblems = videoSchema.validateSync()
      if (validationProblems) {
        let errorMessage = {}
        _.forOwn(validationProblems.errors, (value, key) => {
          errorMessage[key] = value.message
        })
        return res.error(errorMessage)
      } else {
        _.set(dbUpdateQuery, [`profile.videos.${videoSchema._id}`], videoSchema)
      }
    }
    if (Object.keys(dbUpdateQuery).length === 0) {
      return res.error('Invalid update query')
    }
    User.findOneAndUpdate(
      { facebookId: userFbId },
      { $set: dbUpdateQuery },
      { new: true, upsert: false }
    ).exec()
      .then((u) => UseCase.hidePrivateFields(u))
      .then((u) => {
        const videos = _.get(u, 'profile.videos', [])
        return res.success(videos)
      })
      .catch((e) => {
        logger.error('updateProfileVideos() Error', e)
        res.error('Could not update the videos')
      })
  }
  
  addProfileVideo(req, res, next) {
    let params = req.body;
    if (!req.user || !req.user.id) { return res.error('Unauthenticated', 401); }
    if (!params.hasOwnProperty('fileName') || !params.hasOwnProperty('uploaded') || 
        !params.hasOwnProperty('storagePath')) {
      return res.error('Invalid parameters', 400);
    }
    let pushParams =  { 
      fileName : params.fileName,
      uploaded : params.uploaded,
      storagePath : params.storagePath
    };
    User.findOne({ facebookId : req.user.id}).exec()
        .then(onUserFound)
        .then(null, (err) => {
          logger.error(err);
          res.error(err.message);
        });

    function onUserFound(user) {
      logger.info('addProfileVideo():');
      if (user && user.profile && user.profile.videos) {
        let alreadyExists = false;
        user.profile.videos.forEach((v, i) => {
          if (params.fileName === v.fileName) {
            alreadyExists = true;
          }
        });
        if (!alreadyExists) {
          user.profile.videos.push(pushParams);
          user.save()
            .then((u) => {
              res.success({ changed : true }, 200);
            })
            .then(null, (e) => {
              res.error(e.message);
            });
        } else {
          res.success( { changed : false }, 200);
        }
      } else {
        res.error('No such user', 400);
      }
    }
  }

  addComplaint(req, res, next) {
    const userIdToComplainAbout = _.get(req, ['params', 'userId'], undefined)
    const reason = _.get(req, ['body', 'reason'], undefined)
    if (!reason || !userIdToComplainAbout) return res.error('Must provide a user and a reason.');
    User.findOne({ _id: userIdToComplainAbout }).exec()
      .then((myUser) => {
        if (!myUser) {
          throw new Error('Could not flag the user.');
        }
        const firstName = _.get(myUser, 'profile.firstName', '')
        const lastName = _.get(myUser, 'profile.lastName', '')
        const complaint = {
          reporter: {
            name: `${firstName} ${lastName}`,
            facebookId: myUser.facebookId,
            _id: myUser._id
          },
          reason: reason
        };
        return User.findByIdAndUpdate(myUser._id,
          { $push: { complaints: complaint }},
          { safe: true, new: true, upsert: false }).exec();
      })
      .then((result) => {
        if (result) {
          res.success({ result: 'Flagged'}); 
        } else {
          res.error('Could not file a complaint');
        }
      })
      .catch((e) => {
        logger.error(e)
        return res.error('Could not flag the user');        
      });
  }

  deleteUser(req, res, next) {
    const fbId = _.get(req, ['user', 'id'], undefined)
    if (_.isNil(fbId)) {
      return res.error('No user specified');
    }
    // Only admins will be able to actually remove these users, though.
    User.update({ facebookId : fbId }, { isDeleted: true }, { upsert: false }).exec()
      .then(() => {
        res.success({ result: 'Deleted'});
      })
      .then(null, (e) => {
        logger.error(e);
        res.error(e);
      })
  }
}

module.exports = new UserProfileController()