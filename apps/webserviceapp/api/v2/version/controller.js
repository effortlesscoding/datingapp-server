'use strict';
const _ = require('lodash');

const Versions = rootRequire('apps/webserviceapp/models/Versions');
const config = rootRequire('config');

function rootRequire(path) {
  const ROOT_PATH = '../../../../../';  
  return require(`${ROOT_PATH}${path}`);
}

// const ANDROID_MIN_VERSION = process.env.ANDROID_MIN_VERSION;
// const ANDROID_CUR_VERSION = process.env.ANDROID_CUR_VERSION;
// const IOS_MIN_VERSION = process.env.IOS_MIN_VERSION;
// const IOS_CUR_VERSION = process.env.IOS_CUR_VERSION;

class ValidationController {

  setAndroidVersion(req, res, next) {
    const username = _.get(req, ['body', 'username']);
    const password = _.get(req, ['body', 'password']);
    const minVersion = _.get(req, ['body', 'minVersion']);
    const curVersion = _.get(req, ['body', 'curVersion']);
    const updateMessage = _.get(req, ['body', 'updateMessage']);
    const currentPackage = _.get(req, ['body', 'currentPackage']);
    if (_.isEmpty(username) || _.isEmpty(password)) 
      return res.error('No username or password supplied.');
    
    if (!this.validAuthentication(username, password)) 
      return res.error('Wrong username or password');

    if (_.isEmpty(minVersion) && _.isEmpty(curVersion) && _.isEmpty(updateMessage) && 
        _.isEmpty(currentPackage)) {
      return res.error('Not enough arguments');
    } 

    Versions.setAndroidVersion(minVersion, curVersion, updateMessage, currentPackage)
      .then(() => {
        res.success('Done');
      })
      .catch((e) => {
        res.error(new Error(e));
      });
  }

  setIOSVersion(req, res, next) {
    const username = _.get(req, ['body', 'username']);
    const password = _.get(req, ['body', 'password']);
    const minVersion = _.get(req, ['body', 'minVersion']);
    const curVersion = _.get(req, ['body', 'curVersion']);
    const updateMessage = _.get(req, ['body', 'updateMessage']);

    if (_.isEmpty(username) || _.isEmpty(password)) 
      return res.error('No username or password supplied.');

    if (!this.validAuthentication(username, password)) 
      return res.error('Wrong username or password');

    if (_.isEmpty(minVersion) && _.isEmpty(curVersion) && _.isEmpty(updateMessage)) 
      return res.error('Not enough arguments');

    Versions.setIOSVersion(minVersion, curVersion, updateMessage)
      .then(() => {
        res.success('Done');
      })
      .catch((e) => {
        res.error(new Error(e));
      });
  }

  getAndroidVersion(req, res, next) {
    Versions.getVersions()
      .then((v) => {
        const minVersion = _.get(v, ['android', 'minVersion']);
        const curVersion = _.get(v, ['android', 'curVersion']);
        const updateMessage = _.get(v, ['android', 'updateMessage']);
        const currentPackage = _.get(v, ['android', 'currentPackage']);

        if (_.isEmpty(minVersion) || _.isEmpty(curVersion) || 
            _.isEmpty(updateMessage) || _.isEmpty(currentPackage))
          return res.error('Cannot get the versions.');

        res.success({
          minVersion: minVersion,
          curVersion: curVersion,
          updateMessage: updateMessage,
          currentPackage: currentPackage
        });
      })
      .catch((e) => {
        res.error('Cannot get the versions.');
      });
  };

  getIOSVersion(req, res, next) {
    Versions.getVersions()
      .then((v) => {
        const minVersion = _.get(v, ['ios', 'minVersion']);
        const curVersion = _.get(v, ['ios', 'curVersion']);
        const updateMessage = _.get(v, ['ios', 'updateMessage']);

        if (_.isEmpty(minVersion) || _.isEmpty(curVersion) || 
            _.isEmpty(updateMessage))
          return res.error('Cannot get the versions.');

        res.success({
          minVersion: minVersion,
          curVersion: curVersion,
          updateMessage: updateMessage
        });
      })
      .catch((e) => {
        res.error('Cannot get the versions.');
      });
  };

  getApiVersion(req, res, next) {
    res.success({
      version: config.version
    });
  }

  validAuthentication(username, password) {
    return (username === 'admin' && password === 'tester123');
  }
}


module.exports = new ValidationController();