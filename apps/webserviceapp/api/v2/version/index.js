'use strict';
const ROOT_PATH = '../../../../../';

const passport = require('passport');
const controller = require('./controller');
const config = require(`${ROOT_PATH}config`);

// TODO: implement EXTRA security here. LOL.
// Add passport validations for GET
// Add a very special password for PUT requests
module.exports = function(app) {

  /**
   * @apiDefine Versions Versions API endpoints
   *
   * "Versions" APIs Validation of iOS/Android app versions
   * 
   */

  /**
   * @api {GET} /api/v2/versions/android Get the current Android version
   * @apiName Get the current Android version
   * @apiGroup Versions
   */
  app.get('/api/v2/versions/android', controller.getAndroidVersion);

  /**
   * @api {PUT} /api/v2/versions/android Update the current Android version
   * @apiName Update the current Android version
   * @apiGroup Versions
   */
  app.put('/api/v2/versions/android', controller.setAndroidVersion);

  /**
   * @api {GET} /api/v2/versions/ios Get the current iOS version
   * @apiName Get the current iOS version
   * @apiGroup Versions
   */
  app.get('/api/v2/versions/ios', controller.getIOSVersion);

  /**
   * @api {PUT} /api/v2/versions/ios Update the current iOS version
   * @apiName Update the current iOS version
   * @apiGroup Versions
   */
  app.put('/api/v2/versions/ios', controller.setIOSVersion);


  /**
   * @api {GET} /api/v2/versions/ Get the current web API version
   * @apiName Gets the current web API version
   * @apiGroup Versions
   */
  app.get('/api/v2/versions/', controller.getApiVersion); 
};


