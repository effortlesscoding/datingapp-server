'use strict';
var amqp = require('amqplib');
var logger = require('../helpers/logger');
var EventEmitter = require('events').EventEmitter;
var config = require('../../config'); 
var PublisherNextProfiles = require('./publishers/publisherNextProfiles.js');

function Publisher() {
  EventEmitter.call(this);
  this.publisherNextProfiles = null;
}

Publisher.prototype = Object.create(EventEmitter.prototype);

Publisher.prototype.start =  function() {
  var _this = this;
  var url = config.amqp.url + ':' + config.amqp.port + '?heartbeat=60';
  logger.debug('[AMQP Publisher] start() connection. Url : ' + url);
  
  return amqp.connect(url)
    .then(onAmqpConnected)
    .then(startPublisher)
    .catch(onAmqpConnectionError);

  function startPublisher() {
    if (_this.publisherNextProfiles) {
      return _this.publisherNextProfiles.openConfirmChannel();
    } else {
      throw new Error('No publisher available.');
    }
  }

  function onAmqpConnectionError(error) {
    logger.error('[AMQP PUBLISHER ERROR] ' + error.message);
    _this.emit('connectionError', error);
  }

  function onAmqpConnected(conn) {
    logger.info('[AMQP Publisher] Connection established.');
    conn.on('error', onAmqpConnectionError);
    conn.on('close', onConnectionClose);
    _this.publisherNextProfiles = new PublisherNextProfiles(conn);
  }

  function onConnectionClose() {
    logger.error('[AMQP] closed. Attempting to reconnect');
    _this.emit('connectionClosed');
  }
}

Publisher.prototype.replenishNextProfiles = function(m) {
  if (this.publisherNextProfiles) {
    return this.publisherNextProfiles.publish(m)
      .catch(function(err) {
        logger.error('[AMQP Publisher] Could not replenish profiles.');
        logger.error(err);
        throw new Error('[AMQP Publisher] Could not replenish profiles.');
      });
  } else {
    throw new Error('[AMQP Publisher] Publisher is not ready.');
  }
}

module.exports = new Publisher();