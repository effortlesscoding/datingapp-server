'use strict';

/** 
  This needs extra amount of testing.
**/

var EventEmitter = require('events').EventEmitter;
var config = require('../../../config');
var mongoose = require('mongoose');
var logger = require('../../helpers/logger');
const QUEUE_NAME = config.amqp.queues.nextProfiles; 

function PublisherNextProfiles(amqpConnection) {
  EventEmitter.call(this);
  this.amqpConnection = amqpConnection;
  this.offlinePubQueue = [];
  this.publishChannel = null;
}

PublisherNextProfiles.prototype = Object.create(EventEmitter.prototype);

/**
 *  Creates a confirm channel where you can publish
 **/
PublisherNextProfiles.prototype.openConfirmChannel = openConfirmChannel;

PublisherNextProfiles.prototype.publish = publish;


function openConfirmChannel() {
  var _this = this;
  if (_this.amqpConnection) {
    return _this.amqpConnection.createConfirmChannel()
        .then(onChannelCreated)
        .catch(onChannelError);
  } else {
    throw new Error('[AMQP PUBLISHER ERROR] Could not find amqp connection.');
  }

  //////

  function onChannelError(err) {
    logger.error(err);
    return Promise.reject(err);
  }

  function onChannelCreated(ch) {
    _this.publishChannel = ch;
    _this.publishChannel.on("error", onPublishChannelError);
    _this.publishChannel.on("close", onPublishChannelClosed);
    publishOffline();
    logger.debug('[AMQP PUBLISHER] Channel created');
  }


  function publishOffline() {
    if (_this.offlinePubQueue.length <= 0) { 
      logger.info('[AMQP PUBLISHER] Offline queue is empty.'); 
      return;
    } 
    var m = _this.offlinePubQueue.shift();
    
    if (!m || m.length < 3) { 
      throw new Error('[AMQP PUBLISHER] Cannot publish offline queue because message is not correct : ' + JSON.stringify(m)); 
    };

    return publish(m[0], m[1], m[2])
      .then(function(ok) {
        if (_this.offlinePubQueue.length > 0) {
          publishOffline();
        }
      })
      .catch(function(err) {
        logger.error(err);
        _this.offlinePubQueue.push(m);
      });
  }


  function onPublishChannelError(err) {
    logger.error("[AMQP] channel error", err.message);
    logger.debug('[AMQP Publisher] channel error');
    _this.emit('channelError')
  }

  function onPublishChannelClosed() {
    logger.error('[AMQP] channel closed');
    logger.debug('[AMQP Publisher] channel closed');
    _this.emit('channelClosed')
  }
}

function publish(content) {
  var _this = this;
  var exchange = '';
  var c = null;
  var promise = new Promise(function(resolve, reject) {
    var publishChannel = _this.publishChannel;
    var offlinePubQueue = _this.offlinePubQueue;
    try {
      c = JSON.stringify(content);
    } catch(err) {
      logger.error(err);
      reject(err);
    }

    publishChannel.publish(exchange, QUEUE_NAME, new Buffer(c), { persistent: true }, function(err, ok) {
      if (err) {
        offlinePubQueue.push([exchange, QUEUE_NAME, content]);
        publishChannel.connection.close();
        reject(err);
      } else {
        resolve(ok);
      }
    });
  });

  return promise;
}

module.exports = PublisherNextProfiles;