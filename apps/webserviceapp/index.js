'use strict';
const _ = require('lodash')
const config = require('../../config')
const apiEndpoints = {
	storage: require('./api/v2/storage'),
	user: require('./api/v2/user'),
	chat: require('./api/v2/chat'),
	version: require('./api/v2/version')
}
const express = require('express')
const childProcess = require('child_process')

const app = express()
const QB = require('quickblox')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const session = require('express-session')
const logger = require('./helpers/logger')
const fcmManager = require('./helpers/fcm')
// const amqpPublisher = require('./amqpPublisher');
const passportConfig = require('./helpers/passport')
const EventEmitter = require('events').EventEmitter
const expressValidator = require('express-validator')

express.response.success = function(response, code) {
	this.status(code ? code : 200);
	this.json({ data : response } );
};

express.response.error = function(message, code) {
	this.status(code ? code : 400);
	this.json({error : { message : message } });	
};

class WebServiceApp {

	constructor(dbUrl, dbConfig) {
		process.on('SIGNINT', () => {
			mongoose.connection.close(function() {
				process.exit(0);
			});
		});
		this.dbUrl = dbUrl
		this.dbConfig = dbConfig
		this.server = null
	}

	initWebserviceApp() {
		return new Promise((resolve, reject) => {
			logger.info('Starting the server. Version:' + config.version);
			app.use(bodyParser.json());
			app.use(expressValidator([]))
			app.use(session({
			  secret: 'secretify',
		    resave: true,
		    saveUninitialized: true
		  }));

		  passportConfig(app);
		  app.use('/api/docs', express.static('doc'));
			apiEndpoints.chat(app);
			apiEndpoints.storage(app);
			apiEndpoints.user(app);
			apiEndpoints.version(app);
			
			app.use((err, req, res, next) => {
				const code = err.status || 500;	
				res.status(code);
				res.json({ code : code, message: err.message});
			});

			this.server = app.on('after', (req, res, route, err) => {
					if (err) { logger.error(err);}
					logger.info('[Main app] Started the server.');
				})
				.listen(config.server.port, () => {
					logger.info('Server is listening on port ', config.server.port);
				});
			resolve(app);
		});
	}

	initPublisher() {
		return new Promise((resolve, reject) => {
			return resolve();
		// return amqpPublisher.start()
		// 	.then(function() {
		// 		logger.info('[Main App] AMQP Publisher started on the server.');
		// 		return true;
		// 	})
		// 	.then(null, function(err) {
		// 		logger.error('[Main App] AMQP Publisher failed to start.');
		// 		logger.error(err);
		// 		throw new Error('[Main App] Could not start AMQP publisher.');
		// 	});
		});
	}


	initQuickblox() {
		return new Promise((resolve, reject) => {
			const qbConfig = config.quickblox;
			// TODO: Untested:
			const extraConfig = {
				on: {
			    sessionExpired: (next, retry) => {
			    	logger.error('quickblox session EXPIRED!');
						QB.createSession(function(err, result) {
						  if (err) {
						  	logger.error('Error when trying to recreate a session in QB:');
						  	logger.error(err);
						  	return retry();
						  }
						  logger.info('QB session re-created with result:');
						  logger.info(result);
						  return next();
						});
			    }
			  }
			}
			logger.info('Initialising a quickblox instance.');
			QB.init(qbConfig.appId, qbConfig.authKey, qbConfig.authSecret, extraConfig);
			//QB.init(43460, 'TxCUjgLY7KD8G6q', 'U8trwXLRRVCGrJT');
			QB.createSession(function(err, result) {
			  if (err) {
			  	logger.error('Error in QB:');
			  	return reject(err);
			  }
			  logger.info('QB inited with result:');
			  logger.info(result);
			  return resolve(result);
			});
		});
	}

	initDatabase() {
		let connectAttempts = 0;
		return new Promise((resolve, reject) => {
			logger.info('Starting a mongodb connection.')
			mongoose.connect(this.dbUrl, this.dbConfig)
			
			mongoose.connection.on('connected', () => {
				logger.info('Connected to mongodb!')
				connectAttempts = 0
				resolve(true)
			})

			mongoose.connection.on('error', (error) => {
				connectAttempts++
				logger.error('Mongoose connection has failed.')
				logger.error(error)
				if (connectAttempts < 3) {
					setTimeout(function() {
						logger.info('Attempting to reconnect to ', config.mongoDb.url)
						mongoose.connect(config.mongoDb.url + config.mongoDb.collection)
					}, 2500);
				} else {
					throw new Error('Could not reconnect to mongodb.')
				}
			})
		});

	}

	initFcm() {
		logger.info('Initialising FCM');
		logger.error('NO ERROR');
		return fcmManager.initialise();
	}

	spawnWorkerQueue() {
		if (process.env.NODE_ENV.indexOf('localtest') === -1) {
			var worker = childProcess.fork('./workerapp', [], {execArgv: ['--debug=5859']});
			worker.on('exit', function(code) {
			    logger.info('exit code: ' + code);
			});
		}
		return Promise.resolve();
	}

	start() {
		logger.info('Setting up the connections and environment for the server.');
		return this.initPublisher()
			.then(() => this.initQuickblox())
			.then(() => this.initDatabase())
			.then(() => this.initFcm())
			.then(() => this.initWebserviceApp())
			.catch((e) => {
				logger.error(e);
			});
	};

	stop() {
		return new Promise((resolve, reject) => {
			if (this.server) {
				mongoose.disconnect()
				this.server.close(() => {
					console.log('Server shut down')
					resolve()
				})
			}
		})
	}

	getExpressApp() {
		return app
	}
}

module.exports = WebServiceApp;