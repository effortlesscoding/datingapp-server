'use strict';
const facebookAppId = process.env.DATING_APP_FACEBOOK_ID;
const facebookAppSecret = process.env.DATING_APP_FACEBOOK_SECRET;
const gCloudStorageProjectId = process.env.DATING_APP_GCM_PROJECT_ID; // 'snapflirt-1203'
const gCloudStorageBucket = process.env.DATING_APP_GCM_BUCKET; // 'snapflirt-1203' // 'kissthatprince-production'
const qbAppId = process.env.DATING_APP_QUICKBLOX_APP_ID;
const qbSecret = process.env.DATING_APP_QUICKBLOX_SECRET;
const qbAuthKey = process.env.DATING_APP_QUICKBLOX_KEY;
const gCloudSecret = process.env.DATING_APP_GCLOUD_SECRET;
const gCloudClientId = process.env.DATING_APP_GCLOUD_CLIENT_ID;
const fcmSecretKey = process.env.DATING_APP_FCM_SECRET_KEY;
const dbUrl = process.env.DATING_APP_DATABASE_URL;
const dbUsername = process.env.DATING_APP_DATABASE_USERNAME;
const dbPassword = process.env.DATING_APP_DATABASE_PASSWORD;
const dbTestUsername = process.env.DATING_APP_DATABASE_TEST_USERNAME;
const dbTestPassword = process.env.DATING_APP_DATABASE_TEST_PASSWORD;

let prefix = '';

if (process.env.NODE_ENV === 'production') {
	prefix = '_prod_';
}

const config = {
	version: '0.1.0.0', // Version is just used to be bumped and to be queried.
	server: {
		port: process.env.PORT | 3000,
		url: 'http://127.0.0.1',
		reconnectTimeout: 2500
	},
	adminApp: {
		port: process.env.PORT + 1 | 3001,
		url: 'http://127.0.0.1',
		reconnectTimeout: 2500
	},
	quickblox: {
	  appId: qbAppId,
	  authKey: qbAuthKey,
	  authSecret: qbSecret,
	  loginPrefix: prefix
	},
	fcm: {
		serverKey: fcmSecretKey
	},
	google: {
		storage: { 
			projectId: gCloudStorageProjectId,
			bucket: gCloudStorageBucket,
		}
	},
	logging: {
		file: 'logs/webservice.log',
		consumerFile: 'logs/consumer.log',
		level: 'debug'
	},
	mongoDb: {
		url: dbUrl,
		username: dbUsername,
		usernameTest: dbTestUsername,
		password: dbPassword,
		passwordTest: dbTestPassword,
		collection: '/datingapp',
		collectionTest: '/datingapptest'
	},
	amqp: {
		url: 'amqp://127.0.0.1',
		port: 5672,
		queues: {
			nextProfiles: 'NEXT_PROFILES'
		} // unnecessary
	},
	usersSettings: {
		id: 1
	},
	facebook: {
		appId: facebookAppId,
		secret: facebookAppSecret
	},
	matchRules: {
		round: 100,
		upperBound: 60,
		lowProfilesCount: 10,
		MAX_PROFILE_VIDEOS: 6,
		MAX_PROFILES_TO_REVIEW: 50
	}
};


module.exports = config;