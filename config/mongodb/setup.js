var normalActionsPrivileges = [
  'createIndex',
  'remove',
  'find',
  'insert',
  'listCollections',
  'listIndexes',
  'update'
];

var adminActionsPrivileges = [
  'createCollection',
  'dropCollection',
  'dropIndex'
];

var createFirstUser = function() {
  db = db.getSiblingDB('admin');

  var admin = {
    user: 'admin',
    pwd: '!?@dm1n1st3rS3cur3Stuff!?',
    roles: [
      'userAdminAnyDatabase',
      'readWriteAnyDatabase',
      'root',
      'dbAdminAnyDatabase'
    ]
  };
  
  db.createUser(admin);
};

var createRoles = function() {
  db = db.getSiblingDB('datingapp');

  var webappRole = {
    role: 'datingapp.WebApp',
    privileges: [
      {
        resource: { db: db.getName(), collection: '' },
        actions: normalActionsPrivileges
      }
    ],
    roles: []
  };

  var admin = {
    role: 'datingapp.Admin',
    privileges: [
      {
        resource: {db: db.getName(), collection: ''},
        actions: adminActionsPrivileges
      }
    ],
    roles: ['datingapp.WebApp']
  };

  db.createRole(webappRole);
  db.createRole(admin);
};

var updateRolesPrivileges = function() {

  db = db.getSiblingDB('datingapp');
  const webappPrivileges = [
    {
      resource: { db: db.getName(), collection: '' },
      actions: normalActionsPrivileges
    }
  ];
  const adminPrivileges = [
    {
      resource: {db: db.getName(), collection: ''},
      actions: adminActionsPrivileges
    }
  ];
  db.updateRole('datingapp.Admin', {
    privileges: adminPrivileges,
    roles: ['datingapp.WebApp']
  });
  db.updateRole('datingapp.WebApp', {
    privileges: webappPrivileges,
    roles: []
  });
}

var createUsers = function() {
  db = db.getSiblingDB('datingapp');

  var webappUser = {
    user: 'webappuser',
    pwd: '!?W3|3@ppUs3r!?',
    roles: ['datingapp.WebApp'],
  };

  var adminUser = {
    user: 'sysadmin',
    pwd: '?$@dm#`n1Us3r$',
    roles: ['datingapp.Admin'],
  };

  db.createUser(webappUser);
  db.createUser(adminUser);

  db = db.getSiblingDB('admin');
  db.system.users.find({ 'roles.role': /^datingapp/ }).pretty();
};