const _ = require('lodash')
const mongoose = require('mongoose')
const UserModel = rootRequire('apps/webserviceapp/models/User')
const UsersSettings = rootRequire('apps/webserviceapp/models/UsersSettings')
const UserBuilder = rootRequire('specs/mocks/MockUser')
const config = rootRequire('config')
const data = rootRequire('scripts/fake_profiles/profilesData/production')
console.log(data)
function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`);
}


const connectDb = () => {
  let connectAttempts = 0;
  const dbUrl = `${config.mongoDb.url}${config.mongoDb.collection}`
  const dbConfig = { 
    user: config.mongoDb.username,
    pass: config.mongoDb.password,
  }
  return new Promise((resolve, reject) => {
    mongoose.connect(dbUrl, dbConfig)
    
    mongoose.connection.on('connected', () => {
      console.log('Connected to mongodb!')
      connectAttempts = 0
      resolve(true)
    })

    mongoose.connection.on('error', (error) => {
      console.error('Mongoose connection has failed.')
      reject(error)
    })
  });
}

const getSeqNumber = () => {
  console.log('WROOOM')
  return UsersSettings.findOne({}).exec()
}

const constructProfiles = (settings) => {
  const females = data.female
  const males = data.male
  const endResult = []
  let seqNumber = settings.nextSeqNumber
  _.forEach(males, (userData) => {
    const user = new UserBuilder()
      .setUserSeqNumber(seqNumber++)
      .setGenderMale()
      .setFacebookId(userData.facebookId)
      .setFirstName(userData.profile.firstName)
      .setLastName(userData.profile.lastName)
      .setEmail(userData.profile.email)
      .setAge(24)
      .withPreferencesGender({ male: false, female: true, })
    _.forEach(userData.videos, (vid) => {
      user.addProfileVideo({
        _id: vid._id,
        storagePath: vid.storagePath,
        thumbnailName: vid.thumbnailName,
        videoName: vid.videoName,
        review: {
          approved: true,
        },
      })
    })
    endResult.push(user)
  })
  _.forEach(females, (userData) => {
    const user = new UserBuilder()
      .setUserSeqNumber(seqNumber++)
      .setGenderFemale()
      .setFacebookId(userData.facebookId)
      .setFirstName(userData.profile.firstName)
      .setLastName(userData.profile.lastName)
      .setEmail(userData.profile.email)
      .setAge(24)
      .withPreferencesGender({ male: true, female: false, })
    _.forEach(userData.videos, (vid) => {
      user.addProfileVideo({
        _id: vid._id,
        storagePath: vid.storagePath,
        thumbnailName: vid.thumbnailName,
        videoName: vid.videoName,
        review: {
          approved: true,
        },
      })
    })
    endResult.push(user)
  })
  return Promise.resolve({
    profilesData: endResult,
    seqNumber: seqNumber,
  })
}

connectDb()
  .then(() => getSeqNumber())
  .then((data) => constructProfiles(data))
  .then((data) => {
    const profilesData = data.profilesData
    const seqNumber = data.seqNumber
    console.log('Next seq number:', seqNumber)
    const operations = _.map(profilesData, (profileData) => new UserModel(profileData).save())
    operations.push(UsersSettings.update({}, { nextSeqNumber: seqNumber, }).exec())
    return Promise.all(operations)
  })
  .then((success) => {
    console.log('Successfully added all profiles')
    mongoose.connection.close(function () {
      process.exit(0)
    })
  })
  .catch((err) => {
    console.error('Failed to add profiles', err)
  })
