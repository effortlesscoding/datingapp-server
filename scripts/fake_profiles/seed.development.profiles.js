'use strict'
const _ = require('lodash')
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect
const sinon = require('sinon')

const DbSetup = rootRequire('specs/setuphelpers/DbSetup.js')
const setup = new DbSetup()
const QBAuthUseCase = rootRequire('apps/webserviceapp/usecases/user/auth.quickblox').UseCase
const config = rootRequire('config')
const WebServiceApp = rootRequire('apps/webserviceapp')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const UserReviewProfilesUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase
const UserLoginUseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase
const MockUser = rootRequire('specs/mocks/MockUser')

//https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai

function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`)
}

chai.use(chaiHttp)

var count = 0

describe('seeding', () => {

  const TOTAL_USERS = 50

  before((done) => {
    setup.connect(true)
      .then(() => {
        return setup.resetDatabase()
      })
      .then(() => {
        done()
      })
      .catch((error) => {
        done(error)
      })
  })

  after(() => {
    setup.disconnect()
  })

  it('inserts fake profiles', function(done) {
    this.timeout(20000)
    insertRealUsers()
      .then((usersIds) => {
        console.log('Inserted real users')
        return insertFakeUsers(usersIds)
      })
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('Error', e)
      })
  })

  function insertRealUsers(users) {
    const anhFbId = 10208930797610536
    const freelancerFbId = 1307614435952188
    let userAnh = new MockUser()
                  .setUserSeqNumber(count++)
                  .setFacebookId(anhFbId)
                  .setGenderMale()
                  .setFirstName('Anh')
                  .setLastName('Dao')
                  .setAge(27)
    // Add quickblox ids

    let userDiviya = new MockUser()
                  .setUserSeqNumber(count++)
                  .setFacebookId(freelancerFbId)
                  .setGenderMale()
                  .setFirstName('Diviya')
                  .setLastName('Vikash')
    return Promise.all([
      userDiviya.getModel().save(),
      userAnh.getModel().save(),
    ]).then((users) => {
      _.forEach(users, (user) => {
        user.qbchat = {
          login: QBAuthUseCase.defaultLogin(user),
          password: QBAuthUseCase.defaultPassword(user),
        }
      })
      return Promise.all([
        users[0].save(),
        users[1].save()
      ])
    })
    .then((users) => {
      return [
        users[0]._id,
        users[1]._id,
      ]
    })
    .catch((e) => {
      console.error(e)
      return e
    })
  }

  function insertFakeUsers(realUserIds) {
    console.log('realUserIds', realUserIds)
    let promises = [];
    for (let i = count + 1; i < TOTAL_USERS + 1; i++) {
      console.log('i#', i)
      let user = new MockUser()
                    .setUserSeqNumber(i)
                    .setFacebookId(i * 10);
      if (i % 2 === 0) {
        user.setEmail(`tester${i}@gmail.com`)
            .setFirstName(`Girl #${i}`)
            .setLastName('Girly')
            .setGenderFemale()
            .addProfileVideo({
              _id: 0,
              storagePath : 'profiles/female/',
              uploaded : true,
              videoName : `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .addProfileVideo({
              _id: 1,
              storagePath : 'profiles/female/',
              uploaded : true,
              videoName : `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .addProfileVideo({
              _id: 2,
              storagePath : 'profiles/female/',
              uploaded : true,
              videoName : `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .setAge(27)
            .setQbLogin('qb_1')
            .setQbPassword('qb_password')
            .setQbId(16306353)
            .setUserSeqNumber(i)
            .setIsDeleted(false)
        _.forEach(realUserIds, (userId) => {
          user.addProfileLiked({ _id: userId, })
        })
      } else {
        user.setGenderMale()
            .setAge(27)
            .setEmail(`tester${i}@gmail.com`)
            .setFirstName(`Boy #${i}`)
            .setLastName('Boyish')
            .addProfileVideo({
              _id: 0,
              storagePath: 'profiles/male/',
              uploaded: true,
              videoName: `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .addProfileVideo({
              _id: 1,
              storagePath : 'profiles/male/',
              uploaded : true,
              videoName : `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .addProfileVideo({
              _id: 2,
              storagePath : 'profiles/male/',
              uploaded : true,
              videoName : `${i % 20}.mp4`,
              thumbnailName: 'placeholder.jpg',
              review: {
                approved: true,
                reason: 'Great video!',
              },
            })
            .setQbLogin('qb_2')
            .setQbPassword('qb_password')
            .setQbId(16306354)
            .setUserSeqNumber(i)
            .setIsDeleted(false)
        _.forEach(realUserIds, (userId) => {
          user.addProfileLiked({ _id: userId, })
        })
      }
      promises.push(user.getModel().save());
    }
    return Promise.all(promises)
  }

});