const profiles = {
  male: [
    {
      facebookId: 1641047379523271,
      profile: {
        firstName: 'Simone',
        lastName: 'Mancini',
        email: 'mancini.simone@gmail.com',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/male/simone_mancini/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
    {
      facebookId: 10209925819576098,
      profile: {
        firstName: 'Ranin',
        lastName: 'Jayasekara',
        email: 'djranin722@gmail.com',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/male/ranin_jayasekara/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
    {
      facebookId: 1293959380670928,
      profile: {
        firstName: 'Ateeq',
        lastName: 'Chaudhry',
        email: 'm.ateeqn@rocketmail.com',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/male/ateeq_chaudhry/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
    {
      facebookId: 10154066732082213,
      profile: {
        firstName: 'Christian',
        lastName: 'Hutchinson',
        email: 'kristanh@outlook.com.au',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/male/christian_hutchinson/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
  ],
  female: [
    {
      facebookId: 1588902447802908,
      profile: {
        firstName: 'Àïmee',
        lastName: 'Wïlsön',
        email: 'daimee1210@hotmail.com',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/female/aimee_wilson/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
        {
          _id: 1,
          storagePath: 'test_profiles/female/aimee_wilson/',
          videoName: '1.mp4',
          thumbnailName: '1.jpg',
        },
        {
          _id: 2,
          storagePath: 'test_profiles/female/aimee_wilson/',
          videoName: '2.mp4',
          thumbnailName: '2.jpg',
        },
        {
          _id: 3,
          storagePath: 'test_profiles/female/aimee_wilson/',
          videoName: '3.mp4',
          thumbnailName: '3.jpg',
        },
      ],
    },
    {
      facebookId: 10210976766802775,
      profile: {
        firstName: 'Brenna',
        lastName: 'Symonds',
        email: 'frecklettee@gmail.com',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/female/brenna_symonds/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
    {
      facebookId: 10205753662399564,
      profile: {
        firstName: 'Oluchi',
        lastName: 'Marcus-Mbachu',
        email: '',
      },
      videos: [
        {
          _id: 0,
          storagePath: 'test_profiles/female/oluchi_marcus_mbachu/',
          videoName: '0.mp4',
          thumbnailName: '0.jpg',
        },
      ],
    },
  ],
}

module.exports = profiles