const App = require('./apps/webserviceapp/')
const config = require('./config')
const dbUrl = `${config.mongoDb.url}${config.mongoDb.collection}`
const dbConfig = { 
  user: config.mongoDb.username,
  pass: config.mongoDb.password,
}
new App(dbUrl, dbConfig).start()