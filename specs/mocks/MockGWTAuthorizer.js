const sinon = require('sinon')
const GoogleJWTAuthoriser = rootRequire(`apps/webserviceapp/api/v2/storage/google.jwt.auth.js`)

function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`)
}

class MockGWTAuthorizer {
  
  mock() {
    this.authorizeStub = sinon.stub(GoogleJWTAuthoriser, 'authorise', (req, res, next) => {
      req.auth = {
        access_token: '1234567TEST'
      }
      next()
    })
  }

  restore() {
    if (this.authorizeStub) {
      this.authorizeStub.restore()
    }
  }
}

module.exports = new MockGWTAuthorizer()