/*jslint node: true */
'use strict';
var EventEmitter = require('events').EventEmitter;

function MockResponse() {
   EventEmitter.call(this);
}

MockResponse.prototype = Object.create(EventEmitter.prototype);

MockResponse.prototype.success = function(response, code) {
  this.emit('success', { response : response, code: code});
};

MockResponse.prototype.error = function(error, code) {
  this.emit('error', { error : error, code: code});
};


module.exports = MockResponse;