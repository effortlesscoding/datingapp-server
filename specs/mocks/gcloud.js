'use strict';
var stream = require('stream');

/** Stub storage **/
var storage = function() {
	console.log('Mocked storage.');
};

storage.prototype.bucket = function(config) {
	return new bucket();
}

/** Stub bucket **/

var bucket = function() {
	console.log('Mocked bucket.');
};

bucket.prototype.file = function(config) {
	return new file();
}


/** Stub file **/

var file = function() {
	console.log('Mocked file.');
};

file.prototype.createWriteStream = function() {
	// Mocks the stream...
	var writestream = new stream.Stream();
	writestream.emit('finish');
	return writestream;
}

// var Storage = gcloud.storage(..)
// var bucket = Storage.bucket(...);
// var file = bucket.file(...);
// var stream = file.createWriteStream();

module.exports = new storage();