
const passport = require('passport')
const sinon = require('sinon')

class MockPassport {
  constructor() {
    this.authenticateStub = null
    this.initializeStub = null
    this.currentUser = {
      id: 'wawawawa',
      firstName: 'lalalal',
      lastName: 'wawawa32',
    }
  }

  willReturn(user) {
    this.currentUser = user
  }

  mock() {
    this.authenticateStub = sinon.stub(passport, 'authenticate', () => {
      return this.facebookMiddleware.bind(this)
    })
  }

  facebookMiddleware(req, res, next) {
    req.user = this.currentUser
    return next();
  }

  setUser(user) {
    this.currentUser = user
  }

  restore() {
    if (this.authenticateStub) this.authenticateStub.restore()
    if (this.initializeStub) this.initializeStub.restore()
    return Promise.resolve()
  }
}

module.exports = new MockPassport();