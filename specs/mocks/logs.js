/*jslint node: true */
/*jslint esversion : 6*/
'use strict';

function Logger() {
}

Logger.prototype.info = function(message) {
  if (typeof message === 'object') {
    //console.log(`TEST_LOG ---- ${JSON.stringify(message)}`);
  } else {
    //console.log(`TEST_LOG ---- ${message}`);
  }
};


Logger.prototype.debug = function(message) {
  if (typeof message === 'object') {
    //console.log(`TEST_LOG_DEBUG ---- ${JSON.stringify(message)}`);
  } else {
    //console.log(`TEST_LOG_DEBUG ---- ${message}`);
  }
};


Logger.prototype.error = function(message) {
  if (typeof message === 'object') {
    //console.log(`TEST_LOG_ERROR ---- ${JSON.stringify(message)}`);
  } else {
    //console.log(`TEST_LOG_ERROR ---- ${message}`);
  }
};

module.exports = new Logger();