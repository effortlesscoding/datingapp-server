/*jslint node: true */
/*jslint esversion : 6*/
'use strict';

function MockRequest(bodyData, user) {
  this.user = {
    email: 'default@email.com',
    firstName : 'defaultFirstName',
    lastName : 'defaultLastName',
    gender : 'male',
    id   : 1,
    accessToken: 0,
    refreshToken : 0
  };
  return this;
}

MockRequest.prototype.setBody = function(data) {
  this.body = data;
  return this;
};


MockRequest.prototype.setAuth = function(data) {
  this.auth = data;
  return this;
};

MockRequest.prototype.setQuery = function(queryData) {
  this.query = queryData;
  return this;
};

MockRequest.prototype.setUserId = function(id) {
  this.user.id = id;
  return this;
};


module.exports = MockRequest;