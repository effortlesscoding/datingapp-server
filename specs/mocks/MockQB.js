'use strict';
const _ = require('lodash')
let QB = require('quickblox')
let sinon = require('sinon')

class MockQB {
  constructor() {
    this.usersCreateStub = null
    this.usersLoginStub = null
    this.createSession = null
    this.loginError = null
    this.loginResult = null
    this.registerError = null
    this.registerResult = null
    QB.users = {
      create: () => {},
      login: () => {},
    }
  }

  mockUsersApi() {
    this.usersCreateStub = sinon.stub(QB.users, 'create', (params, callback) => {
      console.log('STUB function create', this.registerResult)
      if (!this.registerResult) {
        callback(this.registerError, {
          login: params.login,
          id: 1
        })
      } else {
        callback(this.registerError, this.registerResult)
      }
    })
    this.usersLoginStub = sinon.stub(QB.auth, 'login', (params, callback) => {
      if (!this.loginResult) {
        callback(this.loginError, {
          login: params.login,
          id: 1
        })
      } else {
        callback(this.loginError, this.loginResult)
      }
    })
  }

  mock() {
    console.log('MOCKING!')
    return new Promise((resolve, reject) => {
      this.createSessionStub = sinon.stub(QB, 'createSession', (callback) => {
        callback(null, {})
        this.mockUsersApi()
      })
      resolve();
    });
  }

  restore() {
    if (QB.auth.login.restore) QB.auth.login.restore()
    if (QB.users.create.restore) QB.users.create.restore()
    if (QB.createSession.restore) QB.createSession.restore()
    this.usersCreateStub = null
    this.usersLoginStub = null
    this.createSessionStub = null
    return Promise.resolve()
  }

  willReturn(params) {
    this.loginError = _.get(params, 'login.error', null)
    this.loginResult = _.get(params, 'login.result', null)
    this.registerError = _.get(params, 'register.error', null)
    this.registerResult = _.get(params, 'register.result', null)
  }
}


module.exports = new MockQB()