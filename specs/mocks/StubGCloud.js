/*jslint node: true */
/*jslint esversion : 6*/
'use strict';
var https = require('https');
var url = require('url');
var logger = require('./logs');

function StubGCloud(location) {
  this.url = url.parse(location);
  return this;
}

StubGCloud.prototype.getUploadStatus = function(totalSize) {
  return new Promise(getUploadStatus.bind(this));

  function getUploadStatus(resolve ,reject) {
    let options = {
      hostname: this.url.host,
      port: 443,
      path: this.url.pathname + this.url.search,
      method: 'PUT',
      headers: {
        'Content-Length' : 0,
        'Content-Range' : `bytes */${totalSize ? totalSize : '*'}`
      }
    };
    logger.info(options.headers);
    let req = https.request(options, (res) => {
      debugger;

      let data = '';
      res.on('data', (d) => {
        logger.debug('Chunk:');
        logger.debug(d.toString('utf-8'));
        if (!data) {
          data = d;
        } else {
          data += d;
        }
      });
      res.on('end', (e) => {
        debugger;
        resolve({ 
          statusCode : res.statusCode, 
          headers : res.headers,
          data : data.toString('utf-8')
        });
      });
    });
    req.end();
  }
};


/** TO_DO : Fix... Right now, it FULLY uploads because there is no Content-Range header **/
StubGCloud.prototype.uploadChunk = function(mimeType, buffer, startByte, totalSize) {
  return new Promise(makeUploadCall.bind(this));

  function makeUploadCall(resolve ,reject) {
    if (!buffer) { return reject(new Error('No valid parameters.')); }
    if (!mimeType) { return reject(new Error('No valid parameters.')); }
    debugger;
    let headers = {
      'Content-Type' : mimeType,
      'Content-Length' : `${buffer.length}`,
      'Content-Range' : `bytes ${startByte}-${startByte + buffer.length - 1}/${totalSize}`
    };
    logger.debug('makeUploadCall():');
    logger.debug(headers);
    let options = {
      hostname: this.url.host,
      port: 443,
      path: this.url.pathname + this.url.search,
      method: 'PUT',
      headers: headers
    };

    let req = https.request(options, (res) => {
      debugger;

      let data = '';
      res.on('data', (d) => {
        logger.debug('Chunk!');
        if (!data) {
          data = d;
        } else {
          data += d;
        }
      });
      res.on('end', (e) => {
        debugger;
        resolve({ 
          statusCode : res.statusCode,
          headers : res.headers,
          data : data.toString('utf-8')
        });
      });
    });
    req.write('bytes ');
    req.write(buffer);
    req.end();

    req.on('error', (e) => {
      logger.error(e);
      throw e;
    });
  }
};


module.exports = StubGCloud;