'use strict';
const _ = require('lodash')
const mongoose = require('mongoose');
const logger = require('./logs');

const genders = rootRequire(`apps/webserviceapp/models/GendersEnum`);
const UserModel = rootRequire(`apps/webserviceapp/models/User`);
const ProfileSchema = rootRequire(`apps/webserviceapp/models/ProfileSchema`);
const VideoSchema = rootRequire(`apps/webserviceapp/models/VideoSchema`);

const ProfileModel = mongoose.model('Profile', ProfileSchema);
const VideoModel = mongoose.model('Video', VideoSchema);

const matchingDefaults = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').Constants

function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`);
}

/**
 * @brief This must coincide with the user model, obviously.
 * @details [long description]
 * @return [description]
 */
class MockUser {
  constructor() {
    this.seqNumber = 0;
    this.gender = genders.MALE;
    this.profile = {
      firstName : 'default_name',
      lastName : 'default_last_name',
      videos : {
      },
      email : 'default_email',
      interests: [],
      aboutMe: [],
      occupation: '',
      study: '',
      age: 48,
      location: {
        lat: 37.8136,
        lng: 144.9631,
      },
    };
    this.preferences = {
      minAge: matchingDefaults.DEFAULT_MIN_AGE,
      maxAge: matchingDefaults.DEFAULT_MAX_AGE,
      distance: matchingDefaults.DEFAULT_DISTANCE_PREFERENCE_KM,
      gender: {
        male: false,
        female: true,
      },
    };
    this.facebookId = 0;
    this.reviewedUpTo = -1;
    this.profilesLiked = [];
    this.profilesMatched = [];
    this.profilesToReview = {};
    this.review = { approved: true, }
    return this;
  }

  setFacebookId(id) {
    this.facebookId = id;
    return this;
  };

  setUserId(id) {
    this._id = id
    return this
  }

  setUserSeqNumber(seqNumber) {
    this.seqNumber = seqNumber;
    return this;
  }

  setGenderMale() {
    this.gender = genders.MALE;
    return this;
  }

  setGenderFemale() {
    this.gender = genders.FEMALE;
    return this;
  }

  setReviewedUpTo(seqNumber) {
    this.reviewedUpTo = seqNumber;
    return this;
  }

  setEmail(email) {
    this.profile.email = email;
    return this;
  }

  setFirstName(firstName) {
    this.profile.firstName = firstName;
    return this;
  }

  setLastName(lastName) {
    this.profile.lastName = lastName;
    return this;
  }

  setAge(age) {
    this.profile.age = age;
    return this;
  }
  
  addProfileVideo(data) {
    let video = new VideoModel(data);
    _.set(this.profile, ['videos', data._id] , video.toObject({ versionKey: false }))
    return this;
  };

  addProfileVideos(videos) {
    videos.forEach((video) => {
      const videoModel = new VideoModel(video);
      this.profile.videos.push(videoModel.toObject({ versionKey: false }));
    });
    return this;
  }

  addProfileToReview(data) {
    let profile = new ProfileModel({
      _id: data._id,
    })
    let profilesToReview = _.get(this, ['profilesToReview', data.priority], [])
    profilesToReview.push(profile)
    _.set(this, ['profilesToReview', data.priority], profilesToReview)
    return this;
  };

  addProfileMatched(data) {
    let profile = new ProfileModel(data);
    this.profilesMatched.push(new ProfileModel({
      _id: data._id 
    }));
    return this;
  };

  addProfileLiked(data) {
    let profile = new ProfileModel(data);
    this.profilesLiked.push(new ProfileModel({
      _id: data._id 
    }));
    return this;
  };

  addLikedProfileId(id) {
    this.profilesLiked.push({ _id : id});
    return this;
  };

  addMatchedProfileId(id) {
    this.profilesMatched.push({
      _id : id,
      conversationId : null,
      updatedAt : Date.now(),
      createdAt : Date.now()
    });
    return this;
  };

  addMatchedProfile(data) {
    throw new Error('Unimplemented')
    return this;
  };

  setQbLogin(login) {
    if (!this.qbchat) this.qbchat = {};

    this.qbchat.login = login;
    return this;
  };

  setQbPassword(password) {
    if (!this.qbchat) this.qbchat = {};

    this.qbchat.password = password;
    return this;
  };

  setQbId(id) {
    if (!this.qbchat) this.qbchat = {};

    this.qbchat.id = id;
    return this;
  };

  setFcmToken(token) {
    if (!this.fcm) this.fcm = {};

    this.fcm.token = token;
    return this;
  }

  withInterests(interests) {
    this.profile.interests = interests;
    return this;
  }

  withOccupation(occupation) {
    this.profile.occupation = occupation;
    return this;
  }

  withStudy(study) {
    this.profile.study = study;
    return this;
  }

  withAboutMe(aboutMe) {
    this.profile.aboutMe = aboutMe;
    return this;
  }

  withPreferencesGender(gender) {
    this.preferences.gender = gender;
    return this;
  }

  withPreferencesMinAge(minAge) {
    this.preferences.minAge = minAge;
    return this;
  }

  withPreferencesMaxAge(maxAge) {
    this.preferences.maxAge = maxAge;
    return this;
  }

  withPreferencesDistance(distance) {
    this.preferences.distance = distance;
    return this;
  }

  setIsDeleted(isDeleted) {
    this.isDeleted = isDeleted;
    return this;
  }

  getModel() {
    return new UserModel(this)
  }
  
}




module.exports = MockUser;