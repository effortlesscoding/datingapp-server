const _ = require('lodash');
const specs = require('./helper');
const expect = require('chai').expect;

const controller = rootRequire(`apps/adminapp/api/v1/admin/controller`);
const logger = rootRequire(`specs/helpers/logger`);

function rootRequire(path) {
  const ROOT_PATH = '../../../../../';
  return require(`${ROOT_PATH}${path}`)
}
describe('controller', () => {

  const userIdsNoReview = [
    '53cb6b9b4f4ddef1ad47f941',
    '53cb6b9b4f4ddef1ad47f942',
    '53cb6b9b4f4ddef1ad47f943',
    '53cb6b9b4f4ddef1ad47f944',
  ]
  const userIdsOneVidNeedsReview = [
    '53cb6b9b4f4ddef1ad47f945',
    '53cb6b9b4f4ddef1ad47f946',
    '53cb6b9b4f4ddef1ad47f947',
  ]
  const userIdsTwoVidsNeedReview = [
    '53cb6b9b4f4ddef1ad47f948',
    '53cb6b9b4f4ddef1ad47f949',
    '53cb6b9b4f4ddef1ad47f950',
  ]
  const userIdsNoVids = [
    '53cb6b9b4f4ddef1ad47f951',
    '53cb6b9b4f4ddef1ad47f952',
    '53cb6b9b4f4ddef1ad47f953',
  ]

  before((done) => {
    specs.setupDatabase()
      .then(() => specs.setupQuickblox())
      .then(() => done())
      .catch((e) => {
        console.error('Admin error::', e)
        done(new Error(`Error: ${e.message}`))
      })
  })

  beforeEach((done) => {
    specs.removeAllUsers()
      .then(() => specs.userIdsNoReview(userIdsNoReview))
      .then(() => specs.userIdsOneVidNeedsReview(userIdsOneVidNeedsReview))
      .then(() => specs.userIdsTwoVidsNeedReview(userIdsTwoVidsNeedReview))
      .then(() => specs.userIdsNoVids(userIdsNoVids))
      .then(() => done())
      .catch((e) => {
        console.error('Error', e)
        done(new Error(e.message));
      })
  })

  after((done) => {
    specs.closeDatabaseConnection()
      .then(specs.restoreQuickblox.bind(specs))
      .then(done)
      .catch((e) => done(new Error(e.message)))
  })

  describe('#getProfileVideosToReview', () => {
    it('should retrieve users who have not been reviewed at all (neither approved/rejected)', (done) => {
      const request = specs.request();
      const response = specs.response();

      response.on('success', (data) => {
        expect(data.response.users.length).to.be.equal(6);
        const usersIds = _.map(data.response.users, (user, i) => user._id.toString());
        const expectedIds = _.union(userIdsOneVidNeedsReview, userIdsTwoVidsNeedReview);
        const difference = _.xor(usersIds, expectedIds);
        expect(difference.length).to.be.equal(0);
        done();
      });
      response.on('error', (error) => {
        done(new Error(error.message));
      });

      controller.getProfileVideosToReview(request, response, (e) => done(new Error('Wrong path')));
    })
  })

  describe('#approveProfileVideo', () => {

    it(`Given that a user has two videos to review,
        calling the API to approve a video should approve one video
        and leave the other video intact`, (done) => {
      const userId = userIdsTwoVidsNeedReview[0];
      const videoIdToReview = 0;
      const reason = 'Good video!';
      specs.getUserById(userId)
        .then((user) => {
          const indices = Object.keys(user.profile.videos.toObject({ versionKey: false, }))
          expect(indices.length).to.be.equal(3);
          const unreviewedVideosIds = _.filter(indices, (index) => {
            const video = user.profile.videos[index]
            const reviewFields = Object.keys(video.review.toObject({ versionKey: false }))
            return !video.review || reviewFields.length === 0
          });
          expect(unreviewedVideosIds.length).to.be.equal(2);
          const videoExists = _.some(unreviewedVideosIds, (videoId) => {
            return parseInt(videoId) === videoIdToReview
          })
          expect(videoExists).to.be.true
          return specs.approveVideoViaAPI(userId, videoIdToReview, reason)
        })
        .then((response) => {
          expect(response.code).to.be.equal(200);
          return specs.getUserById(userId);
        })
        .then((user) => {
          const indices = Object.keys(user.profile.videos.toObject({ versionKey: false, }))
          expect(indices.length).to.be.equal(3)
          const videoStillExists = _.some(indices, (index) => {
            return user.profile.videos[index]._id === videoIdToReview
          })
          expect(videoStillExists).to.be.equal(true);

          const unreviewedVideosIndices = _.filter(indices, (index) => {
            const video = user.profile.videos[index]
            const reviewFields = Object.keys(video.review.toObject({ versionKey: false }))
            return !video.review || reviewFields.length === 0
          });
          expect(unreviewedVideosIndices.length).to.be.equal(1)
          expect(user.profile.videos[unreviewedVideosIndices[0]]).to.not.be.equal(videoIdToReview)
          done()
        })
        .catch((error) => {
          console.error('Error', error)
          done(error)
        })
    })

    it(`should approve a video when approve is called 
        and a push notification has to be sent`, (done) => {
      const videoIdToReview = 0;
      const reason = 'Great video';
      const userId = userIdsOneVidNeedsReview[0];
      specs.approveVideoViaAPI(userId, videoIdToReview, reason)
        .then(() => specs.getUserById(userId))
        .then((user) => {
          const videos = user.profile.videos.toObject({ versionKey: false, })
          const matchingVids = []
          _.forOwn(videos, (vid, i) => {
            if (vid._id === videoIdToReview) {
              matchingVids.push(vid)
            }
          })
          expect(matchingVids.length).to.be.equal(1);
          const matchingVid = _.first(matchingVids);
          expect(matchingVid.review.reason).to.be.equal(reason);
          expect(matchingVid.review.approved).to.be.equal(true);
          done();
        })
        .catch((e) => done(e));
    })

    it('should return error if a non-existing video id is specified', (done) => {
      specs.approveVideoViaAPI(userIdsOneVidNeedsReview[0], 1000, 'GOOD video')
        .then((res) => {
          done(new Error('Should not be in success'))
        })
        .catch((e) => {
          expect(e.error).to.be.equal('Could not update the videos')
          done();
        });
    })

    it('should return error if an already reviewed video id is specified', (done) => {
      specs.approveVideoViaAPI(userIdsOneVidNeedsReview[0], 1, 'GOOD video')
        .then(() => done(new Error('Should not be in success')))
        .catch((e) => {
          expect(e.error).to.be.equal('Video has already been reviewed');
          done();
        });
    })

    it('should return error if a non-existing user id is specified', (done) => {
      specs.approveVideoViaAPI('53cb6b9b4f4ddef1ad47f111', 1, 'GOOD video')
        .then(() => done(new Error('Should not be in success')))
        .catch((e) => {
          console.error('e.error###', e.error)
          expect(e.error).to.be.equal('User just could not be found');
          done();
        })
    })
  })

  describe('#rejectProfileVideo', () => {

    it('should reject a video when reject API is called', (done) => {
      const videoIdToReject = 0;
      const userId = userIdsOneVidNeedsReview[0];
      const reason = 'Bad video';
      specs.getUserById(userId)
        .then((user) => {
          const videos = user.profile.videos.toObject({ versionKey: false, })
          const indices = Object.keys(videos)
          expect(indices.length).to.be.equal(2)
          const someVideosReviewed = _.some(indices, (index) => {
            return user.profile.videos[index].review
          })
          expect(someVideosReviewed).to.be.equal(true);
          const matchingVids = []
          _.forEach(indices, (index) => {
            if (parseInt(videos[index]._id) === videoIdToReject) {
              matchingVids.push(videos[index])
            }
          });
          expect(matchingVids.length).to.be.equal(1)
          const matchingVid = _.first(matchingVids)
          const matchingVidReview = matchingVid.review.toObject({ versionKey: false, })
          expect(Object.keys(matchingVidReview).length).to.be.equal(0)
          return specs.rejectVideoViaAPI(userId, videoIdToReject, reason)
        })
        .then((res) => specs.getUserById(userId))
        .then((user) => {
          const videos = user.profile.videos.toObject({ versionKey: false, })
          const indices = Object.keys(videos)
          expect(indices.length).to.be.equal(2)

          const matchingVids = []
          _.forEach(indices, (index) => {
            if (parseInt(videos[index]._id) === videoIdToReject) {
              matchingVids.push(videos[index])
            }
          })
          expect(matchingVids.length).to.be.equal(1)
          const matchingVid = _.first(matchingVids)
          expect(matchingVid.review.reason).to.be.equal(reason)
          expect(matchingVid.review.approved).to.be.equal(false)
          done();
        })
        .catch((e) => {
          console.error('Error##', e)
          done(e)
        });
    });

    it('should return an error when requesting a non-existing user', (done) => {
      const videoIdToReject = 1;
      const userId = '53cb6b9b4f4ddef1ad47f111';
      const reason = 'Bad video';
      specs.rejectVideoViaAPI(userId, videoIdToReject, reason)
        .then((data) => done(new Error('Should not succeed')))
        .catch((e) => {
          expect(e.error).to.be.equal('User just could not be found');
          done();
        });
    });

    it(`should return an error when requesting a non-existing video 
        for an existing user`, (done) => {
      const videoIdToReject = 999;
      const userId = userIdsOneVidNeedsReview[0];
      const reason = 'Bad video';
      specs.rejectVideoViaAPI(userId, videoIdToReject, reason)
        .then((data) => done(new Error('Should not succeed')))
        .catch((e) => {
          console.error('Error###', e)
          expect(e.error).to.be.equal('Could not update the videos');
          done();
        });
    });

    it(`should return an error when requesting an already reviewed video 
        for an existing user`, (done) => {
      const videoIdToReject = 1;
      const userId = userIdsOneVidNeedsReview[0];
      const reason = 'Bad video';
      specs.rejectVideoViaAPI(userId, videoIdToReject, reason)
        .then((data) => done(new Error('Should not succeed')))
        .catch((e) => {
          console.error('rejectVideoViaAPI##', e)
          expect(e.error).to.be.equal('Video has already been reviewed');
          done();
        });
    });

  });


});