'use strict';
const _ = require('lodash')

const SpecsSetup = rootRequire(`specs/helpers/SpecsSetup`)
const controller = rootRequire(`apps/adminapp/api/v1/admin/controller`)
const MockUser = rootRequire(`specs/mocks/MockUser`)

const User = rootRequire(`apps/webserviceapp/models/User`)
const logger = rootRequire(`specs/helpers/logger`)

function rootRequire(path) {
  const ROOT_PATH = '../../../../../';
  return require(`${ROOT_PATH}${path}`)
}

class AdminSpecHelper extends SpecsSetup {
  
  constructor() {
    super()
    this.facebookIdCounter = 0
  }

  userIdsOneVidNeedsReview(ids) {
    return new Promise((resolve, reject) => {
      const users = _.map(ids, (id, i) => {
        return new User(
          new MockUser()
            .setUserId(id)
            .setUserSeqNumber(this.facebookIdCounter)
            .setFacebookId(this.facebookIdCounter++)
            .setGenderMale()
            .setFirstName(`default_first_name${i}`)
            .setLastName(`default_last_name${i}`)
            .addProfileVideo({
              _id: 0,
              videoName: 'test_0.mp4',
              thumbnailName: 'test_0.jpg',
              uploaded: true,
              storagePath: 'profile/males/1'
            })
            .addProfileVideo({
              _id: 1,
              videoName: 'test_0.mp4',
              thumbnailName: 'test_0.jpg',
              uploaded: true,
              storagePath: 'profile/males/1',
              review: {
                approved: false,
                reason: 'Video sucks'
              }
            })
        );
      });
      const promises = _.map(users, (u) => u.save());
      Promise.all(promises).then(resolve).catch(reject);
    });
  } 

  userIdsTwoVidsNeedReview(ids) {
    return new Promise((resolve, reject) => {
      const users = _.map(ids, (id, i) => {
        return new User(new MockUser()
                  .setUserId(id)
                  .setUserSeqNumber(this.facebookIdCounter)
                  .setFacebookId(this.facebookIdCounter++)
                  .setGenderMale()
                  .setFirstName(`default_first_name${i}`)
                  .setLastName(`default_last_name${i}`)
                  .addProfileVideo({
                    _id: 0,
                    videoName: 'test_0.mp4',
                    thumbnailName: 'test_0.jpg',
                    uploaded: true,
                    storagePath: 'profile/males/1',
                    review: null
                  })
                  .addProfileVideo({
                    _id: 1,
                    videoName: 'test_0.mp4',
                    thumbnailName: 'test_0.jpg',
                    uploaded: true,
                    storagePath: 'profile/males/1',
                    review: null
                  })
                  .addProfileVideo({
                    _id: 2,
                    videoName: 'test_0.mp4',
                    thumbnailName: 'test_0.jpg',
                    uploaded: true,
                    storagePath: 'profile/males/1',
                    review: {
                      approved: false,
                      reason: 'Video sucks'
                    }
                  }));
      });
      const promises = _.map(users, (u) => u.save());
      Promise.all(promises).then(resolve).catch(reject);
    });
  }

  userIdsNoReview(ids) {
    return new Promise((resolve, reject) => {
      const users = _.map(ids, (id, i) => {
        return new User(new MockUser()
                  .setUserId(id)
                  .setUserSeqNumber(this.facebookIdCounter)
                  .setFacebookId(this.facebookIdCounter++)
                  .setGenderMale()
                  .setFirstName(`default_first_name${i}`)
                  .setLastName(`default_last_name${i}`)
                  .addProfileVideo({
                    _id: 0,
                    videoName: 'test_0.mp4',
                    thumbnailName: 'test_0.jpg',
                    uploaded: true,
                    storagePath: 'profile/males/1',
                    review: {
                      approved: false,
                      reason: 'Video sucks'
                    }
                  }));
      });
      const promises = _.map(users, (u) => u.save());
      Promise.all(promises).then(resolve).catch(reject);
    });
  }

  userIdsNoVids(ids) {
    return new Promise((resolve, reject) => {
      const users = _.map(ids, (id, i) => {
        return new User(new MockUser()
                  .setUserId(id)
                  .setUserSeqNumber(this.facebookIdCounter)
                  .setFacebookId(this.facebookIdCounter++)
                  .setGenderMale()
                  .setFirstName(`default_first_name${i}`)
                  .setLastName(`default_last_name${i}`));
      });
      const promises = _.map(users, (u) => u.save());
      Promise.all(promises).then(resolve).catch(reject);
    });
  }

  getUserById(id) {
    return User.findOne({_id: id}).exec();
  }

  approveVideoViaAPI(userId, videoId, reason) {
    return new Promise((resolve, reject) => {
      const req = this.request().setBody(
        {
          userId: userId,
          videoId: videoId,
          reason: reason
        }
      );
      const res = this.response();

      res.on('success', (data) => resolve(data));
      res.on('error', (error) => {
        console.error('###approveVideoViaAPI')
        reject(error);
      });
      controller.approveProfileVideo(req, res, () => reject(new Error('Wrong path')));
    });
  }

  rejectVideoViaAPI(userId, videoId, reason) {
    return new Promise((resolve, reject) => {

      const req = this.request()
                      .setBody({
                        userId: userId,
                        videoId: videoId,
                        reason: reason
                      });
      const res = this.response();

      res.on('success', (data) => resolve(data));
      res.on('error', (error) => {
        console.error(error);
        reject(error);
      });

      controller.rejectProfileVideo(req, res, (e) => done(new Error('Wrong path')));
    });
  }

}

module.exports = new AdminSpecHelper();