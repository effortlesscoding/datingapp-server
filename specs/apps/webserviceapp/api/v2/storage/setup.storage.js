'use strict'

const UserModel = rootRequire('apps/webserviceapp/models/User')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const MockUser = rootRequire('specs/mocks/MockUser')
const DbSetup = rootRequire('specs/setuphelpers/DbSetup')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class StorageAPISetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 0
  }

  addUser(userParams) {
    const user = new MockUser()
      .setUserId(userParams._id)
      .setUserSeqNumber(userParams.seqNumber || this.usersCount)
      .setFacebookId(userParams.facebookId)
      .setReviewedUpTo(userParams.reviewedUpTo || 0)
    this.usersCount++
    return new UserModel(user).save()
  }
}

module.exports = new StorageAPISetup()