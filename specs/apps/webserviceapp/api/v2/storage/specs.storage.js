const _ = require('lodash')

const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect
const sinon = require('sinon')
const setup = require('./setup.storage.js')
const config = rootRequire('config')
const WebServiceApp = rootRequire('apps/webserviceapp')
const UrlsUploadUseCase = rootRequire('apps/webserviceapp/usecases/storage/urls.upload').UseCase
const bucketName = _.get(config, 'google.storage.bucket', undefined)
let useCaseStub
function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

function stubUrlsUploadUseCase() {
  useCaseStub = sinon.stub(UrlsUploadUseCase, 'generateUploadUrls', (parameters) => {
    if (_.isNil(parameters.fileName) || _.isNil(parameters.directory) ||
        _.isNil(parameters.mimeType) || _.isNil(parameters.contentLength) ||
        _.isNil(parameters.accessToken)) {
      return Promise.reject(new Error('The generator has not been configured properly'))
    }
    if (parameters.contentLength <= 0) {
      return Promise.reject(new Error('The content length has to be more than 0'))
    }
    return Promise.resolve('http://somewheregood.com')
  })
}

chai.use(chaiHttp)

describe('/api/v2/storage', () => {


  let server
  let appWrapper
  const users = [
    { _id: '53cb6b9b4f4ddef1ad47f943', quickbloxId: 12344, videoId: 1, facebookId: 1, },
    { _id: '53cb6b9b4f4ddef1ad47f944', quickbloxId: 12345, videoId: 1, facebookId: 2, }
  ]

  before(function(done) {
    this.timeout(5000)
    setup.mockDependencies()
      .then(() => {
        appWrapper = new WebServiceApp(
          setup.getDatabaseTestUrl(),
          setup.getDatabaseTestConfiguration()
        )
        return appWrapper.start()
      })
      .then((app) => {
        server = app
      })
      .then(() => setup.resetDatabase())
      .then(() => done())
      .catch((e) => {
        done(new Error(e.message))
      })
  })

  after((done) => {
    setup.restoreDependencies()
      .then(() => appWrapper.stop())
      .then(() => done())
      .catch((e) => {
        console.error('Error##', e)
        done(new Error('Could not restore dependencies'))
      })
  })

  beforeEach((done) => {
    setup.resetDatabase()
      .then(() => setup.addUser(users[0]))
      .then(() => setup.addUser(users[1]))
      .then(() => stubUrlsUploadUseCase())
      .then(() => done())
      .catch((e) => {
        console.error('Error##', e)
        done(new Error(e.message))
      })
  })

  afterEach((done) => {
    useCaseStub.restore()
    done()
  })

  describe('GET /api/v2/storage/chats/:opponentId/videos', () => {
    it('should return upload urls, download urls, etc.', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=14000&thumbnailSize=1400`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200)
          const data = res.body.data
          expect(data.video.uploadUrl).to.exist
          expect(data.video.mimeType).to.exist
          expect(data.video.fileName).to.exist
          expect(data.video.storagePath).to.exist
          expect(data.video.size).to.exist
          expect(data.thumbnail.uploadUrl).to.exist
          expect(data.thumbnail.mimeType).to.exist
          expect(data.thumbnail.fileName).to.exist
          expect(data.thumbnail.storagePath).to.exist
          expect(data.thumbnail.size).to.exist
          const videoPath = `${data.video.storagePath}/${data.video.fileName}`
          const thumbnailPath = `${data.video.storagePath}/${data.thumbnail.fileName}`
          expect(data.video.accessUrl).to.be.equal(`https://${bucketName}.commondatastorage.googleapis.com/${videoPath}`)
          expect(data.thumbnail.accessUrl).to.be.equal(`https://${bucketName}.commondatastorage.googleapis.com/${thumbnailPath}`)
          done()
        })
    })

    it('should require file size of the video', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=&thumbnailSize=14000`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should require file size of the thumbnail', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=14000&thumbnailSize=`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should disallow videos above 20MB', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      const videoSize = 20971520
      const thumbnailSize = 2097151
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=${videoSize}&thumbnailSize=${thumbnailSize}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should disallow thumbnails above 2MB', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      const videoSize = 20971519
      const thumbnailSize = 2097152
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=${videoSize}&thumbnailSize=${thumbnailSize}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })
  })

  describe('GET /api/v2/storage/chats/:opponentId/videos', () => {

    it('should return upload urls, download urls, etc.', (done) => {
      const user = users[0]
      const videoId = users[0].videoId
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/profiles/videos/${videoId}/uploadUrls?videoSize=14000&thumbnailSize=8000`)
        .end((err, res) => {
          const data = _.get(res, 'body.data', null)
          expect(res.statusCode).to.be.equal(200)
          expect(data).to.exist
          expect(data.thumbnail.uploadUrl).to.exist
          expect(data.thumbnail.storagePath).to.exist
          expect(data.thumbnail.fileName).to.exist
          expect(data.thumbnail.size).to.exist
          expect(data.thumbnail.mimeType).to.exist
          expect(data.video.uploadUrl).to.exist
          expect(data.video.storagePath).to.exist
          expect(data.video.fileName).to.exist
          expect(data.video.size).to.exist
          expect(data.video.mimeType).to.exist

          const videoPath = `${data.video.storagePath}/${data.video.fileName}`
          const thumbnailPath = `${data.video.storagePath}/${data.thumbnail.fileName}`
          expect(data.video.accessUrl).to.be.equal(`https://${bucketName}.commondatastorage.googleapis.com/${videoPath}`)
          expect(data.thumbnail.accessUrl).to.be.equal(`https://${bucketName}.commondatastorage.googleapis.com/${thumbnailPath}`)
          done()
        })
    })

    it('should require file size of the video', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=&thumbnailSize=14000`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should require file size of the thumbnail', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=&thumbnailSize=14000`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should disallow videos above 20MB', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      const videoSize = 20971520
      const thumbnailSize = 2097151
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=&thumbnailSize=14000`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

    it('should disallow thumbnails above 2MB', (done) => {
      const user = users[0]
      const opponentId = users[1]._id
      setup.passport.willReturn({
        id: user.facebookId,
      })
      const videoSize = 20971519
      const thumbnailSize = 2097152
      chai.request(server)
        .get(`/api/v2/storage/chats/videos/${opponentId}/uploadUrls?videoSize=&thumbnailSize=14000`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          done()
        })
    })

  })
})