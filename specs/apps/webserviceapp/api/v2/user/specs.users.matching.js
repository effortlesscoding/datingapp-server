'use strict'
const _ = require('lodash')
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect
const sinon = require('sinon')

const config = rootRequire('config')
const WebServiceApp = rootRequire('apps/webserviceapp')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const Utils = rootRequire('apps/webserviceapp/helpers/utils/')
const UserReviewProfilesUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase
const UserLoginUseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase
const NotificationsUseCase = rootRequire('apps/webserviceapp/usecases/notifications/').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

chai.use(chaiHttp)

const Environment = rootRequire('specs/setuphelpers/Environment')
const resetDatabase = Environment.db.resetDatabase.bind(Environment.db)
const insertUsers = Environment.db.insertUsers.bind(Environment.db)
const insertUsersBulk = Environment.db.insertUsersBulk.bind(Environment.db)
const getUser = Environment.db.getUser.bind(Environment.db)
const getUsers = Environment.db.getUsers.bind(Environment.db)
const authenticateWith = Environment.authentication.authenticateWith.bind(Environment.db)
const hashToArray = Utils.hashToArray

describe('api/v2/users/ #matching', () => {

  before((done) => {
    Environment.setup()
      .then(() => done())
      .catch((err) => done(new Error(e.message)))
  })

  after((done) => {
    Environment.tearDown()
      .then(() => done())
      .catch((err) => done(new Error(e.message)))
  })

  describe('DELETE /profilesMatched', () => {
    let notificationsStub
    const users = [
      { _id: '53cb6b9b4f4ddef1ad47f941', 
        gender: GendersEnum.MALE, 
        seqNumber: 1, 
        facebookId: 10,
        profilesMatched: [
          { _id: '53cb6b9b4f4ddef1ad47f942', }
        ],
        reviewedUpTo: 24 
      },
      { 
        _id: '53cb6b9b4f4ddef1ad47f942', 
        gender: GendersEnum.FEMALE, 
        seqNumber: 2, 
        facebookId: 20,
        profilesMatched: [
          { _id: '53cb6b9b4f4ddef1ad47f941', }
        ],
        reviewedUpTo: config.matchRules.MAX_PROFILES_TO_REVIEW * 2 + 10
      },
    ]

    beforeEach((done) => {
      notificationsStub = sinon.stub(NotificationsUseCase, 'sendUnmatchedNotification', () => {})
      resetDatabase()
        .then(() => insertUsers(users))
        .then(() => done())
        .catch((e) => {
          console.error('Before()', e)
          done(new Error(e.message))
        })
    })

    afterEach((done) => {
      notificationsStub.restore()
      done()
    })

    it('should return 404 error if with parameter is not specified', (done) => {
      authenticateWith({ id: users[0].facebookId, gender: 'male', })
      const userId = users[0]._id
      const userUnmatched = users[1]

      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesMatched/`)
        .end((err, res) => {
          expect(res.status).to.be.equal(404)
          expect(notificationsStub.callCount).to.be.equal(0)
          done()
        })
    })

    it('should unmatch provided correct ids are supplied', (done) => {
      authenticateWith({ id: users[0].facebookId, gender: 'male', })
      const userId = users[0]._id
      const userUnmatched = users[1]

      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesMatched/${userUnmatched._id}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200)
          expect(notificationsStub.callCount).to.be.equal(1)
          getUsers()
            .then((dbUsers) => {
              expect(dbUsers.length).to.be.equal(2)
              expect(dbUsers[0].profilesMatched.length).to.be.equal(0)
              expect(dbUsers[1].profilesMatched.length).to.be.equal(0)
              done()
            })
            .catch((err) => {
              console.error('Error##', err)
              done(new Error(err.message))
            })
        })
    })

    it('should not unmatch if incorrect id is supplied', (done) => {
      authenticateWith({ id: users[0].facebookId, gender: 'male', })
      const userId = users[0]._id

      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesMatched/53cb6b9b4f4ddef1ad47f943`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          expect(res.body.error.message).to.be.equal('Could not find users')
          expect(notificationsStub.callCount).to.be.equal(0)
          getUsers()
            .then((dbUsers) => {
              expect(dbUsers.length).to.be.equal(2)
              expect(dbUsers[0].profilesMatched.length).to.be.equal(1)
              expect(dbUsers[1].profilesMatched.length).to.be.equal(1)
              done()
            })
            .catch((err) => {
              console.error('Error##', err)
              done(new Error(err.message))
            })
        })
    })
  })

  describe('GET /profilesToReview', () => {
    const users = [
      { _id: '53cb6b9b4f4ddef1ad47f941', 
        gender: GendersEnum.MALE, 
        seqNumber: 1, 
        facebookId: 10,
        profilesToReview: { 
          0: [
            { count: 10 }
          ]
        }, 
        reviewedUpTo: 24 
      },
      { 
        _id: '53cb6b9b4f4ddef1ad47f942', 
        gender: GendersEnum.MALE, 
        seqNumber: 2, 
        facebookId: 20,
        profilesToReview: {
          0: [
            { count: 10 }
          ]
        }, 
        reviewedUpTo: config.matchRules.MAX_PROFILES_TO_REVIEW * 2 + 10
      },
      { _id: '53cb6b9b4f4ddef1ad47f943',
        gender: GendersEnum.MALE,
        seqNumber: 3,
        facebookId: 30,
        profilesToReview: {
          0: [
            { count: config.matchRules.MAX_PROFILES_TO_REVIEW }
          ]
        },
        reviewedUpTo: 0 
      },
    ]

    const batchUsers = [
      { gender: GendersEnum.FEMALE, count: config.matchRules.MAX_PROFILES_TO_REVIEW * 2 }
    ]

    before((done) => {
      resetDatabase()
        .then(() => insertUsers(users))
        .then(() => insertUsersBulk(batchUsers))
        .then(() => done())
        .catch((e) => {
          console.error('Before()', e)
          done(new Error(e.message))
        })
    })

    it('should respond with the next batch of profilesToReview queues', (done) => {

      authenticateWith({ id: users[0].facebookId, gender: 'male', })
      const userId = users[0]._id
      const originalReviewedUpTo = users[0].reviewedUpTo

      chai.request(Environment.server)
        .get(`/api/v2/users/profilesToReview`)
        .end((err, res) => {
          const profilesToReview = hashToArray(_.get(res, 'body.data', undefined))
          const totalProfilesToReview = profilesToReview.length

          expect(res.statusCode).to.be.equal(200)
          expect(profilesToReview).to.exist
          expect(totalProfilesToReview).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)

          getUser({ _id: userId })
            .then((user) => {
              const totalProfilesToReviewInDb = hashToArray(user.profilesToReview).length
              expect(totalProfilesToReviewInDb).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)
              expect(user.reviewedUpTo).to.be.equal(users[2].seqNumber + config.matchRules.MAX_PROFILES_TO_REVIEW + 1)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it(`if there are no next profiles in the db,
        should respond with the very first batch of profiles
        and go in a cycle from the beginning`, 
      (done) => {

      authenticateWith({ id: users[1].facebookId, gender: 'male', })
      const userId = users[1]._id
      const originalCount = 10

      chai.request(Environment.server)
        .get(`/api/v2/users/profilesToReview`)
        .end((err, res) => {
          const profilesToReview = hashToArray(_.get(res, 'body.data', undefined))
          const totalProfilesToReview = profilesToReview.length

          expect(res.statusCode).to.be.equal(200)
          expect(profilesToReview).to.exist
          expect(totalProfilesToReview).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)

          getUser({ _id: userId })
            .then((user) => {
              const totalProfilesToReviewInDb = hashToArray(user.profilesToReview).length
              expect(totalProfilesToReviewInDb).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)
              expect(user.reviewedUpTo).to.be.equal(users[2].seqNumber + config.matchRules.MAX_PROFILES_TO_REVIEW + 1)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it(`if the current user has not reviewed anybody (reviewedUpTo = 0)
        should respond with the initial batch of profilesToReview counted from the 
        earliest eligible User by seqNumber`, 
      (done) => {

      authenticateWith({
        id: users[2].facebookId,
        gender: 'male',
      })
      const userId = users[2]._id
      chai.request(Environment.server)
        .get(`/api/v2/users/profilesToReview`)
        .end((err, res) => {
          const profilesToReview = hashToArray(_.get(res, 'body.data', undefined))

          expect(res.statusCode).to.be.equal(200)
          expect(profilesToReview).to.exist
          expect(profilesToReview.length).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)

          getUser({ _id: userId })
            .then((user) => {
              let totalProfilesToReviewInDb = hashToArray(user.profilesToReview).length
              expect(user.reviewedUpTo).to.be.equal(users[2].seqNumber + config.matchRules.MAX_PROFILES_TO_REVIEW + 1)
              expect(totalProfilesToReviewInDb).to.be.equal(config.matchRules.MAX_PROFILES_TO_REVIEW)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 401 if the specified userId is wrong', (done) => {

      authenticateWith({
        id: '123456',
        gender: 'male',
      })
      chai.request(Environment.server)
        .get(`/api/v2/users/profilesToReview`)
        .end((err, res) => {
          const responseData = _.get(res, 'body.data', undefined)
          const error = _.get(res, 'body.error', undefined)
          expect(res.statusCode).to.be.equal(401)
          expect(responseData).to.not.exist
          expect(error.message).to.be.equal('Invalid user requested')
          done()
        })
    })
  })

  // controller.likeProfile
  describe('POST /:userId/profilesLiked', () => {
    let notificationsStub
    const users = [
      { 
        _id: '53cb6b9b4f4ddef1ad47f940',
        facebookId: 10,
        gender: GendersEnum.MALE,
        seqNumber: 1,
        profilesToReview: {
          0: [
            { _id: '53cb6b9b4f4ddef1ad47f941' },
            { _id: '53cb6b9b4f4ddef1ad47f942' },
            { _id: '53cb6b9b4f4ddef1ad47f943' },
            { _id: '53cb6b9b4f4ddef1ad47f944' },
          ]
        },
        profilesLiked: [],
        profilesMatched: [],
      },
      { // Will match
        _id: '53cb6b9b4f4ddef1ad47f941',
        facebookId: 20,
        gender: GendersEnum.MALE,
        seqNumber: 2,
        profilesToReview: {},
        profilesLiked: [{
          _id: '53cb6b9b4f4ddef1ad47f940'
        }],
        profilesMatched: [],
      },
      { // Will not match
        _id: '53cb6b9b4f4ddef1ad47f942',
        facebookId: 30,
        gender: GendersEnum.MALE,
        seqNumber: 3,
        profilesToReview: {},
        profilesLiked: [],
        profilesMatched: [],
      }
    ]

    beforeEach((done) => {
      notificationsStub = sinon.stub(NotificationsUseCase, 'sendMatchedNotification', () => {})
      resetDatabase()
        .then(() => insertUsers(users))
        .then(() => done())
        .catch((e) => {
          console.error('DELETE /:userId/profilesToReview before() Error:', e)
          done(new Error(e.message))
        })
    })

    afterEach((done) => {
      notificationsStub.restore()
      done()
    })

    it('should put the new user to profilesLiked', (done) => {
      authenticateWith({
        id: users[0].facebookId,
        gender: 'male',
      })
      const user = users[0]
      const profileIdToLike = user.profilesToReview[0][1]._id
      chai.request(Environment.server)
        .post(`/api/v2/users/profilesLiked`)
        .send({ idToLike: profileIdToLike })
        .end((err, res) => {
          const result = _.get(res, 'body.data', undefined)
          const profileLiked = _.find(result.user.profilesLiked, (profile) => profile._id === profileIdToLike)
          const profileMatched = _.find(result.user.profilesMatched, (profile) => profile._id === profileIdToLike)

          expect(res.statusCode).to.be.equal(200)
          
          expect(result.liked).to.be.equal(true)
          expect(result.matched).to.be.equal(false)

          expect(result.user).to.exist
          expect(profileLiked).to.exist
          expect(profileMatched).to.not.exist
          expect(notificationsStub.callCount).to.be.equal(0)
          done()
        })
    })

    it('should put the new user to profilesMatched, if they matched', (done) => {
      authenticateWith({
        id: users[0].facebookId,
        gender: 'male',
      })
      const user = users[0]
      const profileIdToLike = user.profilesToReview[0][0]._id
      chai.request(Environment.server)
        .post(`/api/v2/users/profilesLiked`)
        .send({ idToLike: profileIdToLike })
        .end((err, res) => {
          const result = _.get(res, 'body.data', undefined)
          const profileLiked = _.find(result.user.profilesLiked, (profile) => profile._id === profileIdToLike)
          const profileMatched = _.find(result.user.profilesMatched, (profile) => profile._id === profileIdToLike)

          expect(res.statusCode).to.be.equal(200)
          expect(result.liked).to.be.equal(true)
          expect(result.matched).to.be.equal(true)
          expect(result.user).to.exist
          expect(profileLiked).to.not.exist
          expect(profileMatched).to.exist
          expect(notificationsStub.callCount).to.be.equal(1)
          done()
        })
    })

    it('should respond with 400 if the specified userId does not exist', (done) => {
      authenticateWith({
        id: '123123213213',
        gender: 'male',
      })
      const profileIdToLike = '53cb6b9b4f4ddef1ad47f998'
      chai.request(Environment.server)
        .post(`/api/v2/users/profilesLiked`)
        .send({ idToLike: profileIdToLike })
        .end((err, res) => {
          const error = _.get(res, 'body.error', undefined)
          expect(res.statusCode).to.be.equal(400)
          expect(error).to.exist
          expect(error.message).to.be.equal('Could not pass the user')
          expect(notificationsStub.callCount).to.be.equal(0)
          done()
        })
    })

    it(`should still respond with 200 OK if the profile to review 
        does not exist in the db anymore`, (done) => {
      authenticateWith({
        id: users[0].facebookId,
        gender: 'male',
      })
      const user = users[0]
      const userId = user._id
      const profileIdToLike = user.profilesToReview[0][3]._id
      chai.request(Environment.server)
        .post(`/api/v2/users/profilesLiked`)
        .send({ idToLike: profileIdToLike })
        .end((err, res) => {
          const user = res.body.data.user
          const profileLiked = _.find(user.profilesLiked, (p) => p._id === profileIdToLike)

          expect(res.statusCode).to.be.equal(200)
          expect(user).to.exist
          expect(profileLiked).to.exist
          expect(notificationsStub.callCount).to.be.equal(0)
          done()
        })
    })

  })

  // controller.passProfile
  describe('DELETE /:userId/profilesToReview', () => {

    const users = [
      { 
        _id: '53cb6b9b4f4ddef1ad47f940',
        facebookId: 10,
        gender: GendersEnum.MALE,
        seqNumber: 1,
        profilesToReview: {
          0: [
            { _id: '53cb6b9b4f4ddef1ad47f941' },
            { _id: '53cb6b9b4f4ddef1ad47f942' },
            { _id: '53cb6b9b4f4ddef1ad47f943' },
          ]
        },
        profilesLiked: [],
        profilesMatched: [],
      }
    ]

    beforeEach((done) => {
      resetDatabase()
        .then(() => insertUsers(users))
        .then((users) => done())
        .catch((e) => {
          console.error('DELETE /:userId/profilesToReview before() Error:', e)
          done(new Error(e.message))
        })
    })

    it('should remove the user from profilesToReview', (done) => {
      const userId = users[0]._id
      const idToPass = users[0].profilesToReview[0][0]._id
      authenticateWith({ id: users[0].facebookId, gender: 'male', })
      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesToReview/${idToPass}`)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(200)
          const result = _.get(res, 'body.data', undefined)
          const profilesToReview = hashToArray(result.user.profilesToReview)
          const profileToReview = _.find(profilesToReview, (profile) => profile._id === idToPass)
          const profileLiked = _.find(result.user.profilesLiked, (profile) => profile._id === idToPass)
          const profileMatched = _.find(result.user.profilesMatched, (profile) => profile._id === idToPass)

          expect(result.user).to.exist
          expect(result.passed).to.be.equal(true)
          expect(profileToReview).to.not.exist
          expect(profileLiked).to.not.exist
          expect(profileMatched).to.not.exist
          done()
        })
    })

    it('should respond with 401 if the specified user facebook Id does not exist', (done) => {
      const userFbId = '12312312'
      const idToPass = '53cb6b9b4f4ddef1ad47f932'

      authenticateWith({ id: userFbId, gender: 'male', })
      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesToReview/${idToPass}`)
        .end((err, res) => {
          const error = _.get(res, 'body.error', undefined)

          expect(res.statusCode).to.be.equal(401)
          expect(error.message).to.be.equal('Could not update the user')
          done()
        })
    })

    it('should respond with 200 even if the requested idToPass does not exist', (done) => {
      const userFbId = users[0].facebookId
      const idToPass = '53cb6b9b4f4ddef1ad47f933'

      authenticateWith({ id: userFbId, gender: 'male', })
      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesToReview/${idToPass}`)
        .end((err, res) => {
          const result = _.get(res, 'body.data', undefined)

          expect(res.statusCode).to.be.equal(200)
          expect(result.user).to.exist
          expect(result.passed).to.be.equal(true)
          done()
        })
    })

    it('should respond with 400 if user fb id is invalid', (done) => {
      const userId = 'wawawa'
      const idToPass = '53cb6b9b4f4ddef1ad47f933'

      authenticateWith({ id: userId, gender: 'male', })
      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesToReview/${idToPass}`)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })

    it('should respond with 400 if idToPass is invalid', (done) => {
      const userId = '53cb6b9b4f4ddef1ad47f933'
      const idToPass = '432432423432'
      chai.request(Environment.server)
        .delete(`/api/v2/users/profilesToReview/${idToPass}`)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })
  })
})