'use strict'
const _ = require('lodash')
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect
const sinon = require('sinon')
const setup = require('./setup')
const config = rootRequire('config')
const WebServiceApp = rootRequire('apps/webserviceapp')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const UserReviewProfilesUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase
const UserLoginUseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase

//https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

chai.use(chaiHttp)

describe('api/v2/users/ #auth', () => {
  let server
  let appWrapper

  before(function(done) {
    this.timeout(5000)
    setup.mockDependencies()
      .then(() => {
        appWrapper = new WebServiceApp(
          setup.getDatabaseTestUrl(),
          setup.getDatabaseTestConfiguration()
        )
        return appWrapper.start()
      })
      .then((app) => {
        server = app
      })
      .then(() => setup.resetDatabase())
      .then(() => {
        done()
      })
      .catch((e) => {
        done(new Error(e.message))
      })
  })

  after((done) => {
    setup.restoreDependencies()
      .then(() => { return appWrapper.stop() })
      .then(() => {
        done()
      })
      .catch((e) => {
        done(new Error('Could not restore dependencies'))
      })
  })

  beforeEach((done) => {
    setup.resetDatabase()
      .then(() => {
        done()
      })
      .catch((e) => {
        done(new Error(e.message))
      })
  })

  describe('GET /:userId', () => {

    const userIds = [
      { _id: '53cb6b9b4f4ddef1ad47f941', seqNumber: 1, facebookId: 10, gender: GendersEnum.MALE, isDeleted: true, },
      { _id: '53cb6b9b4f4ddef1ad47f942', seqNumber: 2, facebookId: 20, gender: GendersEnum.MALE, isDeleted: true, },
      { _id: '53cb6b9b4f4ddef1ad47f943', seqNumber: 3, facebookId: 30, gender: GendersEnum.FEMALE, isDeleted: true, },
      { _id: '53cb6b9b4f4ddef1ad47f944', seqNumber: 4, facebookId: 40, gender: GendersEnum.FEMALE, isDeleted: false, },
      { _id: '53cb6b9b4f4ddef1ad47f945', seqNumber: 5, facebookId: 50, gender: GendersEnum.FEMALE, isDeleted: false, },
      { _id: '53cb6b9b4f4ddef1ad47f946', seqNumber: 6, facebookId: 60, gender: GendersEnum.FEMALE, isDeleted: true, },
    ]

    beforeEach((done) => {
      setup.resetDatabase()
        .then(() => {
          return setup.addUsers(userIds)
        })
        .then(() => {
          done()
        })
        .catch((e) => {
          done(new Error(e.message))
        })
    })

    it('should respond with 200 and a logged in user object, modifying isDeleted to false', (done) => {
      const user = userIds[0]
      setup.passport.willReturn({
        id: user.facebookId,
      })
      chai.request(server)
        .get(`/api/v2/users/`)
        .end((err, res) => {
          const user = _.get(res, 'body.data', null)
          expect(res.statusCode).to.be.equal(200)
          expect(user).to.exist
          expect(user.facebookId).to.not.exist
          expect(user.seqNumber).to.not.exist
          // TODO: To fix profilesToReview test
          expect(user.profilesToReview[0].length).to.be.equal(0)
          expect(user.profilesToReview[1].length).to.be.equal(0)
          expect(user.profilesToReview[2].length).to.be.equal(0)
          expect(user.profilesToReview[3].length).to.be.equal(0)
          expect(user.profilesToReview[4].length).to.be.equal(0)
          expect(user.profilesToReview[5].length).to.be.equal(0)
          expect(user.profilesToReview[6].length).to.be.equal(2)
          expect(user._id).to.be.equal(user._id)
          setup.getUser({ _id: user._id })
            .then((u) => {
              expect(u.isDeleted).to.be.equal(false)
              done()
            })
            .catch((e) => {
              console.error('Error in login', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 401 if invalid userId is supplied', (done) => {

      setup.passport.willReturn({
        id: '12312321312321',
      })
      chai.request(server)
        .get(`/api/v2/users/`)
        .end((err, res) => {
          const user = _.get(res, 'body.data', null)
          expect(res.statusCode).to.be.equal(401)
          expect(res.body.error.message).to.be.equal('No user found')
          expect(user).to.not.exist
          done()
        })
    })
  })

  describe('POST /', () => {

    const userIds = [
      { _id: '53cb6b9b4f4ddef1ad47f941', seqNumber: 10, facebookId: 10, gender: GendersEnum.MALE, },
      { _id: '53cb6b9b4f4ddef1ad47f942', seqNumber: 11, facebookId: 11, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f943', seqNumber: 12, facebookId: 12, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f944', seqNumber: 13, facebookId: 13, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f945', seqNumber: 14, facebookId: 14, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f946', seqNumber: 15, facebookId: 15, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f947', seqNumber: 16, facebookId: 16, gender: GendersEnum.FEMALE, },
      { _id: '53cb6b9b4f4ddef1ad47f948', seqNumber: 17, facebookId: 17, gender: GendersEnum.FEMALE, },
    ]

    beforeEach((done) => {
      setup.resetDatabase()
        .then(() => {
          return setup.addUsers(userIds)
        })
        .then(() => {
          done()
        })
        .catch((e) => {
          done(new Error(e.message))
        })
    })

    it(`if an existing facebook id is provided, it 
        should respond with 400 and not create any new users`, (done) => {
      const facebookId = userIds[0].facebookId
      setup.passport.willReturn({
        id: facebookId,
        gender: 'male',
      })

      chai.request(server)
        .post(`/api/v2/users`)
        .send({
          accessToken: '1234567890',
          fcmToken: '1234567890',
        })
        .end((err, res) => {
          const qbMock = setup.getQbMock()
          const qbRegistrationCallCount = _.get(qbMock, 'usersCreateStub.callCount', undefined)
          const qbLoginCallCount = _.get(qbMock, 'usersLoginStub.callCount', undefined)
          expect(qbRegistrationCallCount).to.be.equal(0)
          expect(qbLoginCallCount).to.be.equal(0)
          expect(res.statusCode).to.be.equal(400)
          const user = res.body.data
          expect(user).to.not.exist
          expect(res.body.error.message).to.be.equal('Failed to create a new user')
          setup.getUsers()
            .then((users) => {
              expect(users.length).to.be.equal(userIds.length)
              done()
            })
            .catch((e) => {
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 201 and create a new user', (done) => {
      const facebookId = '12345678'
      setup.passport.willReturn({
        id: facebookId,
        gender: 'male',
      })

      chai.request(server)
        .post(`/api/v2/users`)
        .send({
          accessToken: '1234567890',
          fcmToken: '1234567890',
        })
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(201)
          const user = res.body.data
          expect(user).to.exist
          expect(user.profilesToReview).to.exist
          // TODO: To fix profilesToReview test
          expect(user.profilesToReview[0].length).to.be.equal(0)
          expect(user.profilesToReview[1].length).to.be.equal(0)
          expect(user.profilesToReview[2].length).to.be.equal(0)
          expect(user.profilesToReview[3].length).to.be.equal(0)
          expect(user.profilesToReview[4].length).to.be.equal(0)
          expect(user.profilesToReview[5].length).to.be.equal(0)
          expect(user.profilesToReview[6].length).to.be.equal(7)
          expect(user.facebookId).to.not.exist
          expect(user.gender).to.be.equal(GendersEnum.MALE)
          setup.getUsers()
            .then((users) => {
              expect(users.length).to.be.equal(9)
              const foundUser = _.find(users, (u) => u.facebookId === parseInt(facebookId))
              expect(foundUser).to.exist
              expect(foundUser.gender).to.be.equal(GendersEnum.MALE)
              done()
            })
            .catch((e) => {
              done(new Error(e.message))
            })
        })
    })

  })

  describe('PATCH /profile/qbchat', () => {

    const userIds = [
      {
        _id: '53cb6b9b4f4ddef1ad47f941',
        seqNumber: 1,
        facebookId: 10,
        gender: GendersEnum.MALE,
        isDeleted: false,
        qbchat: {
          login: '1234',
          password: '123456'
        }
      }
    ]

    beforeEach((done) => {
      setup.passport.willReturn({
        id: userIds[0].facebookId,
      })
      setup.resetDatabase()
        .then(() => {
          return setup.addUsers(userIds)
        })
        .then(() => {
          setup.quickblox.usersLoginStub.reset()
          setup.quickblox.usersCreateStub.reset()
          done()
        })
        .catch((e) => {
          done(new Error(e.message))
        })
    })

    it('should not change qbchat if the login succeeds', (done) => {
      setup.quickblox.willReturn({
        loginError: null,
      })
      chai.request(server)
        .patch(`/api/v2/users/profile/qbchat`)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(304)
          expect(setup.quickblox.usersLoginStub.callCount).to.be.equal(1)
          expect(setup.quickblox.usersCreateStub.callCount).to.be.equal(0)
          setup.getUser({ facebookId: userIds[0].facebookId, })
            .then((u) => {
              expect(u.qbchat).to.exist
              expect(u.qbchat.login).to.be.equal(userIds[0].qbchat.login)
              expect(u.qbchat.password).to.be.equal(userIds[0].qbchat.password)
              done()
            })
            .catch((e) => {
              console.error('Could not get a user', e)
              done(new Error(e.message))
            })
        })
    })

    it(`should respond with 400 if login fails, 
        and new registration fails for some reason
        and the qbchat should not change
       `, (done) => {
      setup.quickblox.willReturn({
        login: {
          error: new Error('Simulating qb login error'),
        },
        register: {
          error: new Error('Simulating registration failure'),
        }
      })
      chai.request(server)
        .patch(`/api/v2/users/profile/qbchat`)
        .end((err, res) => {
          expect(setup.quickblox.usersLoginStub.callCount).to.be.equal(1)
          expect(setup.quickblox.usersCreateStub.callCount).to.be.equal(1)
          expect(res.statusCode).to.be.equal(400)
          expect(res.body.error.message).to.be.equal('Could not verify user chat credentials')
          setup.getUser({ facebookId: userIds[0].facebookId, })
            .then((u) => {
              expect(u.qbchat).to.exist
              expect(u.qbchat.login).to.be.equal(userIds[0].qbchat.login)
              expect(u.qbchat.password).to.be.equal(userIds[0].qbchat.password)
              done()
            })
            .catch((e) => {
              console.error('Could not get a user', e)
              done(new Error(e.message))
            })
        })
    })

    it(`should respond with 200 if login fails,
        and new registration succeeds.
        The qbchat should change, too`, (done) => {
      setup.quickblox.willReturn({
        login: {
          error: new Error('Simulating qb login error'),
        },
      })
      chai.request(server)
        .patch(`/api/v2/users/profile/qbchat`)
        .end((err, res) => {
          expect(setup.quickblox.usersLoginStub.callCount).to.be.equal(1)
          expect(setup.quickblox.usersCreateStub.callCount).to.be.equal(1)
          expect(res.statusCode).to.be.equal(200)
          done()
        })
    })    
  })
})