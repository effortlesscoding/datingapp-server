'use strict'
const _ = require('lodash')
const expect = require('chai').expect
const sinon = require('sinon')
const chai = require('chai')
const User = rootRequire('apps/webserviceapp/models/User')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const Environment = rootRequire('specs/setuphelpers/Environment')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}
const users = [
  { _id: '53cb6b9b4f4ddef1ad47f941', facebookId: 1, }
]

describe('api/v2/users/notifications', () => {
  // TODO: Use this setup EVERYWHERE.
  const resetDatabase = Environment.db.resetDatabase.bind(Environment.db)
  const insertUsers = Environment.db.insertUsers.bind(Environment.db)
  const authenticateWith = Environment.authentication.authenticateWith.bind(Environment.db)

  before((done) => {
    Environment.setup()
      .then(() => done())
      .catch((err) => done(new Error(e.message)))
  })

  after((done) => {
    Environment.tearDown()
      .then(() => done())
      .catch((err) => done(new Error(e.message)))
  })
  
  describe('PUT', (done) => {

    beforeEach((done) => {
      resetDatabase()
        .then(() => insertUsers(users))
        .then(() => done())
        .catch((e) => {
          console.error('Before All##', e)
          done(new Error(e.message))
        })
    })

    it('should update a user\'s notifications token', (done) => {
      authenticateWith({ id: users[0].facebookId, })
      const TOKEN = '123456'
      chai.request(Environment.server)
        .put(`/api/v2/users/notifications`)
        .send({ token: TOKEN, })
        .end((err, res) => {
          expect(res.status).to.be.equal(200)
          expect(res.body.data).to.be.equal('Updated')
          User.findOne({ _id: users[0]._id}).exec()
            .then((user) => {
              const tok = _.get(user, 'fcm.token', undefined)
              expect(tok).to.be.equal(TOKEN)
              done()
            })
            .catch((err) => {
              console.error('Error###', err)
              done(err)
            })
        })
    })

    it('should return 400 if invalid user is provided', (done) => {
      authenticateWith({ id: 123, })
      const TOKEN = '123456'
      chai.request(Environment.server)
        .put(`/api/v2/users/notifications`)
        .send({ token: TOKEN, })
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          expect(res.body.error.message).to.be.equal('User could not be updated')
          User.findOne({ _id: users[0]._id}).exec()
            .then((user) => {
              const tok = _.get(user, 'fcm.token', undefined)
              expect(tok).to.be.undefined
              done()
            })
            .catch((err) => {
              console.error('Error###', err)
              done(err)
            })
        })
    })

    it('should respond with 400 if token is not provided', (done) => {
      authenticateWith({ id: '1', })
      chai.request(Environment.server)
        .put(`/api/v2/users/notifications`)
        .end((err, res) => {
          expect(res.status).to.be.equal(400)
          expect(res.body.error.message.token.msg).to.be.equal('A notifications token has to be provided')
          User.findOne({ _id: users[0]._id}).exec()
            .then((user) => {
              const tok = _.get(user, 'fcm.token', undefined)
              expect(tok).to.be.undefined
              done()
            })
            .catch((err) => {
              console.error('Error###', err)
              done(err)
            })
        })
    })

  })
})