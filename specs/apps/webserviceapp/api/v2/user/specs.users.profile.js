'use strict'
const _ = require('lodash')
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect
const sinon = require('sinon')
const setup = require('./setup.users.profile')

const config = rootRequire('config')
const WebServiceApp = rootRequire('apps/webserviceapp')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const UserModel = rootRequire('apps/webserviceapp/models/User')
const UserReviewProfilesUseCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles').UseCase
const UserLoginUseCase = rootRequire('apps/webserviceapp/usecases/user/login').UseCase

//https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

chai.use(chaiHttp)

describe('api/v2/users/', () => {
  let server, appWrapper

  before(function(done) {
    this.timeout(5000)
    setup.mockDependencies()
      .then(() => {
        appWrapper = new WebServiceApp(
          setup.getDatabaseTestUrl(),
          setup.getDatabaseTestConfiguration()
        )
        return appWrapper.start()
      })
      .then((app) => {
        server = app
      })
      .then(() => setup.resetDatabase())
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('before() failed', e)
        done(new Error(e.message))
      })
  })

  after((done) => {
    setup.restoreDependencies()
      .then(() => { return appWrapper.stop() })
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('ERROR ###', e)
        done(new Error('Could not restore dependencies'))
      })
  })
    
  const users = [
    { _id: '53cb6b9b4f4ddef1ad47f941', facebookId: 10, gender: GendersEnum.MALE, videos: [{ _id: 1, review: { approved: true, }}, { _id: 2}] },
    { _id: '53cb6b9b4f4ddef1ad47f942', facebookId: 11, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f943', facebookId: 12, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f944', facebookId: 13, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f945', facebookId: 14, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f946', facebookId: 15, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f947', facebookId: 16, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f948', facebookId: 17, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f949', facebookId: 18, gender: GendersEnum.FEMALE },
    { _id: '53cb6b9b4f4ddef1ad47f950', facebookId: 19, gender: GendersEnum.FEMALE },
  ]
  
  beforeEach((done) => {
    setup.resetDatabase()
      .then(() => {
        return setup.addUsers(users)
      })
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('Error', e)
        done(new Error(e.message))
      })
  })

  // controller.getUsersDetails
  describe('GET /', () => {

    it('should respond with details of only those requested users that exist in the db', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const profilesRequestBody = [
        { _id: '53cb6b9b4f4ddef1ad47f941' },
        { _id: '53cb6b9b4f4ddef1ad47f942' },
        { _id: '53cb6b9b4f4ddef1ad47f943' },
        { _id: '53cb6b9b4f4ddef1ad47f944' },
        { _id: '53cb6b9b4f4ddef1ad47f945' },
        { _id: '53cb6b9b4f4ddef1ad47f946' },
        { _id: '53cb6b9b4f4ddef1ad47f947' },
        { _id: '53cb6b9b4f4ddef1ad47f948' },
        { _id: '53FAIL9b4f4ddef1ad47f100' }, // Does not exist
        { _id: '53FAIL9b4f4ddef1ad47f101' }, // Does not exist
        { _id: '53FAIL9b4f4ddef1ad47f102' }, // Does not exist
      ]
      // MAXIMUM LENGTH OF A GET REQUEST ??
      // TODO: Transform to using X-HTTP-Method-Override (https://github.com/expressjs/method-override)
      // But for now... just use the fucking post method.
      chai.request(server)
        .post('/api/v2/users/profiles')
        .send({ ids: profilesRequestBody })
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(200)
          expect(err).to.be.null
          const profiles = _.get(res, 'body.data', undefined)
          expect(profiles).to.exist
          expect(profiles.length).to.be.equal(profilesRequestBody.length - 3)
          const originalIds = _.map(profilesRequestBody, (profile) => profile._id)
          const requestIds = _.map(profiles, (profile) => profile._id)
          const difference = _.xor(originalIds, requestIds)
          expect(difference.length).to.be.equal(3)
          done()
        })
    })

    it('should respond with details of all requested users if they all exist in the db', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })

      const profilesRequestBody = [
        { _id: '53cb6b9b4f4ddef1ad47f941' },
        { _id: '53cb6b9b4f4ddef1ad47f942' },
        { _id: '53cb6b9b4f4ddef1ad47f943' },
        { _id: '53cb6b9b4f4ddef1ad47f944' },
        { _id: '53cb6b9b4f4ddef1ad47f945' },
        { _id: '53cb6b9b4f4ddef1ad47f946' },
        { _id: '53cb6b9b4f4ddef1ad47f947' },
        { _id: '53cb6b9b4f4ddef1ad47f948' },
        { _id: '53cb6b9b4f4ddef1ad47f949' },
      ]
      // MAXIMUM LENGTH OF A GET REQUEST ??
      // TODO: Transform to using X-HTTP-Method-Override (https://github.com/expressjs/method-override)
      // But for now... just use the fucking post method.
      chai.request(server)
        .post('/api/v2/users/profiles')
        .send({ ids: profilesRequestBody })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.statusCode).to.be.equal(200)
          const profiles = _.get(res, 'body.data', undefined)
          expect(profiles).to.exist
          expect(profiles.length).to.be.equal(profilesRequestBody.length)
          const originalIds = _.map(profilesRequestBody, (profile) => profile._id)
          const requestIds = _.map(profiles, (profile) => profile._id)
          const difference = _.xor(originalIds, requestIds)
          expect(difference.length).to.be.equal(0)
          done()
        })
    })

    it('should respond with an empty array if no users requested are in the db', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const profilesRequestBody = [
        { _id: '53cb6b9b4f4ddef1ad47f999' },
        { _id: '53cb6b9b4f4ddef1ad47f000' },
        { _id: '53cb6b9b4f4ddef1ad47f001' },
        { _id: '53cb6b9b4f4ddef1ad47f002' },
        { _id: '53cb6b9b4f4ddef1ad47f003' },
        { _id: '53cb6b9b4f4ddef1ad47f004' },
        { _id: '53cb6b9b4f4ddef1ad47f005' },
        { _id: '53cb6b9b4f4ddef1ad47f006' },
        { _id: '53cb6b9b4f4ddef1ad47f007' },
      ]
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .post('/api/v2/users/profiles')
        .send({ ids: profilesRequestBody })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.statusCode).to.be.equal(200)
          const profiles = _.get(res, 'body.data', undefined)
          expect(profiles).to.exist
          expect(profiles.length).to.be.equal(0)
          done()
        })
    })
  })

  // controller.deleteUser
  describe('DELETE /', () => {

    it('should mark a user as deleted', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .delete('/api/v2/users/')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId}).exec()
            .then((u) => {
              expect(u.isDeleted).to.be.true
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

  })

  // controller.flagUser
  describe('POST /:userId/complaints', () => {

    it('should flag a complaint against a user', (done) => {
      const userFbId = users[0].facebookId
      const flaggedUser = users[1]
      const flaggedUserId = flaggedUser._id
      const reason = 'Lalalala'
      setup.passport.willReturn({
        id: userFbId,
      })
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .post(`/api/v2/users/${flaggedUserId}/complaints`)
        .send({ reason: reason })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ _id: flaggedUserId }).exec()
            .then((u) => {
              expect(u.complaints.length).to.be.equal(1)
              expect(u.complaints[0].reason).to.be.equal(reason)
              expect(u.complaints[0].reporter.facebookId).to.be.equal(flaggedUser.facebookId)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 400 if the user with userId does not exist in the db', (done) => {
      const userFbId = users[0].facebookId
      const flaggedUserId = '53cb6b9b4f4ddef1ad47fADS'
      const reason = 'Lalalala'
      setup.passport.willReturn({
        id: userFbId,
      })
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .post(`/api/v2/users/${flaggedUserId}/complaints`)
        .send({ reason: reason })
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          expect(res.body.error.message).to.be.equal('Could not flag the user')
          done()
        })
    })
  })

  // controller.updateUserProfile
  describe('PATCH /', () => {

    it('should update a user\'s location', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {
        location: {
          lat: -37,
          lng: 99,
        }
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/')
        .send(updateRequest)
        .end((err, res) => {
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          let updatedProfile = _.get(res, 'body.data.profile', undefined)
          expect(updatedProfile.location.lat).to.be.equal(updateRequest.location.lat)
          expect(updatedProfile.location.lng).to.be.equal(updateRequest.location.lng)

          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              expect(u.profile.location.lat).to.be.equal(updateRequest.location.lat)
              expect(u.profile.location.lng).to.be.equal(updateRequest.location.lng)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it(`should throw a 400 if none of updateable fields are provided`, (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {
        location: {
        },
        chair: 'red',
        table: 'green',
        something: 'else',
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/')
        .send(updateRequest)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })


    it(`should throw a 400 if no fields are provided`, (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {}
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/')
        .send(updateRequest)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })

    it(`should only update these fields:
        gender, interests, occupation, study, aboutMe, location.lat, location.lng, and age.
        Videos should be ignored in this request.`, (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const NEW_GENDER = GendersEnum.FEMALE
      const updateRequest = {
        gender: NEW_GENDER,
        firstName: 'Ali',
        lastName: 'baba',
        email: 'frank@gmail.com',
        videos: [
          { _id: 0, storagePath: 'somwhere', videoFile: 'video.mp4', thumbnailName: 'video.jpg'}
        ],
        age: 11,
        interests: ['Squash', 'Guitar'],
        occupation: 'Student',
        study: 'RMIT',
        aboutMe: 'A very outgoing person',
        location: {
          lat: -37,
          lng: 99,
        },
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/')
        .send(updateRequest)
        .end((err, res) => {
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          let updatedGender = _.get(res, 'body.data.gender', undefined)
          let updatedProfile = _.get(res, 'body.data.profile', undefined)
          expect(updatedGender).to.be.equal(NEW_GENDER)
          expect(updatedProfile.occupation).to.be.equal(updateRequest.occupation)
          expect(updatedProfile.study).to.be.equal(updateRequest.study)
          expect(updatedProfile.aboutMe).to.be.equal(updateRequest.aboutMe)
          const difference = _.xor(updatedProfile.interests, updateRequest.interests)
          expect(updatedProfile.location.lat).to.be.equal(updateRequest.location.lat)
          expect(updatedProfile.location.lng).to.be.equal(updateRequest.location.lng)
          expect(difference.length).to.be.equal(0)
          expect(updatedProfile.age).to.be.equal(updateRequest.age)
          expect(updatedProfile.firstName).to.exist
          expect(updatedProfile.lastName).to.exist
          expect(updatedProfile.firstName).to.not.be.equal(updateRequest.firstName)
          expect(updatedProfile.lastName).to.not.be.equal(updateRequest.lastName)
          expect(updatedProfile.email).to.not.exist
          expect(updatedProfile.email).to.not.be.equal(updateRequest.email)
          const videos = _.get(updatedProfile, 'videos', {})
          _.forEach(videos, (vid) => {
            expect(vid._id).to.not.be.equal(updateRequest.videos[0]._id)
          })
          expect(videos.length).to.be.equal(users[0].videos.length)

          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              expect(u.gender).to.be.equal(NEW_GENDER)
              expect(u.profile.occupation).to.be.equal(updateRequest.occupation)
              expect(u.profile.study).to.be.equal(updateRequest.study)
              expect(u.profile.age).to.be.equal(updateRequest.age)
              expect(u.profile.aboutMe).to.be.equal(updateRequest.aboutMe)
              expect(u.profile.location.lat).to.be.equal(updateRequest.location.lat)
              expect(u.profile.location.lng).to.be.equal(updateRequest.location.lng)
              const difference = _.xor(u.profile.interests, updateRequest.interests)
              expect(difference.length).to.be.equal(0)

              expect(u.profile.firstName).to.exist
              expect(u.profile.lastName).to.exist
              expect(u.profile.firstName).to.not.be.equal(updateRequest.firstName)
              expect(u.profile.lastName).to.not.be.equal(updateRequest.lastName)
              expect(u.profile.email).to.not.be.equal(updateRequest.email)
              let videosCount = 0
              _.forOwn(u.profile.videos.toObject({ versionKey: false, }), (vid, key) => {
                expect(vid._id).to.not.be.equal(updateRequest.videos[0]._id)
                videosCount++
              })
              expect(videosCount).to.be.equal(users[0].videos.length)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })
  })

  describe('PATCH /preferences', () => {

    it('should update preferences', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {
        gender: {
          male: true,
          female: true,
        },
        age: {
          min: 28,
          max: 38,
        },
        distance: 10, 
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/preferences')
        .send(updateRequest)
        .end((err, res) => {
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          let preferences = _.get(res, 'body.data', undefined)
          expect(preferences.gender.male).to.be.true
          expect(preferences.gender.female).to.be.true
          expect(preferences.age.min).to.be.equal(updateRequest.age.min)
          expect(preferences.age.max).to.be.equal(updateRequest.age.max)
          expect(preferences.distance).to.be.equal(updateRequest.distance)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              expect(u.preferences.gender.male).to.be.true
              expect(u.preferences.gender.female).to.be.true
              expect(u.preferences.age.min).to.be.equal(updateRequest.age.min)
              expect(u.preferences.age.max).to.be.equal(updateRequest.age.max)
              expect(u.preferences.distance).to.be.equal(updateRequest.distance)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 400 if no recognizable fields are supplied', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {
        whatever: 12,
        cool: 23
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/preferences')
        .send({ request: updateRequest })
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          expect(res.body.error.message).to.be.equal('Wrong update query')
          done()
        })
    })

    it('should respond with 400 if the update request is empty', (done) => {
      
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const updateRequest = {}
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .patch('/api/v2/users/preferences')
        .send({ request: updateRequest })
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })

  })

  // controller.deleteProfileVideo
  describe('DELETE /profile/videos/:videoId', () => {

    it('should remove a video from the user\'s profile', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      const videoId = 1
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .delete(`/api/v2/users/profiles/videos/${videoId}`)
        .end((err, res) => {
          // the video should be deleted ...
          console.error('Error##', err)
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              let videoExists = false
              _.forOwn(u.profile.videos, (vid, key) => {
                if (vid && vid._id === videoId) videoExists = true
              })
              expect(videoExists).to.be.false
              const videos = u.profile.videos.toObject({ versionKey: false })
              expect(Object.keys(videos).length).to.be.equal(users[0].videos.length - 1)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 404 if the requested video was not provided', (done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .delete(`/api/v2/users/profiles/videos/`)
        .end((err, res) => {
          expect(res.statusCode).to.be.equal(404)
          done()
        })
    })

  })

  // controller.updateProfileVideos (update? with upsert)
  describe('PUT /profile/videos', () => {
    beforeEach((done) => {
      setup.passport.willReturn({
        id: users[0].facebookId,
      })

      // Set up Users profile videos so that the video with id 1 has some kind of review
      done()
    })

    it('should add a video to a user\'s profile', (done) => {
      
      const videoId = 3
      const videoRequest = {
        request: [{
          _id: videoId,
          storagePath: 'somewhere',
          videoName: 'video.mp4',
          thumbnailName: 'video.png',
          order: 2, 
        }]
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .put(`/api/v2/users/profiles/videos`)
        .send(videoRequest)
        .end((err, res) => {
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              const videos = u.profile.videos.toObject({ versionKey: false })
              expect(Object.keys(videos).length).to.be.equal(3)
              let video = null
              _.forOwn(videos, (vid, key) => {
                if (vid._id === videoId) video = vid
              })
              expect(video).to.exist
              expect(video._id).to.be.equal(videoId)
              expect(Object.keys(video.review.toObject({ versionKey: false })).length).to.equal(0)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should respond with 400 if video ids are not specified', (done) => {
      
      const videoRequest = {
        request: [{
          storagePath: 'somewhere',
          videoName: 'video.mp4',
          thumbnailName: 'video.png',
          order: 2, 
        }],
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .put(`/api/v2/users/profiles/videos`)
        .send({ request: [videoRequest] })
        .end((err, res) => {
          // ...
          expect(res.statusCode).to.be.equal(400)
          done()
        })
    })

    it('should replace the existing video that already exists', (done) => {
      const videoId = 2
      const videoRequest = {
        request: [{
          _id: videoId,
          storagePath: 'somewhere',
          videoName: 'video.mp4',
          thumbnailName: 'video.png',
          order: 2,
        }],
      }
      chai.request(server)
        .put(`/api/v2/users/profiles/videos`)
        .send(videoRequest)
        .end((err, res) => {
          // ...
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              const videos = u.profile.videos.toObject({ versionKey: false })
              expect(Object.keys(videos).length).to.be.equal(2)
              // Original video should still have review
              const videoWithReview = _.find(videos, (vid) => vid._id === 1)
              expect(videoWithReview.review).to.exist
              expect(videoWithReview.review.approved).to.be.true

              const video = _.find(videos, (vid) => vid._id === videoId)
              const originalRequest = videoRequest.request[0]
              expect(video._id).to.be.equal(originalRequest._id)
              expect(video.storagePath).to.be.equal(originalRequest.storagePath)
              expect(video.videoName).to.be.equal(originalRequest.videoName)
              expect(video.thumbnailName).to.be.equal(originalRequest.thumbnailName)
              expect(Object.keys(video.review.toObject({ versionKey: false })).length).to.equal(0)
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should replace the existing video with review and set review to an empty', (done) => {
      const videoId = 1
      const videoRequest = {
        request: [{
          _id: videoId,
          storagePath: 'somewhere',
          videoName: 'video.mp4',
          thumbnailName: 'video.png',
          order: 2,
        }],
      }
      chai.request(server)
        .put(`/api/v2/users/profiles/videos`)
        .send(videoRequest)
        .end((err, res) => {
          // ...
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              const videos = u.profile.videos.toObject({ versionKey: false })
              expect(Object.keys(videos).length).to.be.equal(2)

              const video = _.find(videos, (vid) => vid._id === videoId)
              const originalRequest = videoRequest.request[0]
              expect(video._id).to.be.equal(originalRequest._id)
              expect(video.storagePath).to.be.equal(originalRequest.storagePath)
              expect(video.videoName).to.be.equal(originalRequest.videoName)
              expect(video.thumbnailName).to.be.equal(originalRequest.thumbnailName)
              _.forEach(videos, (vid) => {
                expect(Object.keys(vid.review.toObject({ versionKey: false })).length).to.equal(0)
              })
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

    it('should update multiple videos (ex. switch order)', (done) => {

      const videoRequest = {
        request: [
          { _id: 0,
            storagePath: 'somewhere',
            videoName: 'video.mp4',
            thumbnailName: 'video.png',
            order: 1, },
          { _id: 1,
            storagePath: 'somewhere',
            videoName: 'video.mp4',
            thumbnailName: 'video.png',
            order: 3, },
          { _id: 2,
            storagePath: 'somewhere',
            videoName: 'video.mp4',
            thumbnailName: 'video.png',
            order: 2, },
        ]
      }
      // MAXIMUM LENGTH OF A GET REQUEST ??
      chai.request(server)
        .put(`/api/v2/users/profiles/videos`)
        .send(videoRequest)
        .end((err, res) => {
          // ...
          expect(err).to.not.exist
          expect(res.statusCode).to.be.equal(200)
          UserModel.findOne({ facebookId: users[0].facebookId }).exec()
            .then((u) => {
              const videos = u.profile.videos.toObject({ versionKey: false })
              expect(Object.keys(videos).length).to.be.equal(3)
              expect(u.profile.videos[0].order).to.be.equal(1)
              expect(u.profile.videos[1].order).to.be.equal(3)
              expect(u.profile.videos[2].order).to.be.equal(2)
              _.forEach(videos, (video) => {
                expect(Object.keys(video.review.toObject({ versionKey: false })).length).to.equal(0)
              })
              done()
            })
            .catch((e) => {
              console.error('Error', e)
              done(new Error(e.message))
            })
        })
    })

  })

})

