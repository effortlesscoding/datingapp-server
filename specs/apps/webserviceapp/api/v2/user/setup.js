'use strict'

const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const UserModel = rootRequire('apps/webserviceapp/models/User')
const MockUser = rootRequire('specs/mocks/MockUser')
const DbSetup = rootRequire('specs/setuphelpers/DbSetup')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UserApiSetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 1
  }

  getUsers() {
    return UserModel.find({})
  }

  getUser(data) {
    if (data._id) {
      return UserModel.findOne({ _id: data._id }) 
    } else if (data.facebookId) {
      return UserModel.findOne({ facebookId: data.facebookId }) 
    }
  }

  addUsers(usersParams) {
    let promises = []
    usersParams.forEach((userParams) => {
      const user = new MockUser()
        .setUserId(userParams._id)
        .setUserSeqNumber(userParams.seqNumber)
        .setFacebookId(userParams.facebookId)
        .setIsDeleted(userParams.isDeleted || false)
      if (userParams.gender === GendersEnum.FEMALE) {
        user.setGenderFemale()
      } else {
        user.setGenderMale()
      }
      user.qbchat = userParams.qbchat
      this.usersCount++
      promises.push(new UserModel(user).save())
    })
    return Promise.all(promises)
  }
}

module.exports = new UserApiSetup()