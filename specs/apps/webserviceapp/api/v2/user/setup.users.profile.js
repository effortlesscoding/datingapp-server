'use strict'

const UserModel = rootRequire('apps/webserviceapp/models/User')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum')
const MockUser = rootRequire('specs/mocks/MockUser')
const DbSetup = rootRequire('specs/setuphelpers/DbSetup')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class UsersProfilesSetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 1
    this.profilesToReviewCounter = 10000
  }

  getUser(data) {
    return UserModel.findOne({ _id: data._id }).exec()
  }

  getUsers() {
    return UserModel.find({})
  }

  bulkAddUsers(bulkParams) {
    // gender
    return new Promise((resolve, reject) => {
      let usersToInsert = []
      bulkParams.forEach((params) => {
        for (let i = 0; i < params.count; i++) {
          const id = `1234567890123456789${this.profilesToReviewCounter++}`
          const user = new MockUser()
            .setUserId(id)
            .setUserSeqNumber(this.usersCount)
            .setFacebookId(this.usersCount * 10)
          if (params.gender === GendersEnum.MALE) {
            user.setGenderMale()
          } else {
            user.setGenderFemale()
          }
          this.usersCount++
          usersToInsert.push(user)
        }
      })
      UserModel.collection.insert(usersToInsert, {}, (err, docs) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }

  addUsers(usersParams) {
    let promises = []
    usersParams.forEach((userParams) => {
      const user = new MockUser()
        .setUserId(userParams._id)
        .setUserSeqNumber(userParams.seqNumber || this.usersCount)
        .setFacebookId(userParams.facebookId)
        .setReviewedUpTo(userParams.reviewedUpTo || 0)
      if (userParams.seqNumber > this.usersCount) {
        this.usersCount = userParams.seqNumber
      } else {
        this.usersCount++ 
      }
      if (userParams.videos) {
        userParams.videos.forEach((vid) => {
          const videoObject = { 
            _id: vid._id, 
            storagePath: 'wherever', 
            videoName: 'wh.mp4', 
            thumbnailName: 'wh.jpb'
          }
          if (vid.review) {
            videoObject.review = vid.review
          }
          user.addProfileVideo(videoObject)
        })
      }
      if (userParams.profilesToReview) {
        for (const priority in userParams.profilesToReview) {
          userParams.profilesToReview[priority].forEach((profile) => {
            if (profile._id) {
              user.addProfileToReview({
                priority: priority, 
                _id: profile._id,
              }) 
            } else if (profile.count) {
              for (let i = 0; i < profile.count; i++) {
                const id = `1234567890123456789${this.profilesToReviewCounter++}`
                user.addProfileToReview({
                  priority: priority,
                  _id: id,
                }) 
              }
            }
          })
        }
      }
      if (userParams.profilesLiked) {
        userParams.profilesLiked.forEach((profile) => {
          user.addProfileLiked({ _id: profile._id })
        })
      }
      this.usersCount++
      promises.push(new UserModel(user).save())
    })
    return Promise.all(promises)
  }
}

module.exports = new UsersProfilesSetup()