const expect = require('chai').expect
const _ = require('lodash')

const controller = rootRequire(`apps/webserviceapp/api/v2/user/controller.profile`)
const genders = rootRequire(`apps/webserviceapp/models/GendersEnum`)
const config = rootRequire(`config`)
const logger = rootRequire(`specs/helpers/logger`)
const specs = require('./helper.user.getUser')


function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

/** Migrating everything from the old specs **/
describe.skip('controller', () => {


  before((done) => {
    specs.setupDatabase()
      .then(() => specs.setupQuickblox())
      .then(() => done())
      .catch((e) => done(new Error(`Error: ${e.message}`)));
  });

  after((done) => {
    specs.closeDatabaseConnection()
      .then(specs.restoreQuickblox.bind(specs))
      .then(done)
      .then((e) => done(new Error(e.message)));
  });

  describe('#getUser test infinite loading', () => {

    const FEMALE_USERS = [
      
    ];

    const MALE_USERS = [
      { _id: 2, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 3, profileVideo: specs.NOT_UPLOADED,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 4, profileVideo: specs.NONE,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 5, profileVideo: specs.UPLOADED_NOT_REVIEWED,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 6, profileVideo: specs.UPLOADED_AND_REJECTED,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 7, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 8, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isLiked: true, isDeleted: false, gender: genders.MALE },
      { _id: 9, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: true, isLiked: false, isDeleted: false, gender: genders.MALE },
      { _id: 10, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: true, isLiked: false, isDeleted: true, gender: genders.MALE }
    ]

    beforeEach((done) => {
      specs.NEW_USER_ID = _.last(MALE_USERS)._id + 3;
      specs.NEW_USER_FACEBOOK_ID = specs.NEW_USER_ID * 10;
      specs.removeAllUsers()
        .then(() => specs.setupUsersCounter(specs.NEW_USER_ID))
        .then(() => specs.insertUsersWithDefaultUsers(_.concat(FEMALE_USERS, MALE_USERS)))
        .then(() => specs.getUsers())
        .then((users) => {
          expect(users.length).to.be.equal(1 + _.concat(MALE_USERS, FEMALE_USERS).length);
          const user = _.filter(users, (u) => u._id === specs.defaultUserId());
          expect(user.length).to.be.equal(1);
          expect(user[0].facebookId).to.be.equal(specs.defaultUserFacebookId());
        })
        .then(() => done())
        .catch((e) => done(new Error(e)));
    });

    it(`should not add a user A to review 
        if the user A is one of the following:
            - doesn't have profile video approved
            - already liked
            - is deleted
            - already matched with you
            - already in profilesToReview`, (done) => {

      const DEFAULT_USER_ID = specs.defaultUserFacebookId();
      const validUsersToReview = _.filter(MALE_USERS, (u) => {
        return !u.matchedWith && !u.isLiked && u.profileVideo === specs.UPLOADED_AND_APPROVED && !u.isDeleted
      });
      expect(validUsersToReview.length).to.be.equal(2);
      specs.getUserViaAPI(DEFAULT_USER_ID)
        .then((u) => {
          expect(u.response.profilesToReview.length).to.be.equal(validUsersToReview.length);
          return true;
        })
        .then(() => specs.getUserViaAPI(DEFAULT_USER_ID))
        .then(() => specs.getUserViaAPI(DEFAULT_USER_ID))
        .then(() => specs.getUserViaAPI(DEFAULT_USER_ID))
        .then(() => specs.getUserViaAPI(DEFAULT_USER_ID))
        .then((u) => {
          expect(u.response.profilesToReview.length).to.be.equal(validUsersToReview.length);
          done();
        })
        .catch((e) => {
          done(new Error(e));
        });
    });

  });

  describe('#getUser when creating a new user.', () => {

    const FEMALE_USERS = [
      { _id: 2, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 3, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: true,  isDeleted: false, gender: genders.FEMALE },
      { _id: 4, profileVideo: specs.UPLOADED_NOT_REVIEWED,  matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 5, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 6, profileVideo: specs.NOT_UPLOADED,           matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 7, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 8, profileVideo: specs.NONE,                   matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 9, profileVideo: specs.UPLOADED_AND_APPROVED,  matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 10, profileVideo: specs.NOT_UPLOADED,          matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 11, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: true,  gender: genders.FEMALE },
      { _id: 12, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: true,  isDeleted: false, gender: genders.FEMALE },
      { _id: 13, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 14, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 15, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 16, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.FEMALE },
      { _id: 17, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.FEMALE }
    ];

    const MALE_USERS = [
      { _id: 18, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 19, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: true,  isDeleted: false, gender: genders.MALE },
      { _id: 20, profileVideo: specs.UPLOADED_NOT_REVIEWED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 21, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 22, profileVideo: specs.NOT_UPLOADED,          matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 23, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 24, profileVideo: specs.NONE,                  matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 25, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 26, profileVideo: specs.NOT_UPLOADED,          matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 27, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 28, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: true,  gender: genders.MALE },
      { _id: 29, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: true,  isDeleted: true,  gender: genders.MALE },
      { _id: 30, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 31, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 32, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: true, gender: genders.MALE },
      { _id: 33, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: true,  isDeleted: false, gender: genders.MALE },
      { _id: 34, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 35, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: true,  isDeleted: false, gender: genders.MALE },
      { _id: 36, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE },
      { _id: 37, profileVideo: specs.UPLOADED_AND_APPROVED, matchedWith: false, isDeleted: false, gender: genders.MALE }
    ];

    
    beforeEach((done) => {
      specs.NEW_USER_ID = _.last(MALE_USERS)._id + 3;
      specs.NEW_USER_FACEBOOK_ID = specs.NEW_USER_ID * 10;
      specs.removeAllUsers()
        .then(() => specs.setupUsersCounter(specs.NEW_USER_ID))
        .then(() => specs.insertUsersWithDefaultUsers(_.concat(FEMALE_USERS, MALE_USERS)))
        .then(() => specs.getUsers())
        .then((users) => {
          expect(users.length).to.be.equal(1 + _.concat(MALE_USERS, FEMALE_USERS).length);
          const user = _.filter(users, (u) => u._id === specs.defaultUserId());
          expect(user.length).to.be.equal(1);
          expect(user[0].facebookId).to.be.equal(specs.defaultUserFacebookId());
        })
        .then(() => done())
        .catch((e) => done(new Error(e)));
    });


    it(`Should return at maximum the amount of profiles to 
        review defined in the config file, and not more.`, (done) => {
      const MAX_BATCH = config.matchRules.REVIEW_BATCH;
      const NEW_USER_FB_ID = specs.newUserFacebookId();
      specs.getUserViaAPI(NEW_USER_FB_ID)
        .then((data) => {
          expect(data.code).to.be.equal(201);
          const user = data.response;
          const profilesToReview = _.get(data, 'response.profilesToReview');
          expect(user.facebookId).to.be.equal(NEW_USER_FB_ID);
          expect(user.gender).to.be.equal(genders.MALE);
          expect(profilesToReview.length).to.be.above(0);
          expect(profilesToReview.length).to.be.at.most(MAX_BATCH);
          return specs.getUserFromDB(NEW_USER_FB_ID);
        })
        .then((u) => {
          expect(u.profilesToReview.length).to.be.above(0);
          expect(u.profilesToReview.length).to.be.at.most(MAX_BATCH);
          // # Login again and check how many profiles we have got now...
          return specs.getUserViaAPI(NEW_USER_FB_ID);
        })
        .then((data) => {
          expect(data.code).to.be.equal(200);
          const user = data.response;
          const profilesToReview = _.get(data, 'response.profilesToReview');
          expect(user.facebookId).to.be.equal(NEW_USER_FB_ID);
          expect(user.gender).to.be.equal(genders.MALE);
          expect(profilesToReview.length).to.be.above(0);
          expect(profilesToReview.length).to.be.at.most(MAX_BATCH);
          done();
        })
        .catch((e) => {
          console.error('Error', e);
          done(e);
        });  
    });

    it(`Given that
          - Firebase token is NOT provided
          - No Quickblox ids have been generated
          - I am providing a Facebook Id to the method
          - The user with the facebook id does not exist in the database
        When I call getUser on the controller
        Then the app
          - Creates the new user with the facebook id
          - Populates with the correct profiles to review (profiles that have been approved, not matched with, and aren't deleted)
          - However, since this is a new user, it will not have any matches
          - Assigns quickblox ids to the user`, (done) => {
      const NEW_USER_FB_ID = specs.newUserFacebookId();
      specs.getUserViaAPI(NEW_USER_FB_ID)
        .then((data) => {
          expect(data.code).to.be.equal(201);
          const userReceived = data.response;
          // Expect all sorts of fields
          expect(userReceived.gender).to.equal(genders.MALE);
          expect(userReceived.facebookId).to.be.equal(NEW_USER_FB_ID);
          expect(userReceived.qbchat).to.exist;
          expect(userReceived.qbchat).to.have.all.keys(['login', 'id', 'password']);
          
          expect(userReceived.profile).to.exist;
          
          expect(userReceived.profile.firstName).to.exist;
          expect(userReceived.profile.lastName).to.exist;
          expect(userReceived.profile.age).to.exist;
          expect(userReceived.profile.aboutMe).to.be.equal('');
          expect(userReceived.profile.occupation).to.be.equal('');
          expect(userReceived.profile.study).to.be.equal('');
          expect(userReceived.profile.interests.length).to.be.equal(0);


          expect(userReceived.preferences).to.exist;
          expect(userReceived.preferences.minAge).to.exist;
          expect(userReceived.preferences.minAge).to.exist;
          expect(userReceived.preferences.distance).to.exist;
          expect(userReceived.preferences.gender).to.exist;
          const expectedIds = [
            specs.defaultUserId(),
            specs.expectedNewUserId()
          ];
          return specs.getUsers();
        })
        .then((users) => {
          expect(users.length).to.be.equal(2 + _.concat(MALE_USERS, FEMALE_USERS).length);
          const userValid = _.some(users, (u, i) => {
            if (u.facebookId !== NEW_USER_FB_ID) return false;

            expect(u.gender).to.exist;
            expect(u.facebookId).to.be.equal(NEW_USER_FB_ID);
            expect(u.qbchat).to.exist;
            expect(u.qbchat.toObject()).to.have.all.keys(['login', 'id', 'password']);
            
            expect(u.profile).to.exist;
            
            expect(u.profile.firstName).to.exist;
            expect(u.profile.lastName).to.exist;
            expect(u.profile.age).to.exist;
            expect(u.profile.aboutMe).to.be.equal('');
            expect(u.profile.occupation).to.be.equal('');
            expect(u.profile.study).to.be.equal('');
            expect(u.profile.interests.length).to.be.equal(0);


            expect(u.preferences).to.exist;
            expect(u.preferences.minAge).to.exist;
            expect(u.preferences.minAge).to.exist;
            expect(u.preferences.distance).to.exist;
            expect(u.preferences.gender).to.exist;
            return true;
          });
          expect(userValid).to.be.equal(true);
          const newUser = _.last(users);
          
          expect(newUser.profilesToReview.length).to.be.equal(config.matchRules.REVIEW_BATCH);
          const actualIdsToReview = _.map(newUser.profilesToReview, (profile) => profile._id);
          const reviewableUsers = _.filter(FEMALE_USERS, (uid) => {
            return uid.profileVideo === specs.UPLOADED_AND_APPROVED && !uid.isDeleted;
          });
          const expectedIds = _.take(_.map(reviewableUsers, (ru) => ru._id), config.matchRules.REVIEW_BATCH);

          expect(_.xor(expectedIds, actualIdsToReview).length).to.be.equal(0);
          done();
      })
      .catch((e) => {
        done(new Error(e));
      });
    });


    it(`Given that
          - A Firebase token IS provided
          - No Quickblox ids have been generated
          - I am providing a Facebook Id to the method
          - The user with the facebook id does not exist in the database
        When I call getUser on the controller
        Then the app
          - Creates the new user with the facebook id
          - (**) Returns only those profiles to review that had their profile videos approved.
          - Assigns quickblox ids to the user`, (done) => {
      const FCM_TOKEN = specs.defaultFcmToken();
      const NEW_USER_FB_ID = specs.newUserFacebookId();

      const request = specs.request().setUserId(NEW_USER_FB_ID).setBody({ 
        fcmToken: FCM_TOKEN 
      });
      const response = specs.response();

      response.on('error', (error) => done(new Error(error.message)));
      response.on('success', (data) => {
        expect(data.code).to.be.equal(201);
        const userReceived = data.response;
        // Expect all sorts of fields
        expect(userReceived.gender).to.exist;
        expect(userReceived.facebookId).to.be.equal(NEW_USER_FB_ID);
        expect(userReceived.qbchat).to.exist;
        expect(userReceived.qbchat).to.have.all.keys(['login', 'id', 'password']);
        
        expect(userReceived.profile).to.exist;
        expect(userReceived.fcm).to.exist;
        expect(userReceived.fcm.token).to.equal(FCM_TOKEN);
        
        expect(userReceived.profile.firstName).to.exist;
        expect(userReceived.profile.lastName).to.exist;
        expect(userReceived.profile.age).to.exist;
        expect(userReceived.profile.aboutMe).to.be.equal('');
        expect(userReceived.profile.occupation).to.be.equal('');
        expect(userReceived.profile.study).to.be.equal('');
        expect(userReceived.profile.interests.length).to.be.equal(0);


        expect(userReceived.preferences).to.exist;
        expect(userReceived.preferences.minAge).to.exist;
        expect(userReceived.preferences.minAge).to.exist;
        expect(userReceived.preferences.distance).to.exist;
        expect(userReceived.preferences.gender).to.exist;
        const EXPECTED_NEW_USER_ID = specs.expectedNewUserId();
        const expectedIds = [
          specs.defaultUserId(),
          EXPECTED_NEW_USER_ID
        ];
        // Check that db has been updated.
        specs.getUsers()
          .then((users) => {
            expect(users.length).to.be.equal(_.concat(FEMALE_USERS, MALE_USERS).length + 2);
            const userIds = _.map(users, (u) => u._id);
            expect(_.xor(expectedIds, userIds).length).to.be.equal(userIds.length - 2);
            const ourNewUser = _.first(_.filter(users, (u) => u._id === EXPECTED_NEW_USER_ID));
            expect(ourNewUser.fcm.token).to.equal(FCM_TOKEN);
            expect(ourNewUser.gender).to.exist;
            expect(ourNewUser.facebookId).to.be.equal(NEW_USER_FB_ID);
            expect(ourNewUser.qbchat).to.exist;
            expect(ourNewUser.qbchat.toObject()).to.have.all.keys(['login', 'id', 'password']);
            
            expect(ourNewUser.profile).to.exist;
            
            expect(ourNewUser.profile.firstName).to.exist;
            expect(ourNewUser.profile.lastName).to.exist;
            expect(ourNewUser.profile.age).to.exist;
            expect(ourNewUser.profile.aboutMe).to.be.equal('');
            expect(ourNewUser.profile.occupation).to.be.equal('');
            expect(ourNewUser.profile.study).to.be.equal('');
            expect(ourNewUser.profile.interests.length).to.be.equal(0);


            expect(ourNewUser.preferences).to.exist;
            expect(ourNewUser.preferences.minAge).to.exist;
            expect(ourNewUser.preferences.minAge).to.exist;
            expect(ourNewUser.preferences.distance).to.exist;
            expect(ourNewUser.preferences.gender).to.exist;
            done();
          })
          .catch((e) => done(new Error(e)));
      });

      controller.getUser(request, response, () => done(new Error('Wrong path.')));
    });

    it('retrieves an existing user from the database', (done) => {
      const defaultUserFbId = specs.defaultUserFacebookId();
      specs.getUserViaAPI(defaultUserFbId)
        .then((data) => {
          expect(data.code).to.be.equal(200);
          expect(data.response._id).to.equal(specs.defaultUserId());
          return true;
        })
        .then(() => specs.getUsers())
        .then((users) => {
          expect(users.length).to.equal(_.concat(FEMALE_USERS, MALE_USERS).length + 1);
          const myUser = _.filter(users, (u) => u._id === specs.defaultUserId());
          expect(myUser.length).to.be.equal(1);
          expect(myUser[0]._id).to.equal(specs.defaultUserId());
          expect(myUser[0].facebookId).to.equal(specs.defaultUserFacebookId());
          done();
        })
        .catch((e) => done(new Error(e)));
    });

    it(`Given that 
          - When the user first logs in, there are no other users of opposite gender to review
        When 
          - We insert more users of opposite gender
          - We log the user in again
        Then
          - The app will retrieve 10 users to review
          - The profiles will have at least one profile video approved
          - The profiles will not be 'deleted'
          - The profiles will not be any that the user already matched with`, (done) => {
      const fbId = specs.defaultUserFacebookId();
      specs.getUserFromDB(fbId)
        .then((u) => {
          expect(u.profilesToReview.length).to.be.equal(0);
          return specs.getUserViaAPI(fbId);
        })
        .then((data) => {
          const user = data.response;
          
          expect(user.profilesToReview.length).to.be.above(0);
          expect(user.profilesToReview.length).to.be.equal(config.matchRules.REVIEW_BATCH);
          
          const possibleMaleUsers = _.filter(MALE_USERS, (male) => {
            return male.profileVideo === specs.UPLOADED_AND_APPROVED &&
              male.isDeleted === false && male.matchedWith === false
          });
          const expectedMaleUsers = _.take(possibleMaleUsers, config.matchRules.REVIEW_BATCH);
          const expectedIds       = _.map(expectedMaleUsers, (u) => u._id);

          const actualIds         = _.map(user.profilesToReview, (profile) => profile._id);
          expect(_.xor(actualIds, expectedIds).length).to.be.equal(0);
          done();
        })
        .catch((e) => done(new Error(e)));
    });

  });
})