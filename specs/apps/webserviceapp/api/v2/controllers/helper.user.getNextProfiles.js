'use strict';
const _ = require('lodash');
const controller = rootRequire(`apps/webserviceapp/api/v2/user/controller.profile`);
const User = rootRequire(`apps/webserviceapp/models/User`);
const MockUser = rootRequire(`specs/mocks/MockUser`);
const SpecsSetup = rootRequire(`specs/helpers/SpecsSetup`);

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}


class GetNextProfilesSpecsHelper extends SpecsSetup {

  constructor() {
    super();
    this.DEFAULT_USER_ID = 1;
    this.DEFAULT_USER_FACEBOOK_ID = 10;
    this.NEXT_SEQ_NUMBER = 38;
    this.UPLOADED_AND_APPROVED = 0;
    this.UPLOADED_NOT_REVIEWED = 1;
    this.UPLOADED_AND_REJECTED = 2;
    this.NOT_UPLOADED = 3;
    this.NONE = 4;
  }

  insertDefaultUser() {
    const mockUser = new MockUser()
              .setUserId(this.DEFAULT_USER_ID)
              .setFacebookId(this.DEFAULT_USER_FACEBOOK_ID)
              .setGenderFemale();
    const user = new User(mockUser)
    return user.save();
  }

  setUsersToReview(ids, reviewedUpTo) {
    return new Promise((resolve, reject) => {
      User.findOne({ _id: this.DEFAULT_USER_ID }).exec()
        .then((u) => {
          u.profilesToReview = _.map(ids, (id) => {
            return { _id: id };
          });
          u.reviewedUpTo = reviewedUpTo;
          return u.save();
        })
        .then(resolve, reject);
    });
  }

  insertUsers(users) {
    const promises = _.map(users, (user, k) => {
      const userData = new MockUser()
                        .setUserId(user._id)
                        .setFacebookId(user._id * 10);
      switch (user.profileVideo) {
        case this.UPLOADED_AND_APPROVED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true,
            review: {
              approved: true,
              reason: '',
              date: new Date()
            }
          });
          break;
        case this.UPLOADED_NOT_REVIEWED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true
          });
          break;
        case this.UPLOADED_AND_REJECTED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true,
            review: {
              approved: false,
              reason: 'Inappropriate video',
              date: new Date()
            }
          });
          break;
        case this.NOT_UPLOADED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: false
          });
          break;
        case this.NONE:
          break;
      }
      return new User(userData).save();
    });
    return Promise.all(promises);
  }

  getNextProfiles() {
    return new Promise((resolve, reject) => {
      const request = this.request()
                          .setUserId(this.DEFAULT_USER_FACEBOOK_ID);
      const response = this.response();
      response.on('success', (result) => {
        resolve(result);
      });

      response.on('error', (error) => {
        reject(new Error(error));
      });

      controller.getNextProfiles(request, response, (e) => reject(new Error('Wrong path')));
    });
  }

  getMyUser() {
    return User.findOne({ _id: this.DEFAULT_USER_ID }).exec();
  }

}

module.exports = new GetNextProfilesSpecsHelper();