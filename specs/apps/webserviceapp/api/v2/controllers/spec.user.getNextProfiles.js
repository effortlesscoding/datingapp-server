'use strict';
const _ = require('lodash');
const config = rootRequire('config');
const logger = rootRequire('specs/helpers/');
const specs = require('./helper.user.getNextProfiles');
const expect = require('chai').expect;

function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}config`)
}

describe.skip('controller', () => {

  const USER_IDS = [
    { _id: 2, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 3, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 4, profileVideo: specs.UPLOADED_NOT_REVIEWED },
    { _id: 5, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 6, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 7, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 8, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 9, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 10, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 11, profileVideo: specs.NOT_UPLOADED },
    { _id: 12, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 13, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 14, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 15, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 16, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 17, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 18, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 19, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 20, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 21, profileVideo: specs.UPLOADED_AND_REJECTED },
    { _id: 22, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 23, profileVideo: specs.NONE },
    { _id: 24, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 25, profileVideo: specs.NOT_UPLOADED },
    { _id: 26, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 27, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 28, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 29, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 30, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 31, profileVideo: specs.UPLOADED_NOT_REVIEWED },
    { _id: 32, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 33, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 34, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 35, profileVideo: specs.UPLOADED_AND_APPROVED },
    { _id: 36, profileVideo: specs.UPLOADED_AND_APPROVED }
  ];

  describe('#getNextProfiles', () => {
    before((done) => {

      specs.setupDatabase()
        .then(() => specs.setupQuickblox())
        .then(() => done())
        .catch((e) => done(new Error(`Error: ${e.message}`)));
    });

    after((done) => {
      specs.closeDatabaseConnection()
        .then(specs.restoreQuickblox.bind(specs))
        .then(done)
        .then((e) => done(new Error(e.message)));
    });

    beforeEach((done) => {
      specs.removeAllUsers()
        .then(() => specs.insertDefaultUser())
        .then(() => specs.insertUsers(USER_IDS))
        .then(() => done())
        .catch((e) => done(new Error(e)));
    })

    it('Should only return those profiles that have profile videos approved', (done) => {
      const PROFILE_IDS_TO_REVIEW = [16, 17, 18, 19];
      // TODO: Write some function instead of hardcoding ??
      const EXPECTED_IDS = [16, 17, 18, 19, 20, 22, 24, 26, 27, 28];
      specs.setUsersToReview(PROFILE_IDS_TO_REVIEW, 19)
        .then(() => specs.getNextProfiles())
        .then((apiResponse) => {
          const response = apiResponse.response;
          const responseIds = _.map(response, (profile, i) => profile._id);
          expect(_.xor(responseIds, EXPECTED_IDS).length).to.be.equal(0);
          done();
        })
        .catch((e) => done(new Error(e)));
    });

    it(`Given that
          - There are only 3 more profiles left for the current user to review
          - There are 30 profiles in total available for review in the system, and the user had 
          reviewed 27 of them (hence 3 left to review)
          - The user "likes/passes" all 3 of them
        When the user requests for next profiles
          - The system will loop from the beginning and return first X profiles that are available
          - Only profiles that have had profile videos approved will be returned, obviously
          - X is defined in the config
      `, (done) => {

      let userIdsToLike, expectedIds;
      const PROFILE_IDS_TO_REVIEW = [34, 35, 36];

      specs.setUsersToReview(PROFILE_IDS_TO_REVIEW, 37)
      .then(() => specs.getMyUser())
      .then((user) => {
        expect(user.profilesToReview.length).to.be.equal(PROFILE_IDS_TO_REVIEW.length);
        expect(user.profilesToReview.length).to.be.below(config.matchRules.lowProfilesCount);
        return specs.getNextProfiles();
      })
      .then((apiResponse) => {
        const response = apiResponse.response;
        const responseIds = _.map(response, (profile, i) => profile._id);
        const validProfilesToReview = _.map(_.filter(USER_IDS, (uid) => uid.profileVideo === specs.UPLOADED_AND_APPROVED), (user) => {
          return user._id;
        });
        const expectedAdditionalIds = _.take(validProfilesToReview, config.matchRules.REVIEW_BATCH - PROFILE_IDS_TO_REVIEW.length);
        expectedIds = _.concat(PROFILE_IDS_TO_REVIEW, expectedAdditionalIds);
        expect(response.length).to.be.equal(config.matchRules.REVIEW_BATCH);
        expect(_.xor(expectedIds, responseIds).length).to.be.equal(0);
        return specs.getMyUser();
      })
      .then((u) => {
        const profilesToReviewIds = _.map(u.profilesToReview, (profile) => profile._id);

        expect(_.xor(expectedIds, profilesToReviewIds).length).to.be.equal(0);
        done();
      })
      .catch((e) => {
        done(new Error(e));
      });
    });



  });
});