'use strict';
const _ = require('lodash')
const SpecsSetup = rootRequire(`specs/helpers/SpecsSetup`)
const MockUser = rootRequire(`specs/mocks/MockUser`)
const logger = rootRequire(`specs/helpers/logger`)
const User = rootRequire(`apps/webserviceapp/models/User`)
const genders = rootRequire(`apps/webserviceapp/models/GendersEnum`)
const controller = rootRequire(`apps/webserviceapp/api/v2/user/controller.profile`)


function rootRequire(path) {
  const ROOT_PATH = '../../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class GetUserSpecsHelper extends SpecsSetup {

  constructor() {
    super();
    this.DEFAULT_USER_ID = 1;
    this.DEFAULT_USER_FACEBOOK_ID = 10;
    this.NEW_USER_ID = null;
    this.NEW_USER_FACEBOOK_ID = null;
    this.DEFAULT_FCM_TOKEN = '123456';
    this.OPPOSITE_SEX_COUNT = 5;

    this.UPLOADED_AND_APPROVED = 0;
    this.UPLOADED_NOT_REVIEWED = 1;
    this.UPLOADED_AND_REJECTED = 2;
    this.NOT_UPLOADED = 3;
    this.NONE = 4;
  }

  setupUsersCounter(nextSeqNumber) {
    this.NEXT_SEQ_NUMBER = nextSeqNumber;
    this.resetUsersCounter();
  }

  insertDefaultUser() {
    logger.debug('Inserting a default user!');
    const defaultUserId = this.DEFAULT_USER_ID;
    const defaultUserFacebookId = this.DEFAULT_USER_FACEBOOK_ID;
    logger.debug(defaultUserId);
    logger.debug(defaultUserFacebookId);
    return new Promise((resolve, reject) => {
      const mockUser = new MockUser()
                .setUserId(defaultUserId)
                .setFacebookId(defaultUserFacebookId)
                .setGenderFemale();
      const user = new User(mockUser)
      user.save().then(resolve, reject);
    });
  }


  insertUsersWithDefaultUsers(users) {
    const defaultUserId = this.DEFAULT_USER_ID;
    const defaultUserFacebookId = this.DEFAULT_USER_FACEBOOK_ID;
    const mockUser = new MockUser()
              .setUserId(defaultUserId)
              .setFacebookId(defaultUserFacebookId)
              .setGenderFemale();
    const promises = _.map(users, (user, k) => {
      const userData = new MockUser()
                        .setUserId(user._id)
                        .setFacebookId(user._id * 10);
      if (user.gender === genders.FEMALE) {
        userData.setGenderFemale();
      } else {
        userData.setGenderMale();
      }
      if (user.isDeleted) {
        userData.isDeleted = true;
      }
      if (user.matchedWith) {
        // Hm... lol...
        mockUser.addMatchedProfileId(user._id);
      }

      if (user.isLiked) {
        // Hm... lol...
        mockUser.addLikedProfileId(user._id);
      }
      switch (user.profileVideo) {
        case this.UPLOADED_AND_APPROVED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true,
            review: {
              approved: true,
              reason: '',
              date: new Date()
            }
          });
          break;
        case this.UPLOADED_NOT_REVIEWED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true
          });
          break;
        case this.UPLOADED_AND_REJECTED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: true,
            review: {
              approved: false,
              reason: 'Inappropriate video',
              date: new Date()
            }
          });
          break;
        case this.NOT_UPLOADED:
          userData.addProfileVideo({
            _id: 1,
            storagePath: 'some/place',
            fileName: 'someFile.mp4',
            thumbnail: 'someThumbnail.mp4',
            uploaded: false
          });
          break;
        case this.NONE:
          break;
      }
      return new User(userData).save();
    });
    promises.push(new User(mockUser).save());
    return Promise.all(promises);
  }

  getUsers() {
    return new Promise((resolve, reject) => {
      User.find({}).exec().then(resolve, reject);
    });
  }

  defaultUserId() {
    return this.DEFAULT_USER_ID;
  }

  defaultFcmToken() {
    return this.DEFAULT_FCM_TOKEN;
  }

  defaultUserFacebookId() {
    return this.DEFAULT_USER_FACEBOOK_ID;
  }

  expectedNewUserId() {
    return this.NEW_USER_ID;
  }

  newUserFacebookId() {
    return this.NEW_USER_FACEBOOK_ID;
  }

  insertOppositeSexUsers() {
    let promises = [];
    for (let i = 3; i < this.OPPOSITE_SEX_COUNT + 3; i++) {
      const mockUser = new MockUser().setGenderMale().setUserId(i).setFacebookId(i * 10);
      const dbUser = new User(mockUser);
      promises.push(dbUser.save());
    }
    return Promise.all(promises);
  }

  oppositeSexUsersCount() {
    return this.OPPOSITE_SEX_COUNT;
  }

  getUserFromDB(fbId) {
    return User.findOne({facebookId: fbId}).exec();
  }
  
  getUserViaAPI(userFacebookId) {
    return new Promise((resolve, reject) => {
      const request = this.request().setUserId(userFacebookId);
      const response = this.response();

      response.on('error', (error) => reject(new Error(error)));
      response.on('success', (data) => {
        resolve(data);
      });
      controller.getUser(request, response, () => done(new Error('Wrong path.')));
    });
  }
};

module.exports = new GetUserSpecsHelper();