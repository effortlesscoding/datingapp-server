'use strict'

const _ = require('lodash')
const expect = require('chai').expect

const useCase = rootRequire('apps/webserviceapp/usecases/user/registration.js').UseCase
const genders = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const config = rootRequire('config')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

describe('#prepareNewUser', () => {
  
  it('should fail if fbUser is empty', (done) => {
    const seqInfo = { _id: 1, nextSeqNumber: 2 }
    const fbUser = null
    expect(useCase.prepareNewUser.bind(null, seqInfo, fbUser)).to.throw('Fb user is nil')
    done()
  })

  it('should fail if seqInfo is empty', (done) => {
    const seqInfo = null
    const fbUser = {
      gender: 'male',
      firstName: 'Anh',
      lastName: 'Dao',
      email: 'email@email.com'
    }
    expect(useCase.prepareNewUser.bind(null, seqInfo, fbUser)).to.throw('No seq number')
    done()
  })

  it('should fail if seqInfo has no nextSeqNumber', (done) => {
    const seqInfo = { _id: 1 }
    const fbUser = {
      gender: 'male',
      firstName: 'Anh',
      lastName: 'Dao',
      email: 'email@email.com'
    }
    expect(useCase.prepareNewUser.bind(null, seqInfo, fbUser)).to.throw('No seq number')
    done()
  })

  it('should prepare a new user if valid parameters are provided', (done) => {
    const seqInfo = { _id: 1, nextSeqNumber: 2 }
    const fbUser = {
      id: 123,
      gender: 'male',
      firstName: 'Anh',
      lastName: 'Dao',
      email: 'email@email.com',
      age: 24,
    }
    const location = {
      lat: -37,
      lng: -144,
    }
    const u = useCase.prepareNewUser(seqInfo, fbUser, location, 'abc')
    expect(u.gender).to.be.equal(0)
    expect(u.facebookId).to.be.equal(123)
    expect(u.location.lat).to.be.equal(-37)
    expect(u.location.lng).to.be.equal(-144)
    expect(u.preferences.distance).to.be.equal(3)
    expect(u.preferences.age.min).to.be.equal(18)
    expect(u.preferences.age.max).to.be.equal(24)
    expect(u.preferences.gender.female).to.be.equal(true)
    expect(u.preferences.gender.male).to.be.equal(false)
    expect(u.profile.age).to.be.equal(24)
    expect(u.profile.email).to.be.equal('email@email.com')
    expect(u.profile.firstName).to.be.equal('Anh')
    expect(u.profile.lastName).to.be.equal('Dao')
    expect(u.reviewedUpTo).to.be.equal(0)
    expect(u.seqNumber).to.be.equal(2)
    expect(u.fcm.token).to.be.equal('abc')
    done()
  })

  it('should prepare a new user without fcm token and location if they are not provided', (done) => {
    const seqInfo = { _id: 1, nextSeqNumber: 2 }
    const fbUser = {
      gender: 'male',
      firstName: 'Anh',
      lastName: 'Dao',
      email: 'email@email.com',
      age: 24,
    }
    const u = useCase.prepareNewUser(seqInfo, fbUser)
    expect(_.get(u, 'fcm.token', undefined)).to.be.equal(undefined)
    expect(u.gender).to.be.equal(0)
    expect(_.get(u, 'location.lat', undefined)).to.be.equal(undefined)
    expect(_.get(u, 'location.lng', undefined)).to.be.equal(undefined)
    expect(u.preferences.age.max).to.be.equal(24)
    expect(u.preferences.age.min).to.be.equal(18)
    expect(u.preferences.distance).to.be.equal(3)
    expect(u.preferences.gender.female).to.be.equal(true)
    expect(u.preferences.gender.male).to.be.equal(false)
    expect(u.profile.age).to.be.equal(24)
    expect(u.profile.email).to.be.equal('email@email.com')
    expect(u.profile.firstName).to.be.equal('Anh')
    expect(u.profile.lastName).to.be.equal('Dao')
    expect(u.reviewedUpTo).to.be.equal(0)
    expect(u.seqNumber).to.be.equal(2)
    done()
  })

})