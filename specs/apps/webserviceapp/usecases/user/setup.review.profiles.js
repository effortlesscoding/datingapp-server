'use strict'
const expect = require('chai').expect
const _ = require('lodash')

const DbSetup = rootRequire('specs/setuphelpers/DbSetup.js')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const MockUser = rootRequire('specs/mocks/MockUser.js')
const User = rootRequire('apps/webserviceapp/models/User.js')

const calculatePriority = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').calculatePriority
const getKmDistance = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').getKmDistance
const getMatchScore = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').getMatchScore
const matchingDefaults = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').Constants

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class DbSpecsSetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 1
  }

  addUser(params) {
    const seqNumber = params.seqNumber
    const mockUser = new MockUser()
                .setUserSeqNumber(seqNumber)
                .setFacebookId(seqNumber * 10)
    if (params.gender === GendersEnum.MALE) {
      mockUser.setGenderMale()
    } else if (params.gender === GendersEnum.FEMALE) {
      mockUser.setGenderFemale()
    }
    for (let i = 0; i < params.profilesToReview.count; i++) {
      mockUser.addProfileToReview({
        seqNumber: i,
        matchScore: params.profilesToReview.matchScore,
        viewed: false,
      })
    }
    this.usersCount++
    const dbUser = new User(mockUser)
    return dbUser.save()
  }

  removeUsers() {
    return User.remove({}).exec()
  }

  addUsersInBulk(bulkParams) {
    const {
      count,
      isDeleted,
      gender,
      reviewedUpTo,
    } = bulkParams

    const savePromises = []
    for (let i = 0; i < count; i++) {
      const userToAdd = new MockUser()
                          .setUserSeqNumber(this.usersCount)
                          .setFacebookId(this.usersCount)
                          .setIsDeleted(isDeleted)
      this.usersCount++
      if (!_.isNil(reviewedUpTo)) {
        userToAdd.setReviewedUpTo(reviewedUpTo)
      }
      if (gender === GendersEnum.MALE) {
        userToAdd.setGenderMale()
      } else if (gender === GendersEnum.FEMALE) {
        userToAdd.setGenderFemale()
      }
      const userModel = new User(userToAdd)
      savePromises.push(userModel.save())
    }
    return Promise.all(savePromises)
  }
  
  resetDatabaseFully() {
    this.usersCount = 0
    return this.resetDatabase()
  }

  addUsers(usersData) {
    return User.findOne({ seqNumber: usersData.priorityWithUserSeqNumber }).exec()
      .then((user) => {
        if (!user) throw new Error('No user found')
        let savePromises = []
        usersData.profiles.forEach((userData) => {
          let userToAdd = new MockUser()
          if (userData.gender === GendersEnum.MALE) {
            userToAdd.setGenderMale()
          } else if (userData.gender === GendersEnum.FEMALE) {
            userToAdd.setGenderFemale()
          }
          userToAdd = updateProfileToPriority(user, userToAdd, userData.priority)
          for (let i = 0; i < userData.count; i++) {
            const userModel = new User(
              userToAdd
                .setUserSeqNumber(this.usersCount)
                .setFacebookId(this.usersCount * 10)
            )
            this.usersCount++
            savePromises.push(userModel.save())
          }
        })
        return Promise.all(savePromises)
      })
      .then(() => {
      })
      .catch((error) => {
        console.error('addUsers', error)
        throw new Error(error.message)
      })
  }

  getUserById(id) {
    return User.findOne({ _id: id, }).exec()
  }

  getUser(params) {
    return User.findOne({ seqNumber: params.seqNumber }).exec()
  }

  getUsers() {
    return User.find({}).exec()
  }
}

class SpecsSetup {

  constructor() {
    this.db = new DbSpecsSetup()
  }
}


// -37.8200093,144.9540194
// -37.8154862,144.9738604
// lat diff : -0.0045231
// lng diff : -0.019841
// => 1.85 km 

// -37.8224025,144.9547354
// -37.8757027,144.9928935
// lat diff : -0.0533002
// lng diff : -0.0381581
// => 7.00 km difference

// -37.8212404,144.9549314
// -37.9602121,145.054702
// lat diff : -0.1389717
// lng diff : -0.0997706
// => 17.78 km difference
function updateProfileToPriority(user, userToReview, priority) {
  const preferences = _.get(user, 'preferences', {})
  const distancePreference = _.get(preferences, 'distance', matchingDefaults.DEFAULT_DISTANCE_PREFERENCE)
  const genderPreferences = _.get(preferences, 'gender', { 
    male: user.gender === GendersEnum.FEMALE, 
    female: user.gender === GendersEnum.MALE,
  })
  const agePreferences = { 
    min: _.get(preferences, 'age.min', matchingDefaults.DEFAULT_MIN_AGE),
    max: _.get(preferences, 'age.min', matchingDefaults.DEFAULT_MAX_AGE)
  }
  let distanceDifference = 0
  switch (priority) {
    case 0:
      // match all two user preferences (age, distance)
      userToReview.profile.age = agePreferences.min + 1
      userToReview.profile.location = {
        lat: user.profile.location.lat - 0.0045231,
        lng: user.profile.location.lng - 0.019841,
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.below(distancePreference)
      break
    case 1:
      // match all two user preferences (age, distance), except distance with minor difference
      userToReview.profile.age = agePreferences.min + 1
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.0533002,
        lng: userToReview.profile.location.lng - 0.0381581,
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.above(distancePreference)
      expect(distanceDifference).to.be.below(distancePreference + matchingDefaults.DISTANCE_SMALL_MARGIN)
      break
    case 2:
      // Small age difference, Small distance difference
      userToReview.profile.age = agePreferences.max + 3
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.0533002,
        lng: userToReview.profile.location.lng - 0.0381581
      }
      distanceDifference = getKmDistance(user.profile.location, userToReview.profile.location)
      expect(distanceDifference).to.be.above(distancePreference)
      expect(distanceDifference).to.be.below(distancePreference + matchingDefaults.DISTANCE_SMALL_MARGIN)
      break
    case 3:
      // Small age difference, big distance difference
      userToReview.profile.age = agePreferences.max + 3
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.1389717,
        lng: userToReview.profile.location.lng - 0.0997706
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.above(distancePreference + matchingDefaults.DISTANCE_SMALL_MARGIN)
      expect(distanceDifference).to.be.below(distancePreference + matchingDefaults.DISTANCE_BIG_MARGIN)
      break
    case 4:
      // Big age difference, and normal distance difference
      userToReview.profile.age = agePreferences.max + 9
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.0045231,
        lng: userToReview.profile.location.lng - 0.019841
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.below(distancePreference)
      break
    case 5:
      // Big age difference and small distance difference
      userToReview.profile.age = agePreferences.max + 9
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.0533002,
        lng: userToReview.profile.location.lng - 0.0381581,
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.above(distancePreference)
      expect(distanceDifference).to.be.below(distancePreference + matchingDefaults.DISTANCE_SMALL_MARGIN)
      break
    case 6:
      // Big age and Big difference
      userToReview.profile.age = agePreferences.max + 9
      userToReview.profile.location = {
        lat: userToReview.profile.location.lat - 0.1389717,
        lng: userToReview.profile.location.lng - 0.0997706,
      }
      distanceDifference = getKmDistance(userToReview.profile.location, user.profile.location)
      expect(distanceDifference).to.be.above(distancePreference + matchingDefaults.DISTANCE_SMALL_MARGIN)
      expect(distanceDifference).to.be.below(distancePreference + matchingDefaults.DISTANCE_BIG_MARGIN)
      break
  }
  const priorityCalculated = calculatePriority(user, userToReview)
  expect(priorityCalculated.priority).to.be.equal(priority)
  return userToReview
}

module.exports = new SpecsSetup()