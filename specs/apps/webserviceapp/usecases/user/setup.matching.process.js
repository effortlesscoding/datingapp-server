'use strict'
const expect = require('chai').expect
const _ = require('lodash')
const mongoose = require('mongoose')

const DbSetup = rootRequire('specs/setuphelpers/DbSetup.js')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const MockUser = rootRequire('specs/mocks/MockUser.js')
const User = rootRequire('apps/webserviceapp/models/User.js')

const calculatePriority = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').calculatePriority
const getKmDistance = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').getKmDistance
const getMatchScore = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').getMatchScore
const matchingDefaults = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').Constants

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class DbSpecsSetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 1
  }

  addUser(user) {
    const seqNumber = this.usersCount++
    const rawUser = new MockUser()
                .setUserId(mongoose.Types.ObjectId(user._id))
                .setUserSeqNumber(seqNumber)
                .setFacebookId(user.facebookId)

    if (user.gender === GendersEnum.MALE) {
      rawUser.setGenderMale()
    } else if (user.gender === GendersEnum.FEMALE) {
      rawUser.setGenderFemale()
    }
    user.profilesToReview.forEach((profile) => {
      rawUser.addProfileToReview({
        _id: profile._id,
        priority: profile.priority,
      })
    })
    return new User(rawUser).save()
  }

  addUsers(users) {
    let promises = []
    users.forEach((userData) => {
      const seqNumber = this.usersCount++
      const rawUser = new MockUser()
                  .setUserId(mongoose.Types.ObjectId(userData._id))
                  .setUserSeqNumber(seqNumber)
                  .setFacebookId(seqNumber * 10)

      if (userData.gender === GendersEnum.MALE) {
        rawUser.setGenderMale()
      } else if (userData.gender === GendersEnum.FEMALE) {
        rawUser.setGenderFemale()
      }
      if (userData.profilesLiked && userData.profilesLiked.length) {
        userData.profilesLiked.forEach((profile) => {
          rawUser.addProfileLiked({
            _id: profile._id
          })
        })
      }
      promises.push(new User(rawUser).save())
    })
    return Promise.all(promises)
  }

}

class SpecsSetup {

  constructor() {
    this.db = new DbSpecsSetup()
  }
}

module.exports = new SpecsSetup()