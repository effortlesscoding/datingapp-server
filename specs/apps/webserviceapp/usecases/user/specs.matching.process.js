'use strict'

const _ = require('lodash')
const sinon = require('sinon')
const sinonChai = require("sinon-chai")
const chai = require('chai')
const expect = chai.expect
chai.use(sinonChai)

const useCase = rootRequire('apps/webserviceapp/usecases/user/matching.process.js').UseCase
const NotificationsUseCase = rootRequire('apps/webserviceapp/usecases/notifications/').UseCase
const genders = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const config = rootRequire('config')
const setup = require('./setup.matching.process.js')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

describe('useCase', () => {
  // Before each:
  // Fill the database up with a user who has a few profiles to review
  before((done) => {
    setup.db.connect()
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('MongoDb error', e)
        done(new Error('Failed to connect to mongodb'))
      })
  })

  after((done) => {
    setup.db.disconnect()
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('Disconnect error', e)
        done(new Error(e.message))
      })
  })
  const usersIds = [
    { _id: '53cb6b9b4f4ddef1ad47f941', facebookId: 1, },
    { _id: '53cb6b9b4f4ddef1ad47f942', facebookId: 2, },
    { _id: '53cb6b9b4f4ddef1ad47f943', facebookId: 3, },
    { _id: '53cb6b9b4f4ddef1ad47f944', facebookId: 4, },
  ]

  beforeEach((done) => {
    setup.db.resetDatabase()
      .then(() => {
        return setup.db.addUser({
          _id: usersIds[0]._id,
          facebookId: usersIds[0].facebookId,
          gender: genders.MALE,
          profilesToReview: [
            { _id: usersIds[1]._id, priority: 0 },
            { _id: usersIds[2]._id, priority: 1 },
            { _id: usersIds[3]._id, priority: 4 },
          ]
        })
      })
      .then(() => {
        return setup.db.addUsers([
          { gender: genders.FEMALE, _id: usersIds[1]._id, facebookId: usersIds[1].facebookId, profilesLiked: [ { _id: usersIds[0] }] }, // <-- Will match
          { gender: genders.FEMALE, _id: usersIds[2]._id, facebookId: usersIds[2].facebookId, },
          { gender: genders.FEMALE, _id: usersIds[3]._id, facebookId: usersIds[3].facebookId, },
        ])
      })
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('Error', e)
        done(new Error(e.mesage))
      })
  })

  describe('#pass', (done) => {
    let notificationsStub

    before((done) => {
      notificationsStub = sinon.stub(NotificationsUseCase, 'sendMatchedNotification', () => {})
      done()
    })

    afterEach((done) => {
      notificationsStub.restore()
      done()
    })

    it('should pass on a user', (done) => {
      const userIdToPass = usersIds[1]._id
      useCase.pass(usersIds[0].facebookId, userIdToPass)
        .then((u) => {
          for (let key in u.profilesToReview) {
            const profiles = u.profilesToReview[key]
            const passedUserStillInReview = _.some(profiles, (p) => {
              return p && p._id.toString() === userIdToPass
            })
            expect(passedUserStillInReview).to.be.equal(false)
          }
          expect(notificationsStub.callCount).to.be.equal(0)
          done()
        })
        .catch((e) => {
          console.error('Error', e)
          done(new Error(e.message))
        })
    })
  })

  describe('#like', (done) => {
    let notificationsStub

    before((done) => {
      notificationsStub = sinon.stub(NotificationsUseCase, 'sendMatchedNotification', () => {})
      done()
    })

    afterEach((done) => {
      notificationsStub.restore()
      done()
    })

    it('should like, match and send a push notification', (done) => {
      const userIdToLike = usersIds[1]._id
      useCase.like(usersIds[0].facebookId, userIdToLike)
        .then((result) => {
          const u = result.user
          expect(result.liked).to.be.true
          expect(result.matched).to.be.true
          for (let priority in u.profilesToReview) {
            const profiles = u.profilesToReview[priority]
            const likedUserStillInReview = _.some(profiles, (p) => {
              return p && p._id.toString() === userIdToLike
            })
            expect(likedUserStillInReview).to.be.equal(false)
          }
          const userLiked = u.profilesLiked.some((pl) => pl._id.toString() === userIdToLike)
          expect(userLiked).to.be.equal(false)
          const userLikedMatched = u.profilesMatched.some((pl) => pl._id.toString() === userIdToLike)
          expect(userLikedMatched).to.be.equal(true)
          expect(notificationsStub.callCount).to.be.equal(1)
          done()
        })
        .catch((e) => {
          console.error('Error', e)
          done(new Error(e.message))
        })
    })

    it('should like, match and send a push notification', (done) => {
      const userIdToLike = usersIds[2]._id
      useCase.like(usersIds[0].facebookId, userIdToLike)
        .then((result) => {
          const u = result.user
          expect(result.liked).to.be.true
          expect(result.matched).to.be.false
          for (let priority in u.profilesToReview) {
            const profiles = u.profilesToReview[priority]
            const likedUserStillInReview = _.some(profiles, (p) => {
              return p && p._id.toString() === userIdToLike
            })
            expect(likedUserStillInReview).to.be.equal(false)
          }
          const userLiked = u.profilesLiked.some((pl) => pl._id.toString() === userIdToLike)
          expect(userLiked).to.be.equal(true)
          const userLikedMatched = u.profilesMatched.some((pl) => pl._id.toString() === userIdToLike)
          expect(userLikedMatched).to.be.equal(false)
          expect(notificationsStub.callCount).to.be.equal(1)
          done()
        })
        .catch((e) => {
          console.error('Error', e)
          done(new Error(e.message))
        })
    })

    it('should return an error if the id liked is not in review', (done) => {
      const userIdToLike = '53cb6b9b4f4ddef1ad47f000'
      useCase.like(usersIds[0].facebookId, userIdToLike)
        .then((u) => {
          expect(notificationsStub.callCount).to.be.equal(0)
          done(new Error('Expected an error'))
        })
        .catch((e) => {
          console.error(e)
          done()
        })
    })

  })

})