'use strict'

const _ = require('lodash')
const expect = require('chai').expect

const useCase = rootRequire('apps/webserviceapp/usecases/user/review.profiles.js').UseCase
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const config = rootRequire('config')

const setup = require('./setup.review.profiles.js')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

const addUser = setup.db.addUser.bind(setup.db)
const addUsers = setup.db.addUsersInBulk.bind(setup.db)
const removeUsers = setup.db.removeUsers.bind(setup.db)
const resetDatabase = setup.db.resetDatabaseFully.bind(setup.db)
const getUser = setup.db.getUserById.bind(setup.db)
const getUserBySeqNumber = setup.db.getUser.bind(setup.db)
const getUsers = setup.db.getUsers.bind(setup.db)
const hashToArray = (hash) => {
  const hashConverted = hash && hash.toObject ? hash.toObject({ versionKey: false, }) : hash
  let array = []
  for (let key in hashConverted) {
    array = array.concat(hashConverted[key])
  }
  return array
}
const areArraysEqual = (array1, array2) => {
  return _.xor(array1, array2).length === 0
}
/**
 * Algorithm described:
 * You have a bunch of users with id from 1 to X
 * and every time you go through one user, you put him into one of the six categories
 * 1. High priority with all 3 search criteria
 * 2. Medium priority with all 3 search criteria
 * If it doesn't fit 1, or 2
 * 3. High priority with only 2 search criteria
 * 4. Medium priority with only 2 search criteria
 * If it doesn't fit 1, 2, 3, or 4
 * 5. High priority with only 1 search criterion
 * 6. Medium priority with only 1 search criterion
 * 7. The very lowest priority : )
 * 
 * You go through all of the profiles until you get config.matchRules.MAX_HIGH_PRIORITY
 * amount of profiles
 * 
 */
describe('should populate next profiles', () => {

  describe('#populateNextProfilesToReview', () => {
    
    const MAX_HIGH_PRIORITY = 30

    before((done) => {
      expect(_.get(config, 'matchRules.MAX_PROFILES_TO_REVIEW', undefined)).to.be.above(0)
      config.matchRules.MAX_HIGH_PRIORITY = MAX_HIGH_PRIORITY

      setup.db.connect()
        .then(() => {
          done()
        })
        .catch((e) => {
          console.error('MongoDb error', e)
          done(new Error('Failed to connect to mongodb'))
        })
    })

    after((done) => {
      setup.db.disconnect()
        .then(() => {
          done()
        })
        .catch((e) => {
          console.error('Disconnect error', e)
          done(new Error(e.message))
        })
    })

    describe(`250 eligible profiles to review in the database with a batch of 50`, (done) => {
      let eligibleUsersIds = []
      let myUserId
      const INELIGIBLE_1_COUNT = 3
      const INELIGIBLE_2_COUNT = 3
      before((done) => {
        // insert MAX_USERS_TO_REVIEW * 5 ==> 250 of them.
        // Also, insert a few users who are not qualified (deleted = true) inbetween
        // add just one more user, the test user
        config.matchRules.MAX_PROFILES_TO_REVIEW = 50
        const saveUsersIds = (users) => {
          eligibleUsersIds = eligibleUsersIds.concat(
            _.map(users, (user) => user._id.toString())
          )
        }

        const saveMyUser = (users) => {
          myUserId = users[0]._id
        }

        resetDatabase()
          .then(() => addUsers({ count: 49, isDeleted: false, gender: GendersEnum.FEMALE,}))
          .then((eligibleInserted) => saveUsersIds(eligibleInserted))
          .then(() => addUsers({ count: INELIGIBLE_1_COUNT, isDeleted: true, gender: GendersEnum.FEMALE,}))
          .then(() => addUsers({ count: 51, isDeleted: false, gender: GendersEnum.FEMALE,}))
          .then((eligibleInserted) => saveUsersIds(eligibleInserted))
          .then(() => addUsers({ count: INELIGIBLE_2_COUNT, isDeleted: false, gender: GendersEnum.MALE,}))
          .then((usersInserted) => saveMyUser(usersInserted))
          .then(() => addUsers({ count: 150, isDeleted: false, gender: GendersEnum.FEMALE,}))
          .then((eligibleInserted) => saveUsersIds(eligibleInserted))
          .then(() => done())
          .catch((e) => done(new Error(e.message)))
      })

      it('should replace existing profilesToReview with the next batch on every call', (done) => {
        // Get the user.
        const BATCH_SIZE = config.matchRules.MAX_PROFILES_TO_REVIEW
        let originalProfilesToReview
        getUser(myUserId)
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            expect(user.reviewedUpTo).to.be.equal(-1)
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)
            originalProfilesToReview = profilesToReview
            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(BATCH_SIZE + INELIGIBLE_1_COUNT - 1)

            const expectedUsersIds = _.slice(eligibleUsersIds, 0, BATCH_SIZE)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)

            const expectedReviewedUpTo = 2 * BATCH_SIZE + INELIGIBLE_1_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE, BATCH_SIZE * 2)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true

            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)
            
            const expectedReviewedUpTo = 3 * BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE * 2, BATCH_SIZE * 3)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true

            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)
            const expectedReviewedUpTo = 4 * BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE * 3, BATCH_SIZE * 4)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)
            const expectedReviewedUpTo = 5 * BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE * 4, BATCH_SIZE * 5)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            // Repeat the cycle
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)
            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const expectedUsersIds = _.slice(eligibleUsersIds, 0, BATCH_SIZE)
            const actualUsersIds = profilesToReview.map((ptr) => ptr._id.toString())
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            done()
          })
          .catch((e) => {
            console.error('Error##', e)
            done(e)
          })
      })

    })

    describe(`0 eligible profiles to review in the database`, (done) => {
      
      let myUserId
      before((done) => {
        // remove all users
        // add just one user, the test user
        config.matchRules.MAX_PROFILES_TO_REVIEW = 50
        resetDatabase()
          .then(() => addUsers({ count: 1, isDeleted: true, reviewedUpTo: 1, gender: GendersEnum.MALE }))
          .then((users) => {
            myUserId = users[0]._id
            done()
          })
          .catch((e) => done(new Error(e.message)))
      })

      it('should return no profilesToReview', (done) => {
        getUser(myUserId)
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            expect(user.reviewedUpTo).to.be.equal(1)
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            expect(user.reviewedUpTo).to.be.equal(-1)
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            expect(user.reviewedUpTo).to.be.equal(-1)
            done()
          })
          .catch((e) => {
            console.error('##', e)
            done(e)
          })
      })

    })

    describe(`51 eligible profiles with batch size of 50`, (done) => {
      let eligibleUsersIds = []
      let myUserId
      const INELIGIBLE_1_COUNT = 2
      const INELIGIBLE_2_COUNT = 1
      const INELIGIBLE_3_COUNT = 2
      before((done) => {
        // remove all users
        config.matchRules.MAX_PROFILES_TO_REVIEW = 50
        const saveUsersIds = (users) => {
          eligibleUsersIds = eligibleUsersIds.concat(
            _.map(users, (user) => user._id.toString())
          )
        }

        const saveMyUser = (users) => {
          myUserId = users[0]._id
        }

        resetDatabase()
          .then(() => addUsers({ count: 25, isDeleted: false, gender: GendersEnum.FEMALE, }))
          .then((usersInserted) => saveUsersIds(usersInserted))
          .then(() => addUsers({ count: INELIGIBLE_1_COUNT, isDeleted: true, gender: GendersEnum.FEMALE, }))
          .then(() => addUsers({ count: INELIGIBLE_2_COUNT, isDeleted: true, gender: GendersEnum.MALE, }))
          .then((maleUsers) => saveMyUser(maleUsers))
          .then(() => addUsers({ count: 20, isDeleted: false, gender: GendersEnum.FEMALE, }))
          .then((usersInserted) => saveUsersIds(usersInserted))
          .then(() => addUsers({ count: INELIGIBLE_3_COUNT, isDeleted: false, gender: GendersEnum.MALE, }))
          .then((unqualifiedUsersInserted) => addUsers({ count: 6, isDeleted: false, gender: GendersEnum.FEMALE, }))
          .then((usersInserted) => saveUsersIds(usersInserted))
          .then(() => done())
          .catch((e) => {
            console.error('Error', e)
            done(new Error(e.message))
          })
      })

      it(`should populate profilesToReview with
          - 50 profiles on first run 
          - 1 profile on the second run
          - and run the cycle from the beginning filling up 50 again on 3rd run
          considering there are only 51 eligible profiles to review in the database`, (done) => {
        
        const BATCH_SIZE = config.matchRules.MAX_PROFILES_TO_REVIEW
        getUser(myUserId)
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            expect(user.reviewedUpTo).to.be.equal(-1)
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)

            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT + INELIGIBLE_3_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const actualUsersIds = _.map(profilesToReview, (p) => p._id.toString())
            const expectedUsersIds = _.slice(eligibleUsersIds, 0, BATCH_SIZE)
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true

            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(1)

            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT + INELIGIBLE_3_COUNT
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const actualUsersIds = _.map(profilesToReview, (p) => p._id.toString())
            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE, BATCH_SIZE + 1)
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            // Cycle is repeated
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(BATCH_SIZE)

            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT + INELIGIBLE_3_COUNT - 1
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const actualUsersIds = _.map(profilesToReview, (p) => p._id.toString())
            const expectedUsersIds = _.slice(eligibleUsersIds, 0, BATCH_SIZE)
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true

            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(1)

            const expectedReviewedUpTo = BATCH_SIZE + INELIGIBLE_1_COUNT + INELIGIBLE_2_COUNT + INELIGIBLE_3_COUNT
            expect(user.reviewedUpTo).to.be.equal(expectedReviewedUpTo)

            const actualUsersIds = _.map(profilesToReview, (p) => p._id.toString())
            const expectedUsersIds = _.slice(eligibleUsersIds, BATCH_SIZE, BATCH_SIZE + 1)
            expect(areArraysEqual(actualUsersIds, expectedUsersIds)).to.be.true
            
            done()
          })
          .catch((e) => {
            console.error('##', e)
            done(e)
          })
      })

    })

    describe('# of profiles to review < config.matchRules.MAX_PROFILES_TO_REVIEW', () => {
      const profilesInDb = [
        { priority: 0, gender: GendersEnum.FEMALE, count: 10 },
        { priority: 1, gender: GendersEnum.FEMALE, count: 20 },
        { priority: 2, gender: GendersEnum.FEMALE, count: 15 },
        { priority: 3, gender: GendersEnum.FEMALE, count: 33 },
        { priority: 4, gender: GendersEnum.FEMALE, count: 25 },
        { priority: 0, gender: GendersEnum.FEMALE, count: 11 },
        { priority: 5, gender: GendersEnum.FEMALE, count: 14 },
        { priority: 6, gender: GendersEnum.FEMALE, count: 15 },
        { priority: 0, gender: GendersEnum.FEMALE, count: 18 },
        // Lower priority profiles after the last one
        { priority: 1, gender: GendersEnum.FEMALE, count: 2 },
        { priority: 2, gender: GendersEnum.FEMALE, count: 5 },
        { priority: 3, gender: GendersEnum.FEMALE, count: 3 },
        { priority: 4, gender: GendersEnum.FEMALE, count: 5 },
        { priority: 5, gender: GendersEnum.FEMALE, count: 4 },
        { priority: 6, gender: GendersEnum.FEMALE, count: 5 },
      ]

      before((done) => {
        config.matchRules.MAX_PROFILES_TO_REVIEW = 200
        setup.db.resetDatabase()
          .then(() => removeUsers())
          .then(() => addUser({seqNumber: 1, gender: GendersEnum.MALE, profilesToReview: {count: 0, matchScore: 100}}))
          .then(() => addUser({seqNumber: 2, gender: GendersEnum.MALE, profilesToReview: {count: 40, matchScore: 100}}))
          .then(() => addUser({seqNumber: 3, gender: GendersEnum.MALE, profilesToReview: {count: config.matchRules.MAX_PROFILES_TO_REVIEW, matchScore: 100}}))
          .then(() => setup.db.addUsers({ priorityWithUserSeqNumber: 1, profiles: profilesInDb, }))
          .then(() => done())
          .catch((err) => {
            console.error('beforeEach Error', err)
            done(new Error('Failed to setup the tests'))
          })
      })

      it(`should populate the user with the right amount of profilesToReview
          for each priority queue`, (done) => {
        getUserBySeqNumber({ seqNumber: 1, })
          .then((user) => {
            const profilesToReview = hashToArray(user.profilesToReview)
            expect(profilesToReview.length).to.be.equal(0)
            return useCase.populateWithNextProfilesToReview(user)
          })
          .then((user) => {
            let totalProfilesToReview = 0
            totalProfilesToReview += user.profilesToReview['0'].length
            totalProfilesToReview += user.profilesToReview['1'].length
            totalProfilesToReview += user.profilesToReview['2'].length
            totalProfilesToReview += user.profilesToReview['3'].length
            totalProfilesToReview += user.profilesToReview['4'].length
            totalProfilesToReview += user.profilesToReview['5'].length
            totalProfilesToReview += user.profilesToReview['6'].length
            const queuesSizes = _.reduce(profilesInDb, (result, val, i) => {
              _.set(result, val.priority, _.get(result, val.priority, 0) + val.count)
              return result
            }, {})
            let totalSize = 0
            for (let key in queuesSizes) {
              expect(user.profilesToReview[key].length).to.be.equal(queuesSizes[key])
              totalSize += queuesSizes[key]
            }
            expect(totalProfilesToReview).to.be.equal(totalSize)
            done()
          })
          .catch((e) => {
            console.error('spec.auth', e)
            done(new Error(e.message))
          })
      })
    })

  })

  describe('#registerUser', () => {

  })
})