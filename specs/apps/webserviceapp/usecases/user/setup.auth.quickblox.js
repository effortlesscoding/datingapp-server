'use strict'
const expect = require('chai').expect
const _ = require('lodash')
const mongoose = require('mongoose')

const DbSetup = rootRequire('specs/setuphelpers/DbSetup.js')
const GendersEnum = rootRequire('apps/webserviceapp/models/GendersEnum.js')
const MockUser = rootRequire('specs/mocks/MockUser.js')
const User = rootRequire('apps/webserviceapp/models/User.js')

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

class SpecsSetup extends DbSetup {

  constructor() {
    super()
    this.usersCount = 1
  }

  addUser(user) {
    const seqNumber = this.usersCount++
    const rawUser = new MockUser()
                .setUserId(mongoose.Types.ObjectId(user._id))
                .setUserSeqNumber(seqNumber)
                .setFacebookId(user.facebookId)
    rawUser.qbchat = {
      login: 'test',
      password: 'test123',
    }
    return new User(rawUser).save()
  }

  getUser(userId) {
    return User.findOne({ _id: userId, })
  }
}


module.exports = new SpecsSetup()