'use strict'
const _ = require('lodash')
const specs = require('./setup.auth.quickblox')
const UseCase = rootRequire('apps/webserviceapp/usecases/user/auth.quickblox').UseCase
const expect = require('chai').expect

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

describe('auth.quickblox', () => {

  let user
  const userDetails = {
    _id: '53cb6b9b4f4ddef1ad47f942',
    facebookId: 1,
  }

  before((done) => {
    specs.connect()
      .then(() => specs.mockDependencies())
      .then(() => specs.initialiseDependencies())
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('before##', e)
        done(new Error(e.message))
      })
  })
  beforeEach((done) => {
    specs.resetDatabase()
      .then(() => specs.addUser(userDetails))
      .then(() => specs.getUser(userDetails._id))
      .then((u) => {
        user = u
        done()
      })
      .catch((e) => {
        console.error('beforeEach()::', e)
        done(new Error(e.message))
      })
  })

  after((done) => {
    specs.disconnect()
      .then(() => specs.restoreDependencies())
      .then(() => {
        done()
      })
      .catch((e) => {
        console.error('before##', e)
        done(new Error(e.message))
      })
  })

  // What happens to the existing chats ? Well, recreated, I guess?
  describe('#reauthenticate', () => {

    beforeEach((done) => {
      const qbMock = specs.getQbMock()
      qbMock.usersLoginStub.reset()
      qbMock.usersCreateStub.reset()
      done()
    })

    it('does not save new credentials if login succeeds', (done) => {
      const qbMock = specs.getQbMock()
      qbMock.willReturn({
        login: {
        },
        register: {
        },
      })
      UseCase.reauthenticate(user)
        .then((result) => {
          const error = result.error
          const newUser = result.user
          expect(error).to.be.null
          expect(newUser.qbchat.login).to.be.equal(user.qbchat.login)
          expect(newUser.qbchat.password).to.be.equal(user.qbchat.password)
          expect(qbMock.usersLoginStub.callCount).to.be.equal(1)
          expect(qbMock.usersCreateStub.callCount).to.be.equal(0)
          return specs.getUser(userDetails._id)
        })
        .then((dbUser) => {
          expect(dbUser.qbchat.login).to.be.equal(user.qbchat.login)
          expect(dbUser.qbchat.password).to.be.equal(user.qbchat.password)
          done()
        })
        .catch((e) => {
          console.error('reauthenticate#', e)
          done(new Error(e.message))
        })
    })

    it('does not save new credentials if login fails and registration fails', (done) => {
      const qbMock = specs.getQbMock()
      qbMock.willReturn({
        login: {
          error: new Error('login fails')
        },
        register: {
          error: new Error('registration fails')
        },
      })
      UseCase.reauthenticate(user)
        .then((result) => {
          const error = result.error
          const newUser = result.user
          expect(error).to.exist
          expect(newUser).to.not.exist
          expect(error.message).to.be.equal('registration fails')
          expect(qbMock.usersLoginStub.callCount).to.be.equal(1)
          expect(qbMock.usersCreateStub.callCount).to.be.equal(1)
          return specs.getUser(userDetails._id)
        })
        .then((dbUser) => {
          expect(dbUser.qbchat.login).to.be.equal(user.qbchat.login)
          expect(dbUser.qbchat.password).to.be.equal(user.qbchat.password)
          done()
        })
        .catch((e) => {
          console.error('reauthenticate#', e)
          done(new Error(e.message))
        })
    })

    it('saves new credentials if login fails and registration succeeds', (done) => {
      const qbMock = specs.getQbMock()
      qbMock.willReturn({
        login: {
          error: new Error('login fails')
        },
      })
      const originalLogin = user.qbchat.login
      const originalPassword = user.qbchat.password
      let newQbChat
      UseCase.reauthenticate(user)
        .then((result) => {
          const error = result.error
          const newUser = result.user
          expect(error).to.not.exist
          expect(newUser).to.exist
          expect(newUser.qbchat.login).to.not.be.equal(originalLogin)
          expect(newUser.qbchat.password).to.not.be.equal(originalPassword)
          expect(qbMock.usersLoginStub.callCount).to.be.equal(1)
          expect(qbMock.usersCreateStub.callCount).to.be.equal(1)
          newQbChat = newUser.qbchat
          return specs.getUser(userDetails._id)
        })
        .then((dbUser) => {
          expect(dbUser.qbchat.login).to.not.be.equal(originalLogin)
          expect(dbUser.qbchat.password).to.not.be.equal(originalPassword)
          expect(dbUser.qbchat.login).to.be.equal(newQbChat.login)
          expect(dbUser.qbchat.password).to.be.equal(newQbChat.password)
          done()
        })
        .catch((e) => {
          console.error('reauthenticate#', e)
          done(new Error(e.message))
        })
    })

  })

  describe.skip('#setDefaultCredentials', () => {
    it('sets the default credentials if not present', (done) => {

    })


    it('does not set the default credentials if already present', (done) => {

    })

  })
})