'use strict';

let QB = require('quickblox')
let sinon = require('sinon')
var expect = require('chai').expect
let QBAuthUseCase = rootRequire(`/apps/webserviceapp/usecases/user/auth.quickblox`)
let MockUser = rootRequire(`/specs/mocks/MockUser`)
let UserModel = rootRequire(`/apps/webserviceapp/models/User`)
let logger = rootRequire(`/specs/mocks/logs`)
let config = rootRequire(`/config`)

function rootRequire(path) {
  const ROOT_PATH = '../../../../'
  return require(`${ROOT_PATH}${path}`)
}

describe('Testing qb user authentication use cases.', function() {

  const FIRST_USER_ID = 1;
  const FIRST_USER_FACEBOOK_ID = 10;
  const SECOND_USER_ID = 2;
  const SECOND_USER_FACEBOOK_ID = 20;
  let mockUser;

  before(function(done) {
    // stub a user and get his default login and password details

    this.timeout(10000);
    Promise.resolve()
      .then(initQb)
      .then(stubUser)
      // .then(stubSecondUser)
      // .then(recreateFirstQbUser)
      .then(() => { done(); }, done);
  });

  it('should use Quickblox\'s user if our login and password match the ones in Quickblox', (done) => {
    // Deep copy of our original login and passwords
    let userLogin = JSON.parse(JSON.stringify(mockUser.qbchat.login));
    let userPassword = JSON.parse(JSON.stringify(mockUser.qbchat.password));

    // Quickblox's login will return a valid user 
    sinon.stub(QB.auth, 'login', function(params, callback) {
      callback(null, {
        id: 1,
        login: 'mock_user',
        email: 'mock_user@gmail.com'
      });
    });
    new QBAuthUseCase(mockUser).setDefaultCredentials()
      .then((user) => {
        expect(user.qbchat.login).to.be.equal(userLogin);
        expect(user.qbchat.password).to.be.equal(userPassword);
        expect(user.qbchat.id).to.be.equal('1');
        done();
      })
      .catch((err) => {
        logger.error('Failure:');
        logger.error(err);
        done(err);
      });
  });

  it('should create a new user if he does not exist in Quickblox', (done) => {
    // Deep copy of our original login and passwords
    let userLogin = JSON.parse(JSON.stringify(mockUser.qbchat.login));
    let userPassword = JSON.parse(JSON.stringify(mockUser.qbchat.password));

    // Quickblox's login will return 401 Unauthorized error
    sinon.stub(QB.auth, 'login', function(params, callback) {
      logger.info('401 error!');
      callback({code: 401, status: '401 Unauthorized' }, null);
    });

    sinon.stub(QB.users, 'create', function(params, callback) {
      logger.info('Creating a user!');
      callback(null, {
        id: 1,
        login: 'mock_user',
        email: 'mock_user@gmail.com'
      });
    });

    new QBAuthUseCase(mockUser).setDefaultCredentials()
      .then((user) => {
        expect(user.qbchat.login).to.be.equal(userLogin);
        expect(user.qbchat.password).to.be.equal(userPassword);
        expect(user.qbchat.id).to.be.equal('1');
        done();
      })
      .catch((err) => {
        logger.error('Failure:');
        logger.error(err);
        done(err);
      });
  });

  it('should just fail if Quickblox already has a user with that login.', (done) => {
    // Quickblox's login will return 401 unauthorized
    sinon.stub(QB.auth, 'login', function(params, callback) {
      callback({code: 401, status: '401 Unauthorized' }, null);
    });
    // Quickblox's user create will return 422 because that user already exists
    sinon.stub(QB.users, 'create', function(params, callback) {
      callback({code: 422, status: '422 Unprocessed Entity'}, null);
    });
    // Quickblox's create will return an error 422 that user already exists
    new QBAuthUseCase(mockUser).setDefaultCredentials()
      .then((user) => {
        done(new Error('Expected a failure'));
      })
      .catch((err) => {
        // Valid because since we couldn't login and we couldn't create
        // that user it means we have lost that Quickblox user's password
        // Just forget about him (ideally, e-mail our admins about this problem)
        logger.info('Failed just as expected.');
        done();
      });
  });

  afterEach((done) => {
    if (QB.auth.login.restore) {
      QB.auth.login.restore(); 
    }
    if (QB.users.create.restore) {
      QB.users.create.restore(); 
    }
    done();
  });

  after((done) => {
    // no need to tear down here, I think
    done();
  });

  function initQb() {
    return new Promise((resolve, reject) => {
      let qbConfig = config.quickblox;
      logger.info('Initialising a quickblox instance.');
      QB.init(qbConfig.appId, qbConfig.authKey, qbConfig.authSecret);
      QB.createSession(function(err, result) {
        if (err) {
          logger.error('Error in QB:');
          return reject(err);
        } 
        logger.info('QB inited with result:');
        logger.info(result);
        return resolve();
      });
    });
  }

  function stubUser() {
    let stubUser = new MockUser()
        .setUserId(FIRST_USER_ID)
        .setFacebookId(FIRST_USER_FACEBOOK_ID)
        .setGenderMale();

    mockUser = new UserModel(stubUser);
    let userQbAuthUseCase = new QBAuthUseCase(mockUser);
    mockUser.qbchat.login = userQbAuthUseCase.defaultLogin();
    mockUser.qbchat.password = userQbAuthUseCase.defaultPassword();
    mockUser.qbchat.id = 1; // Irrelevant
  }
});
