const UseCase = rootRequire('apps/webserviceapp/usecases/storage/urls').UseCase

function rootRequire(path) {
  const ROOT_PATH = '../../../../../'
  return require(`${ROOT_PATH}${path}`)
}

describe('UrlSigningUseCase', () => {
  it('should sign a resource', function(done) {
    UseCase.signResource('something')
      .then((url) => {
        done()
      })
      .catch((e) => {
        done(new Error(e.message))
      })
  })
})