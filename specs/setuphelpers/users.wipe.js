'use strict';
const ROOT_PATH = '../../../';
let UserModel = require(`${ROOT_PATH}/apps/webserviceapp/models/User`);
let logger = require(`${ROOT_PATH}/specs/mocks/logs`);

function removeUsers() {
  logger.info('Removing users.');
  return UserModel.remove({}).exec();
}

module.exports = removeUsers;