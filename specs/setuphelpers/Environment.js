const config = rootRequire('config')
const chai = require('chai')
const chaiHttp = require('chai-http')
const DbSetup = require('./DbSetup')
const WebServiceApp = rootRequire('apps/webserviceapp')
const setup = new DbSetup()
const db = setup
const authentication = setup
chai.use(chaiHttp)

function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`)
}
let server
let appWrapper

class TestEnvironment {
  constructor() {
    this.server = null
    this.appWrapper = null
    this.db = db
    this.authentication = authentication
  }

  setup() {
    return setup.mockDependencies()
      .then(() => {
        console.log('Started!')
        this.appWrapper = new WebServiceApp(
          setup.getDatabaseTestUrl(),
          setup.getDatabaseTestConfiguration()
        )
      })
      .then(() => this.appWrapper.start())
      .then((app) => {
        console.log('Started!')
        this.server = app
      })
      .then(() => setup.resetDatabase())
      .catch((e) => {
        done(new Error(e.message))
      })
  }

  tearDown() {
    return setup.restoreDependencies()
      .then(() => this.appWrapper.stop())
      .catch((e) => {
        done(new Error('Could not restore dependencies'))
      })
  }
}

module.exports = new TestEnvironment()
