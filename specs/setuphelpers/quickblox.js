'use strict';
const ROOT_PATH = '../../../';
let config = require(`${ROOT_PATH}/config`);
let logger = require(`${ROOT_PATH}/specs/mocks/logs`);
let QB = require('quickblox');

function initialiseQuickblox() {
  return new Promise((resolve, reject) => {
    let qbConfig = config.quickblox;
    logger.info('Initialising a quickblox instance.');
    QB.init(qbConfig.appId, qbConfig.authKey, qbConfig.authSecret);
    QB.createSession(function(err, result) {
      if (err) {
        logger.error('Error in QB:');
        return reject(err);
      } 
      logger.info('QB inited with result:');
      logger.info(result);
      return resolve();
    });
  });
}

module.exports = initialiseQuickblox;