'use strict';
const ROOT_PATH = '../../../';
let config = require(`${ROOT_PATH}/config`);
let mongoose = require('mongoose');
let logger = require(`${ROOT_PATH}/specs/mocks/logs`);

function connectToMongodb() {
  return new Promise((resolve, reject) => {
    mongoose.connection.on('connected', resolve);
    mongoose.connection.on('error', (err) => {
      logger.info('Error:');
      logger.info(err);
      return reject(err);
    });
    mongoose.connect(config.mongoDb.url + config.mongoDb.testCollection);
  });
}

module.exports = connectToMongodb;