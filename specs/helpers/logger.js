'use strict';

class Logger {
  
  info(message) {
    return;
    if (typeof message === 'object') {
      console.log(`TEST_LOG ---- ${JSON.stringify(message)}`);
    } else {
      console.log(`TEST_LOG ---- ${message}`);
    }
  }

  debug(message) {
    return;
    if (typeof message === 'object') {
      console.log(`TEST_LOG_DEBUG ---- ${JSON.stringify(message)}`);
    } else {
      console.log(`TEST_LOG_DEBUG ---- ${message}`);
    }
  }

  error(message) {
    return;
    if (typeof message === 'object') {
      console.log(`TEST_LOG_ERROR ---- ${JSON.stringify(message)}`);
    } else {
      console.log(`TEST_LOG_ERROR ---- ${message}`);
    }
  }
}

module.exports = new Logger();