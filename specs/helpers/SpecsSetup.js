'use strict'
const _ = require('lodash')
const QB = require('quickblox')
const sinon = require('sinon')
const mongoose = require('mongoose')
const passport = require('passport')

const config = rootRequire(`config`)
const logger = rootRequire(`specs/helpers/logger`)
const UserSettings = rootRequire(`apps/webserviceapp/models/UsersSettings`)
const User = rootRequire(`apps/webserviceapp/models/User`)
const MockRequest = rootRequire(`specs/mocks/MockRequest`)
const MockResponse = rootRequire(`specs/mocks/MockResponse`)

function rootRequire(path) {
  const ROOT_PATH = '../../'
  return require(`${ROOT_PATH}${path}`)
}

class SpecsSetup {

  constructor() {
    process.env.NODE_ENV = 'test';
    this.NEXT_SEQ_NUMBER = 1;
  }

  closeDatabaseConnection() {
    return new Promise((resolve, reject) => {
      mongoose.connection.close(function () {
        resolve();
      });
    });
  }

  setupDatabase() {
    logger.debug('Setting up database.');
    return new Promise((resolve, reject) => {
      mongoose.connection.on('connected', resolve);
      mongoose.connection.on('error', (err) => {
        logger.info('Error:');
        logger.info(err);
        return reject(err);
      });
      const dbConfig = { 
        user: config.mongoDb.usernameTest,
        pass: config.mongoDb.passwordTest,
      }
      mongoose.connect(
        config.mongoDb.url + config.mongoDb.collectionTest,
        dbConfig
      );
    });
  }

  restoreQuickblox() {
    return new Promise((resolve, reject) => {
      if (this.qbCreate) this.qbCreate.restore();
      if (this.qbLogin) this.qbLogin.restore();
      return resolve();
    });
  }

  setupQuickblox() {
    logger.debug('Setting up quickblox.');
    return new Promise((resolve, reject) => {
      if (!QB.users) {
        QB.users = {
          create: function(params, callback) { callback(null, null); }
        }
      }
      if (!QB.auth) {
        QB.auth = {
          login: function(params, callback) { callback(null, null); }
        }
      }
      this.qbCreate = sinon.stub(QB.users, 'create', function(params, callback) {
        callback(null, {
          login: params.login,
          id: 1
        })
      })
      this.qbLogin = sinon.stub(QB.auth, 'login', function(params, callback) {
        callback(null, {
          login: params.login,
          id: 1
        })
      })
      resolve()
    });
  }

  restoreQb() {
    logger.debug('Restoring up quickblox.');
    return new Promise((resolve, reject) => {
      if (QB.auth.login.restore) QB.auth.restore();
      if (QB.users.create.restore) QB.users.create.restore();
    });
  }

  removeAllUsers() {
    logger.debug('Removing all users.');
    return new Promise((resolve, reject) => {
      User.remove({}).exec()
        .then(resolve, reject);
    });
  }

  resetUsersCounter() {
    logger.debug('Resetting users settings.');
    return new Promise((resolve, reject) => {
      UserSettings.remove({}).exec()
        .then(() => {
          const newSettings = new UserSettings({
            _id: 1,
            nextSeqNumber: this.NEXT_SEQ_NUMBER
          })
          newSettings.save()
            .then(resolve, reject);
        })
        .then(null, reject);
    }); 
  }

  request() {
    const request = new MockRequest();
    request.setUserId(this.DEFAULT_USER_FACEBOOK_ID);
    return request;
  }

  response() {
    return new MockResponse();
  }
}

module.exports = SpecsSetup;