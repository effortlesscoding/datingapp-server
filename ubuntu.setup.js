Mongodb security:

* Using Mongodb 3.4

A few scripts:

createFirstUsers.js
createRoles.js
createUsers.js


////

# Where and how to store data.
storage:
  dbPath: /home/ubuntu/deploy/mongodb/data
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /home/ubuntu/deploy/mongodb/logs

# network interfaces
net:
  port: 3979
  bindIp: 127.0.0.1
