1. To install run npm install
2. node server.js to start the server
3. The mongodb must be run in order to start server. 
[Run mongowin]
4. To run rabbitmq, just run it via command line (?!)

http://blog.andrewray.me/how-to-debug-mocha-tests-with-chrome/

Use node-inspector

1. node-inspector
2. Open that
3. mocha --debug-brk
4. Add debugger; keyword anywhere where you want to debug.


Load Layers API into your app.
Write exact test cases.
Read about accounting

Install :
- sudo npm install -g n
- sudo npm install -g mocha
- sudo npm install -g gulp


/*** To generate docs ***/
apidoc -i ./webserviceapp

To modify environment variables for sudo:
sudo visudo
OR
(Ubuntu)
sudo sudoers

nohup sudo mongod --dbpath=/home/ubuntu/deploy/mongodb/data &

ps -fC mongod

db.createCollection('usersettings');
db.usersettings.insert({ "_id" : 1, "nextSeqNumber" : 1, "__v" : 0 });

pm2 list
pm2 start server.js
